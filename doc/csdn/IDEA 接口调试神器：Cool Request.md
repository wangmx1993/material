## 前言

在后端SpringBoot开发中，都需要一个接口测试工具，从一开始的postman，到现在的国产测试工具，数不胜数，而最方便的莫过于在IDEA中就可以调试，因为IDEA插件中有能力分析出当前项目所编写的Controller数据，可以进行统计，更方便我们发起http请求，不需要复制url到别的测试工具了，今天就推荐一款，名叫Cool Request，他纯免费、开源。

- 插件名称：Cool Request

- 作用：简化SpringBoot后端接口调试流程。

- 安装方式：IDEA 插件商店中搜索Cool Request

## 效果图

Cool Request的界面非常简单，下面是他的主界面，可以收集项目的Controller信息。

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/CKvMdchsUwk7wrocSTDv5R1LQuBYDGKvjRz9QZKptZiaREcW1DJyricGODib99SNziaLUWde6eaLKFQibsicT5KEHnSw/640?wx_fmt=other&from=appmsg&wxfrom=5&wx_lazy=1&wx_co=1)

## 基本HTTP请求

当双击某个Controller时候，会跳转到请求发送界面，在这里可以填写参数，Cool Request和同类的插件相比，在填写Header时候，也是有提示的，而其他没有，这一点非常方便。

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/CKvMdchsUwk7wrocSTDv5R1LQuBYDGKvtZS1UCSSTo2qdSy5BoyEEVOrqaG5LwrZXyapMCGnsQNyjJ1OvibrQaQ/640?wx_fmt=other&from=appmsg&wxfrom=5&wx_lazy=1&wx_co=1)

## 结果预览

Cool Request也有五种不同的响应预览，会自动根据响应头中的Content-Type跳转到不同的预览效果里，有json、text、image、html、xml。

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/CKvMdchsUwk7wrocSTDv5R1LQuBYDGKvvyz4pms5YqmWgh7S3O0QEXibVh0WVsBMb002t0cERkubpVGY742p0icg/640?wx_fmt=other&from=appmsg&wxfrom=5&wx_lazy=1&wx_co=1)

## 手动触发SpringBoot中的定时器

测试定时器时，不知道大家是怎么测试的，以前是通过单独写一个Controller，然后在内部调用一下，而用了Cool Request，居然可以支持手动触发，这样无论定时器的时间间隔是多少，都可以在Cool Request中随心所欲的调用。

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/CKvMdchsUwk7wrocSTDv5R1LQuBYDGKv8pyFynKFKic11uJCiaWgma0J78HXWYwgqibNnZxcEibjy8VU8Fib8NtBw1w/640?wx_fmt=other&from=appmsg&wxfrom=5&wx_lazy=1&wx_co=1)

## 反射调用

这一点可能是有点抽象，不好理解，这个功能只有在特定场景下才能体会到极其方便。

试想一下这个场景。

1. Controller返回的信息不需要用户信息，也就是一些全局的数据

2. 但是Controller需要用户登录后才能使用，即需要附带Token

3. 你的项目具有拦截器，会拦截没有登录的用户

这个时候你有没有想过，我这个Controller又没有用到用户信息，能不能在调试时候绕过拦截器，以前的做法可能是关闭拦截器，或者先登录，正常拿到Token后在调用，而Cool Request的这个功能就是解决这个问题的，他可以通过反射的一系列技术，绕过拦截器，将请求直接到达Controller，并返回结果。

使用方式是在请求界面选择reflex，然后填写参数后发起请求。

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/CKvMdchsUwk7wrocSTDv5R1LQuBYDGKvvkgoIOibYkEFricfB5vSRRRoMBjL5hHMrG46NOb39yiap3o1nWxXWoaZA/640?wx_fmt=other&from=appmsg&wxfrom=5&wx_lazy=1&wx_co=1)

但是reflex也有缺点，他同时也绕过了过滤器，并且没有办法让过滤器也执行，唯一的办法是发起正常的HTTP请求。

## 强大的前后置脚本

最方便的莫过于Cool Request提供了java的前后置脚本了，也算不上脚本了，习惯这么叫，而其他插件要不就是没有，要不就是提供的JS脚本，对不熟悉JS的人来说，增加了学习成本，而Cool Request就非常方便了，另外在编写代码时，是有语法提示的，例如在脚本中修改参数的api，也不需要过多的学习，几乎是0成本。在handlerRequest方法中，有两个参数，第一个是日志输出接口，可以使用他的println方法输出日志，会在右面的log窗口看到，第二个参数是HTTPRequest，他有一系列方法，使用.号时候就有提示，所有的方法我们一眼就能看出是做什么的。

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/CKvMdchsUwk7wrocSTDv5R1LQuBYDGKvJwndLamGgw77S3XVxvjUuHRYonnCXlicX3Km18DcAw1HgphCtR0ZmtQ/640?wx_fmt=other&from=appmsg&wxfrom=5&wx_lazy=1&wx_co=1)

更方便的是，脚本中可以调用项目的类，也支持第三方的类，比如SpringBoot内置的一些Util工具类，都可以调用，已经方便到极致了。

但是也有一个小缺点，内置的编译器是java8的，也就是说，项目使用的java版本超过8，或者是第三方库的版本超过java8，就无法调用了，比如项目使用了SpringBoot3，就无法调用，但是都可以在脚本中自己实现。

在一些动态参数时候，非常有用，比如参数的签名值，是通过一些参数计算出来的，或者是动态时间。

## 支持多种请求体

Cool Request支持六种请求体，也支持二进制文件上传。

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/CKvMdchsUwk7wrocSTDv5R1LQuBYDGKvnx8MOBfe4wVTTJoDHBnj5iaA39gSC67xPWcicIhE1Iqy2YmlyhPLS7Ow/640?wx_fmt=other&from=appmsg&wxfrom=5&wx_lazy=1&wx_co=1)

## 快速导入cURL参数

如果有一个cURL参数（可能是从浏览器中复制过来的），那么可以快速导入的Cool Request里面，同时也支持将请求复制为cURL格式。

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/CKvMdchsUwk7wrocSTDv5R1LQuBYDGKvsgESzPh5Px7UFiaAyEJkQ5uvKevpIicaMicFyiaoiaZTIILcRkQwuKnricTQ/640?wx_fmt=other&from=appmsg&wxfrom=5&wx_lazy=1&wx_co=1)

## 导出到Apifox

如果团队使用了apifox，它还可以支持导出到apifox里面，虽然apifox自己也有插件，但是apifox不提供目录选择，而Cool Request支持，当然需要配置apifox的一些token，这点可以在插件的官网找到详细的说明。

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/CKvMdchsUwk7wrocSTDv5R1LQuBYDGKvoKhp4Vf6T58Ef2E6Of77xibib0sKXALnLSMlu6bvsQsbzVdcAxw8IowA/640?wx_fmt=other&from=appmsg&wxfrom=5&wx_lazy=1&wx_co=1)

## 全局搜索API

可以通过一个api路径来快速找到项目中的Controller位置。

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/CKvMdchsUwk7wrocSTDv5R1LQuBYDGKvL7uicfP2FXnvUJuLQkgdVmP0HtqUqGbiboFmDUzQnib93BoYw0wWLQicoA/640?wx_fmt=other&from=appmsg&wxfrom=5&wx_lazy=1&wx_co=1)

## 静态资源服务器

它内置了一个静态资源服务器，使用也非常简单，选择目录、选择端口、开启即可，就可以通过浏览器访问此目录下的文件了。

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/CKvMdchsUwk7wrocSTDv5R1LQuBYDGKv42otuvIRB9CGeEEyteXhtFmiaTe0rx60ghWLDW1D34xiaKFDpM7RnicMA/640?wx_fmt=other&from=appmsg&wxfrom=5&wx_lazy=1&wx_co=1)

## 项目地址

**[https://github.com/houxinlin/cool-request](https://github.com/houxinlin/cool-request)**