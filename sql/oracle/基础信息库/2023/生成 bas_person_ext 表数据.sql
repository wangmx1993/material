
--1、复制主表数据到临时表，生成bas_person_ext表数据
--ext的必录项如果在Info表没有，则先使用默认值.
create table bas_person_ext_tmp_231017
as 
SELECT t1.PER_ID,
       t1.PER_ID           as PER_EXT_ID,
       t1.biz_key,
       t1.FISCAL_YEAR,
       t1.version,
       t1.VERSION_NAME,
       t1.MOF_DIV_CODE,
       t1.MOF_DIV_NAME,
       t1.AGENCY_CODE,
       t1.create_time,
       t1.update_time,
       t1.IS_LAST_INST,
       t1.START_DATE,
       t1.END_DATE,
       t1.IS_DELETED,
       t1.IS_ENABLED,
       0                   as BASIC_MON, --基本工资
       0                   as FACT_PAY_MON,--实际执行工资
       2                   as IS_SPEC_POS,--是否特殊岗位
       t1.CREATE_USER_ID,
       t1.CREATE_USER_CODE,
       t1.create_user_name,
       t1.UPDATE_USER_ID,
       t1.UPDATE_USER_CODE,
       t1.update_user_name
  FROM bas_person_info t1
 where t1.fiscal_year = '2024'
   and version = '2024rcdtwh'
   and t1.is_deleted = 2
   and not exists
 (SELECT 1 FROM bas_person_ext t2 where t2.PER_ID = t1.PER_ID);

--2、为临时表创建索引，加快第三步的速度
create index IND_bas_person_ext_tmp_231017 on bas_person_ext_tmp_231017 (per_id);

--3、将数据插入到 bas_person_ext 表生成新数据
MERGE INTO bas_person_ext A
USING bas_person_ext_tmp_231017 B
ON (A.per_id = B.per_id)
WHEN NOT MATCHED THEN
  INSERT
    (A.PER_ID,
     A.PER_EXT_ID,
     A.BIZ_KEY,
     A.FISCAL_YEAR,
     A.VERSION,
     A.VERSION_NAME,
     A.MOF_DIV_CODE,
     A.MOF_DIV_NAME,
     A.AGENCY_CODE,
     A.CREATE_TIME,
     A.UPDATE_TIME,
     A.IS_LAST_INST,
     A.START_DATE,
     A.END_DATE,
     A.IS_DELETED,
     A.IS_ENABLED,
     A.BASIC_MON,
     A.FACT_PAY_MON,
     A.IS_SPEC_POS,
     A.CREATE_USER_ID,
     A.CREATE_USER_CODE,
     A.CREATE_USER_NAME,
     A.UPDATE_USER_ID,
     A.UPDATE_USER_CODE,
     A.UPDATE_USER_NAME)
  VALUES
    (B.PER_ID,
     B.PER_EXT_ID,
     B.BIZ_KEY,
     B.FISCAL_YEAR,
     B.VERSION,
     B.VERSION_NAME,
     B.MOF_DIV_CODE,
     B.MOF_DIV_NAME,
     B.AGENCY_CODE,
     B.CREATE_TIME,
     B.UPDATE_TIME,
     B.IS_LAST_INST,
     B.START_DATE,
     B.END_DATE,
     B.IS_DELETED,
     B.IS_ENABLED,
     B.BASIC_MON,
     B.FACT_PAY_MON,
     B.IS_SPEC_POS,
     B.CREATE_USER_ID,
     B.CREATE_USER_CODE,
     B.CREATE_USER_NAME,
     B.UPDATE_USER_ID,
     B.UPDATE_USER_CODE,
     B.UPDATE_USER_NAME);

--4、删除临时表
drop table bas_person_ext_tmp_231017;


