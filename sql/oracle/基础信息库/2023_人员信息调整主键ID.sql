
declare
  --人员信息调整主键ID，将指定的旧值调整为随机的新值
  --运行之后，在输出日志中获取执行脚本
  i            number := 0;
  v_sql        varchar2(32767) := '';
  v_is_has_id  number := 0;
  v_new_per_id varchar2(38);
  cursor vrows is
    select t.per_id
      from BAS_PERSON_INFO t
    --把需要修改的主键放在这里
     WHERE per_id in ('000004774A96F99B6CC62D313D6282C3',
                      '000046E15E27AF0E170ADC8B078F0539',
                      '0000B0E395B9CA0DC735A5571071DDFF',
                      '0000EC2E6FC45BCF0FCC884B88B307AF',
                      '0001B53A536A13F4490661A3D14F67E9');
begin
  --查看主表中是否已经存在per_id=1的假数据
  SELECT count(1)
    into v_is_has_id
    FROM BAS_PERSON_INFO T
   where per_id = '1'
     and rownum <= 1;

  --主表中不存在per_id=1的假数据时，自动新增一条，已经存在时，则忽略。
  if v_is_has_id <= 0 then
    v_sql := v_sql || 'insert into BAS_PERSON_INFO (';
    for COLUMN_NAME_vrow in (SELECT T.COLUMN_NAME,
                                    t.DATA_TYPE,
                                    t.DATA_LENGTH,
                                    t.DATA_PRECISION,
                                    t.DATA_SCALE
                               FROM user_tab_columns T
                              where t.TABLE_NAME = 'BAS_PERSON_INFO'
                              order by t.COLUMN_ID) loop
      v_sql := v_sql || '' || COLUMN_NAME_vrow.COLUMN_NAME || ',';
    end loop;
    v_sql := substr(v_sql, 1, length(v_sql) - 1) || ') values (';

    for COLUMN_NAME_vrow in (SELECT T.COLUMN_NAME,
                                    t.DATA_TYPE,
                                    t.DATA_LENGTH,
                                    t.DATA_PRECISION,
                                    t.DATA_SCALE
                               FROM user_tab_columns T
                              where t.TABLE_NAME = 'BAS_PERSON_INFO'
                              order by t.COLUMN_ID) loop
      if instr(COLUMN_NAME_vrow.Data_Type, 'DATE') > 0 then
        v_sql := v_sql || 'sysdate' || ',';
      elsif instr(COLUMN_NAME_vrow.Data_Type, 'NUMBER') > 0 then
        v_sql := v_sql || '1' || ',';
      elsif instr(COLUMN_NAME_vrow.Data_Type, 'CHAR') > 0 then
        v_sql := v_sql || '''1''' || ',';
      else
        v_sql := v_sql || '1' || ',';
      end if;
    end loop;
    v_sql := substr(v_sql, 1, length(v_sql) - 1) || '); ';
    dbms_output.put_line('--新增一条per_id=1的无效数据，用于数据临时关联.');
    dbms_output.put_line(v_sql);
    dbms_output.put_line(' ');
  end if;

  for vrow in vrows loop
    SELECT sys_guid() into v_new_per_id FROM dual T;
    v_sql := '';
    v_sql := 'update BAS_PERSON_EXT set per_id=' || '''1''' ||
             ',per_ext_id=' || '''1''' || ' where per_id =' || '''' ||
             vrow.per_id || ''';';
    dbms_output.put_line('--附表临时关联到per_id=1的假数据');
    dbms_output.put_line(v_sql);
    v_sql := '';
    v_sql := 'update BAS_PERSON_INFO set per_id=''' || v_new_per_id ||
             ''' where per_id =' || '''' || vrow.per_id || ''';';
    dbms_output.put_line('--主表使用新ID');
    dbms_output.put_line(v_sql);

    v_sql := '';
    v_sql := 'update BAS_PERSON_EXT set per_id=''' || v_new_per_id ||
             ''',per_ext_id=''' || v_new_per_id ||
             '''  where per_id =''1'';';
    dbms_output.put_line('--附表重新关联到主表的新ID');
    dbms_output.put_line(v_sql);

    v_sql := '';
    v_sql := 'update BAS_PERSON_INFO set parent_version_id=''' ||
             v_new_per_id || ''' where parent_version_id =' || '''' ||
             vrow.per_id || ''';';
    dbms_output.put_line('--主表提取生成的财政供养、预算编制版本数据关联新ID');
    dbms_output.put_line(v_sql);

    v_sql := '';
    v_sql := 'update bas_person_audit set per_id=''' || v_new_per_id ||
             ''' where per_id =' || '''' || vrow.per_id || ''';';
    dbms_output.put_line('--版本号与主键关联关系表，指向新ID');
    dbms_output.put_line(v_sql);
    dbms_output.put_line(' ');
  end loop;
end;

