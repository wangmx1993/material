
-- 创建一个临时表(同时随便带一条数据)
create table bas_agency_info_tmp_13 as
  SELECT T.*
    FROM bas_agency_info t
   where length(t.agency_code) = 3
     and t.is_leaf = 2
     and t.is_deleted = 2
     and rownum <= 1;

-- 将这条数据修改为无效数据
UPDATE bas_agency_info_tmp_13 T
   SET t.agency_id        = 'can_delete',
       t.is_enabled       = 2,
       t.is_deleted       = 1,
       t.agency_code      = '000000',
       t.agency_name      = '无效数据',
       t.update_user_name = '无效数据,可删除',
       t.create_user_name = '无效数据,可删除'
 where rownum <= 1;

-- 将这条无效数据插入到info表，用于后续更新 ext 表主键时，临时关联它.
insert into bas_agency_info
  SELECT T.*
    FROM bas_agency_info_tmp_13 t
   where t.agency_id = 'can_delete';

-- 处理基础库有对应的部门编码，但是单位agency_id与平台ele_id对不上的数据，对它们进行更新.
-- 要素表的 ele_code、mof_div_code 在 bas_agency_info 中有对应的 agency_code、mof_div_code，但是 ele_id 没有对应的 agency_id。
-- 直接修复基础库中的 agency_id 为 ele_id 值.
declare
  cursor vrows is
    SELECT e.mof_div_code, e.ele_id, e.ele_code
      FROM ele_agency e
     where e.is_leaf != 1
       and e.is_deleted = 2
       and sysdate between e.start_date and e.end_date
       and exists (SELECT 1
              FROM bas_agency_info t2
             where t2.mof_div_code = e.mof_div_code
               and t2.agency_code = e.ele_code)
       and not exists
     (SELECT 1 FROM bas_agency_info t2 where t2.agency_id = e.ele_id);

  v_update_agency_id bas_agency_info.agency_id%type; -- 需要更新的主键ID
  v_i                number := 1;--用于计数
begin
  for vrow in vrows loop
    SELECT T2.agency_id
      into v_update_agency_id
      FROM (SELECT t.agency_id
              FROM bas_agency_info t
             WHERE t.mof_div_code = vrow.mof_div_code
               and t.agency_code = vrow.ele_code
             order by t.update_time desc) t2
     where rownum <= 1;

    -- dbms_output.put_line(v_i || ' ' || v_update_agency_id || ' -> ' || vrow.ele_id || ' 开始');

    -- 先让它ext表数据关联到临时数据
    UPDATE bas_agency_ext T3 SET t3.agency_id = 'can_delete', t3.agency_ext_id = 'can_delete' WHERE t3.agency_id = v_update_agency_id;

    --修正主表数据
    UPDATE bas_agency_info T3
       SET update_time  = to_date('2023-12-12 12:12:12', 'yyyy-mm-dd hh24:mi:ss'),
           t3.agency_id = vrow.ele_id,
           t3.biz_key = vrow.ele_id
     WHERE t3.agency_id = v_update_agency_id;

   -- 修正附表数据
    UPDATE bas_agency_ext T3
       SET update_time      = to_date('2023-12-12 12:12:12', 'yyyy-mm-dd hh24:mi:ss'),
           t3.agency_id     = vrow.ele_id,
           t3.agency_ext_id = vrow.ele_id,
           t3.biz_key = vrow.ele_id
     WHERE t3.agency_id = 'can_delete';
    v_i := v_i + 1;
    --提交生效
    commit;
  end loop;
end;

/


-- 在部标Info表没有的部门，且这些部门下面已经有单位插入到info表了，则将这些部门新增插入到info表。
declare
  cursor vrows is
    SELECT e.*
      FROM ele_agency e
     where e.is_leaf != 1
       and e.is_deleted = 2
       and sysdate between e.start_date and e.end_date
       and not exists (SELECT 1
              FROM bas_agency_info t2
             where t2.mof_div_code = e.mof_div_code
               and t2.agency_code = e.ele_code
               and t2.is_deleted = 2)
       and not exists
     (SELECT 1 FROM bas_agency_info t2 where t2.agency_id = e.ele_id)
       and exists (SELECT 1
              FROM bas_agency_info t2
             where t2.mof_div_code = e.mof_div_code
               and t2.agency_code like e.ele_code || '%'
               and length(t2.agency_code) > length(e.ele_code)
               and t2.is_leaf = 1
               and t2.is_deleted = 2);

  v_i              number := 1; -- 统计个数
  v_copy_agency_id bas_agency_info.agency_id%type; -- 待新增的部门下面的本级单位ID
begin
  for vrow in vrows loop
    -- 从部标表查询待新增部门的本级单位(xxx001)，部门信息不好明确指定的列尽量与它保持一致.
    SELECT agency_id
      into v_copy_agency_id
      from (SELECT t.agency_id
              FROM bas_agency_info t
             where t.mof_div_code = vrow.mof_div_code
               and t.agency_code like vrow.ele_code || '%'
               and length(t.agency_code) > length(vrow.ele_code)
               and t.is_leaf = 1
               and t.is_deleted = 2
             order by t.agency_code, t.create_time)
     where rownum <= 1;

    -- 将本级单位插入到临时表，作为部门信息进行修改的基础。
    insert into bas_agency_info_tmp_13 SELECT T.* FROM bas_agency_info t where t.agency_id = v_copy_agency_id;

    -- 更新需要插入的部门信息
    UPDATE bas_agency_info_tmp_13 T
       SET t.agency_id           = vrow.ele_id,
           t.audit_id            = '0',
           t.agency_code         = vrow.ele_code,
           t.agency_name         = vrow.ele_name,
           t.mof_div_code        = vrow.mof_div_code,
           t.agency_abbreviation = vrow.ele_name,
           t.biz_key             = vrow.ele_id,
           t.create_time         = vrow.create_time,
           t.end_date            = vrow.end_date,
           t.unifsoc_cred_code   = lpad('20231212' || v_i, 18, '0'),
           t.parent_id           = vrow.parent_id,
           t.start_date          = vrow.start_date,
           t.update_time         = to_date('2023-12-12 12:12:12', 'yyyy-mm-dd hh24:mi:ss'),
           t.level_no            = vrow.level_no,
           t.is_leaf             = vrow.is_leaf,
           t.is_enabled          = vrow.is_enabled,
           t.is_deleted          = vrow.is_deleted,
           t.is_end              = 1,
           t.is_last_inst        = 1,
           t.from_agency_id      = '0',
           t.create_user_name    = 'jcxxk_01',
           t.update_user_name    = 'jcxxk_01',
           t.fiscal_year         = '2024',
           t.version             = '2024rcdtwh',
           t.version_name        = '2024日常动态维护版',
           t.parent_version_id   = null
     WHERE t.agency_id = v_copy_agency_id;

     dbms_output.put_line(v_i || ' -> ' || vrow.ele_id || ' 开始 ' || v_copy_agency_id);
    v_i := v_i + 1;

    --提交生效
    commit;
  end loop;

  -- 将临时表中清洗好的部门信息一次性全部插入回部标info表
  insert into bas_agency_info  SELECT T.* FROM bas_agency_info_tmp_13 t  where t.agency_id != 'can_delete';
  commit;
end;

/
