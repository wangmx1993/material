// ==UserScript==
// @name         网页自定义倒计时
// @namespace    https://greasyfork.org/zh-CN/scripts/457839-%E7%BD%91%E9%A1%B5%E8%87%AA%E5%AE%9A%E4%B9%89%E5%80%92%E8%AE%A1%E6%97%B6
// @version      1.1
// @description  网页自定义倒计时-蚩尤后裔
// @author       蚩尤后裔
// @homepage     https://gitee.com/wangmx1993/material/blob/master/script/tampermonkey/%E7%BD%91%E9%A1%B5%E8%87%AA%E5%AE%9A%E4%B9%89%E5%80%92%E8%AE%A1%E6%97%B6-%E5%85%A5%E9%97%A8%E7%89%88%E6%9C%AC.js
// @match        http*://*/*
// @require      http://libs.baidu.com/jquery/2.0.0/jquery.min.js
// @grant        gm_log
// @license      MIT
// @updateurl    https://greasyfork.org/zh-CN/scripts/457839-%E7%BD%91%E9%A1%B5%E8%87%AA%E5%AE%9A%E4%B9%89%E5%80%92%E8%AE%A1%E6%97%B6
// ==/UserScript==

// 将本内容直接复制粘贴到油猴中即可使用.
(function () {
    'use strict';

    // 目标日期(yyyy,mth,dd,hh,mm,ss)，月份从0开始[0,11]，手动修改为实际日期即可
    let targetDate = new Date(2023, 0, 14, 8, 0, 0);

    /**
     * 向原始页面添加倒计时元素
     *
     * @param align_items ：对齐方式
     * * align-items: flex-start; 上对齐
     * * align-items: center; 中对齐
     * * align-items: flex-end; 下对齐
     */

    function appendCountDownEle(align_items) {
        let contDownJQ = $(`<div id="page-countdown"></div>`);
        contDownJQ.css({'position': 'fixed', 'top': 0, 'bottom': 0, 'left': 0, 'right': 0, 'display': 'flex'});
        contDownJQ.css({'justify-content': 'center', 'align-items': align_items});
        contDownJQ.css({'color': 'rgba(252,85,49, 0.5)', 'font-size': '5vw'});
        contDownJQ.css({'pointer-events': 'none', 'z-index': '9999'});
        $('body').append(contDownJQ);
    }

    /**
     * 开始倒计时，每隔1秒刷新一下
     */
    function countDown() {
        setInterval(() => {
            // 获取当前时间与目标时间的相差的秒数
            let diff = parseInt((targetDate - Date.now()) / 1000);
            let hour = parseInt(diff / 3600);
            let min = parseInt((diff / 60) % 60);
            let sec = parseInt(diff % 60);
            let day = 0;
            if (hour > 24) {
                day = parseInt(hour / 24);
                hour = hour % 24;
            }
            $("#page-countdown").text(`离春节回家还有：${day}天${hour}小时${min}分${sec}秒`);
            // document.querySelector('#page-countdown').innerHTML = `离春节回家还有：${day}天${hour}小时${min}分${sec}秒`;
        }, 1000);
    }

    /**
     * 控制某个元素不断的透明度降为0，然后透明度又升为1
     * @param jQueryObj  ：待操作的 Jquery对象
     * @param speed      ：速度
     * @param opacity    ：透明度
     * @param isShow     ：1表示显示，0表示隐藏
     */
    function hFlout(jQueryObj, speed, opacity, isShow) {
        jQueryObj.fadeTo(speed, opacity, function () {
            /** 当本次为隐藏时，下次应该为显示*/
            if (isShow == 0) {
                isShow = 1;
                opacity = 1;
            } else {
                /** 当本次为显示时，下次应该为隐藏*/
                isShow = 0;
                opacity = 0;
            }
            /**方法回调，循环动画*/
            hFlout(jQueryObj, speed, opacity, isShow);
        });
    }

    $(function () {
        // 向原始页面添加倒计时元素
        appendCountDownEle('flex-end');
        // 开始倒计时
        countDown();
        // 为倒计时加点动画效果
        hFlout($("#page-countdown"), 3000, 0, 0);
    });

})();