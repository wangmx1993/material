
--1、复制主表数据到临时表
create table bas_person_month_tmp_231017
as 
SELECT T.Per_Id,
       t.biz_key,
       t.iden_no,
       t.per_name,
       t.mof_div_code,
       t.fiscal_year,
       t.version,
       t.start_date,
       t.end_date,
       t.update_time,
       t.update_user_id,
       t.update_user_code,
       t.update_user_name,
       t.is_end,
       t.is_last_inst
  FROM bas_person_info T
 where t.mof_div_code='430000000'
   and t.fiscal_year = 2025
   and t.version = '2025rcdtwh'
   and t.agency_code='002888'
   and t.is_deleted = 2
   and not exists
 (SELECT 1 FROM bas_person_month T2 where t2.per_id = t.per_id);

--2、为临时表创建索引，加快第三步的速度
create index IND_bas_person_month_tmp_231017 on bas_person_month_tmp_231017 (per_id);

--3、为 bas_person_month 表生成新数据
MERGE INTO bas_person_month A
USING bas_person_month_tmp_231017 B
ON (A.per_id = B.per_id)
WHEN NOT MATCHED THEN
  INSERT
    (A.Per_Id,
     A.biz_key,
     A.iden_no,
     A.per_name,
     A.mof_div_code,
     A.fiscal_year,
     A.version,
     A.start_date,
     A.end_date,
     A.update_time,
     A.update_user_id,
     A.update_user_code,
     A.update_user_name,
     A.is_end,
     A.is_last_inst)
  VALUES
    (B.Per_Id,
     B.biz_key,
     B.iden_no,
     B.per_name,
     B.mof_div_code,
     B.fiscal_year,
     B.version,
     B.start_date,
     B.end_date,
     B.update_time,
     B.update_user_id,
     B.update_user_code,
     B.update_user_name,
     B.is_end,
     B.is_last_inst);


--4、删除临时表
drop table bas_person_month_tmp_231017;
