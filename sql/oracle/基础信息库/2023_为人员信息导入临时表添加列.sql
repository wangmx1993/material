CREATE OR REPLACE PROCEDURE PROC_BAS_PERSON_IMP_ADD_COL(add_column_name in varchar2,
                                                        add_column_type in varchar2) is

--创建存储过程，用于给人员信息导入临时表添加字段。
--因为人员信息导入的临时表分类比较细，导致比较多，所以借助存储过程新增字段，格式：BAS_PERSON_IMP_区划编码_UI-CODE

  add_column_is_exist number := 0; -- 待添加的列目标表中是否已经存在
  execute_sql         varchar2(1024) := ''; -- 自动拼接执行 SQL，如：Alter Table xxx Add xxx NUMBER(1);
  v_count             number := 0; --统计个数

BEGIN

  for vrow in (SELECT T.TABLE_NAME
                 FROM user_tables t
                where t.TABLE_NAME like 'BAS_PERSON_IMP_%'
                  and length(t.table_name) > 25
                  and instr(t.TABLE_NAME, '_', 1, 4) > 0) loop
    -- 如果表中已经存在待添加的列，则不再重复添加
    SELECT count(1)
      into add_column_is_exist
      FROM user_tab_columns t
     where t.TABLE_NAME = vrow.TABLE_NAME
       and t.COLUMN_NAME = upper(add_column_name);
    if add_column_is_exist <= 0 then
      -- 拼接 SQL
      execute_sql := 'ALTER TABLE ' || vrow.TABLE_NAME || ' ADD ' || add_column_name || ' ' || add_column_type;
      -- 执行 SQL
      execute immediate execute_sql;
      dbms_output.put_line((v_count) || '：' || vrow.TABLE_NAME || ' 添加成功：' || add_column_name || ' ' || add_column_type);
    else
      dbms_output.put_line((v_count) || '：' || vrow.TABLE_NAME || ' 已经存在 ' || add_column_name);
    end if;
    v_count := v_count + 1;
  end loop;
END;

/

--调用存储过程添加列

CALL PROC_BAS_PERSON_IMP_ADD_COL('INTERNAL_DEP_ID', 'VARCHAR2(38)');
CALL PROC_BAS_PERSON_IMP_ADD_COL('INTERNAL_DEP_CODE', 'VARCHAR2(30)');
CALL PROC_BAS_PERSON_IMP_ADD_COL('INTERNAL_DEP_NAME', 'VARCHAR2(90)');
CALL PROC_BAS_PERSON_IMP_ADD_COL('MOF_DIV_NAME', 'VARCHAR2(100)');
CALL PROC_BAS_PERSON_IMP_ADD_COL('REDUCE_REASON_ID', 'VARCHAR2(38)');
CALL PROC_BAS_PERSON_IMP_ADD_COL('REDUCE_REASON_CODE', 'VARCHAR2(100)');
CALL PROC_BAS_PERSON_IMP_ADD_COL('REDUCE_REASON_NAME', 'VARCHAR2(30)');
CALL PROC_BAS_PERSON_IMP_ADD_COL('IS_OTHER_RIG_EXP_PER', 'number(1)');
CALL PROC_BAS_PERSON_IMP_ADD_COL('IS_WHE_PAR_ENDO_INS', 'number(1)');
CALL PROC_BAS_PERSON_IMP_ADD_COL('IS_WHE_PAR_MED_INS', 'number(1)');
CALL PROC_BAS_PERSON_IMP_ADD_COL('IS_WHE_PAR_WORK_INS', 'number(1)');
CALL PROC_BAS_PERSON_IMP_ADD_COL('IS_LAST_INST', 'number(1)');
CALL PROC_BAS_PERSON_IMP_ADD_COL('ONE_MERIT_SER_BON', 'number(18,2)');
CALL PROC_BAS_PERSON_IMP_ADD_COL('PER_EXT_ID', 'VARCHAR2(38)');
CALL PROC_BAS_PERSON_IMP_ADD_COL('BASIC_PER_AWARD', 'number(18,2)');
CALL PROC_BAS_PERSON_IMP_ADD_COL('REGU_SUB', 'number(18,2)');
CALL PROC_BAS_PERSON_IMP_ADD_COL('REFORMATIVE_SUB', 'number(18,2)');
CALL PROC_BAS_PERSON_IMP_ADD_COL('ACHIEVEMENT_BONUS', 'number(18,2)');
CALL PROC_BAS_PERSON_IMP_ADD_COL('ANNUAL_ASS_AWARD', 'number(18,2)');
CALL PROC_BAS_PERSON_IMP_ADD_COL('YEAR_LUMP_SUNM_BON', 'number(18,2)');

