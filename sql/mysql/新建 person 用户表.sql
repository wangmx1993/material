-- 准备数据
DROP TABLE IF EXISTS person;
CREATE TABLE person (
  pId int(11) NOT NULL AUTO_INCREMENT,
  pName varchar(18) NOT NULL,
  id_card varchar(32) NOT NULL UNIQUE ,
  salary float(10,2) DEFAULT NULL,
  summary varchar(256) DEFAULT NULL,
  PRIMARY KEY (pId)
);

INSERT INTO person(pName,id_card,salary,summary) VALUES ('王五', '510104199603075397', '99.00', '重要人物');
INSERT INTO person(pName,id_card,salary,summary) VALUES ('马六', '460105199507071048', '8897.00', '白领');
INSERT INTO person(pName,id_card,salary,summary) VALUES ('张无忌', '110101199003077467', '9999.99', '武学奇才');
