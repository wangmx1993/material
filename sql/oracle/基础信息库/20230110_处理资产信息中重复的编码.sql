

--资产唯一编码，由系统自动生成.
--处理资产信息中重复的编码(asset_code)
declare
  cursor vrows is
  --查询重复的编码
    SELECT t.asset_code, count(1) coun
      FROM BAS_ASSET_INFO T
     where t.is_deleted = 2
     group by t.asset_code
    having count(1) > 1;
  v_index number := 1; --为重复编码重新拼接值，让其唯一
begin
  for vrow in vrows loop
    v_index := 1; --重置
    --根据重复编码查询数据的主键
    for vrow2 in (SELECT T.Asset_Id, t.asset_code
                    FROM BAS_ASSET_INFO T
                   where t.asset_code = vrow.asset_code
                     and t.is_deleted = 2) loop
      --根据主键更新重复的编码，在原有重复编码上除了拼 v_index，再拼一个随机值，防止重复。
      UPDATE BAS_ASSET_INFO T
         SET t.asset_code = vrow2.asset_code || v_index ||
                            abs(mod(dbms_random.random, 100))
       WHERE t.asset_id = vrow2.asset_id;
      v_index := v_index + 1;
    end loop;
    --每个重复编码提交一次
    commit;
  end loop;
end;
