package grp.basic3.common.controller;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import grp.basic.util.BSErrorCodeEnum;
import grp.basic.util.BSResultUtil;
import grp.basic3.se.model.JitConfigInfo;
import grp.framework.dao.FWRpDAO;
import grp.framework.util.SessionUtil;
import grp.pt.core.ReturnData;
import grp.pt.util.model.Session;
import grp.utils.Encryption3;
import grp.utils.JitEncryptionHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * 多线程处理数据库中的历史数据
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2024/3/11 14:04
 */
@RestController
@RequestMapping(value = "/jitEncrypt")
@Api(value = "JitEncryptController", tags = "湖南密评改造吉大正元运行期数据解析及历史数据处理")
@SuppressWarnings({ "rawtypes", "unchecked","Duplicates" })
public class JitEncryptController {

    private static final Logger log = LoggerFactory.getLogger(JitEncryptController.class);

    @Resource
    private JitEncryptionHelper jitEncryptionHelper;

    @Autowired
    private FWRpDAO rpDAO;

    @RequestMapping(value = "/encrypt/table/serial", method = RequestMethod.POST)
    @ApiOperation(value = "历史数据加密(单线程)，用于对表中已存储的数据进行加密。")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "type", value = "业务数据类型，1-单位信息，2-人员信息", dataType = "String", required = true, paramType = "query"),
            @ApiImplicitParam(name = "tableName", value = "表名(如BAS_AGENCY_INFO)", dataType = "String", required = true, paramType = "query"),
            @ApiImplicitParam(name = "mofDviCode", value = "区划编码(如43000000)", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "agencyCode", value = "单位编码(如001001)", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "version", value = "数据版本(如2024czgy)", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "rowNum", value = "每次循环查询处理的条数,默认1w条", defaultValue = "10000", dataType = "String", paramType = "query")})
    public ReturnData jitEncryptTableSerial(@RequestParam(value = "type") String type,
                                            @RequestParam(value = "tableName") String tableName,
                                            @RequestParam(value = "mofDviCode", required = false) String mofDviCode,
                                            @RequestParam(value = "agencyCode", required = false) String agencyCode,
                                            @RequestParam(value = "version", required = false) String version,
                                            @RequestParam(value = "rowNum", required = false, defaultValue = "10000") int rowNum) {
        long t1 = System.currentTimeMillis();
        log.info("历史数据加密(单线程)开始.");
        String typeAgency = "1";
        String typePerson = "2";
        if (!StrUtil.equals(type, typeAgency) && !StrUtil.equals(type, typePerson)) {
            return BSResultUtil.returnError(BSErrorCodeEnum.PARAM_ERROR, "业务数据类型Type值必须为：1-单位信息，2-人员信息！");
        }
        Session se = SessionUtil.getSession();
        tableName = tableName.toUpperCase();
        version = StrUtil.isBlank(version) ? version : version.toLowerCase();

        /**1、查询吉大正元加密配置*/
        Set<String> encryptFields = getEncryptFields(se, type, typeAgency, typePerson);
        if (ObjectUtil.isEmpty(encryptFields)) {
            return BSResultUtil.returnError(BSErrorCodeEnum.ERROR, "加密字段配置为空。");
        }
        /**2、对于配置中有，而表中实际没有的字段进行移除，比如工资系统的 ic_id 在基础库表中是没有的*/
        removeInvalidFields(tableName, encryptFields);
        if (ObjectUtil.isEmpty(encryptFields)) {
            return BSResultUtil.returnError(BSErrorCodeEnum.ERROR, "目标表中没有对应的加密字段，请检查配置是否正确？");
        }
        /**2、根据 is_jit_encrypt(标识数据是否已经进行了吉大正元加密,1是)标识字段循环查询业务数据，每次1万条.*/
        Set<String> selectFields = new LinkedHashSet<>();
        selectFields.addAll(encryptFields);
        if (StrUtil.equals(typePerson, type)) {
            // 人员信息时，同时将证件类型也查询出来，后续需要进行旧方式的解密
            selectFields.add("iden_type_code");
            selectFields.add("iden_type_name");
        }
        // 总执行条数
        AtomicInteger totalExecuteCount = new AtomicInteger();
        // 统计失败次数
        // 当执行子线程任务失败次数连续超过10次时，退出循环，防止因为网络问题、或者其它未知问题，导致死循环.
        AtomicInteger continuousFailCount = new AtomicInteger(0);
        int maxFailCount = 10;
        while (true) {
            List<Map<String, Object>> dataList = getDataList(tableName, selectFields, mofDviCode, agencyCode, version, rowNum);
            if (ObjectUtil.isEmpty(dataList)) {
                break;
            }
            /**3、使用旧方式对身份证先进行解密(单位信息不需要)*/
            if (StrUtil.equals(typePerson, type)) {
                encryptIdenNos(dataList);
            }
            /**4、然后使用吉大正元进行加密*/
            boolean isSync = false;
            try {
                if (StrUtil.equals(typeAgency, type)) {
                    isSync = jitEncryptionHelper.jitBatchEncryptAgency(se, dataList, false);
                } else if (StrUtil.equals(typePerson, type)) {
                    isSync = jitEncryptionHelper.jitBatchEncryptPerson(se, dataList, false);
                }
                if (isSync) {
                    /**5、将重新加密后的结果更新到库中，同时将标记 is_jit_encrypt 置为 1 */
                    updateEncryptResult2DB(tableName, encryptFields, dataList);
                    totalExecuteCount.addAndGet(dataList.size());
                    continuousFailCount.set(0);
                } else {
                    continuousFailCount.addAndGet(1);
                }
            } catch (Exception e) {
                continuousFailCount.addAndGet(1);
                log.error("加密失败：{}", ExceptionUtils.getStackTrace(e));
            }
            if (continuousFailCount.get() >= maxFailCount) {
                log.warn("历史数据加密(单线程)任务失败次数已连续超过{}次，不再继续执行。", maxFailCount);
                break;
            }
        }
        log.info("历史数据加密(单线程)结束，耗时={}.", System.currentTimeMillis() - t1);
        return BSResultUtil.returnSuccess(BSErrorCodeEnum.SUCCESS, "操作完成，共处理" + totalExecuteCount.get() + "条，耗时" + (System.currentTimeMillis() - t1) + "毫秒。");
    }

    // ============下面多线程接口执行时打印的日志如下============
    // 2024-03-12 17:36:25.043[http-nio-9000-exec-2][INFO ][grp.basic3.common.controller.JitEncryptController.jitEncryptTableParallel(JitEncryptController.java:195)]:历史数据加密(多线程)开始.
    // 2024-03-12 17:36:25.047[http-nio-9000-exec-2][INFO ][grp.basic3.common.controller.JitEncryptController.getEncryptFields(JitEncryptController.java:305)]:查询吉大正元加密配置中的加密字段信息：[iden_no, ic_id]
    // 2024-03-12 17:36:25.059[http-nio-9000-exec-2][INFO ][grp.basic3.common.controller.JitEncryptController.removeInvalidFields(JitEncryptController.java:395)]:对配置中有而表中实际没有的字段进行移除:ic_id
    // 2024-03-12 17:36:25.070[http-nio-9000-exec-2][INFO ][grp.basic3.common.controller.JitEncryptController.getDataList(JitEncryptController.java:376)]:查询目标表中需要被加密的历史数据：SELECT iden_no,iden_type_code,iden_type_name, rowid FROM BAS_PERSON_INFO t WHERE mof_div_code=? AND  nvl(is_jit_encrypt, 2) = 2 and rownum <=?  ;--args=[430100000, 20000] 返回条数=192 影响时间=11
    // 2024-03-12 17:36:25.071[历史数据加密1-39][INFO ][grp.basic3.common.controller.JitEncryptController.encryptIdenNos(JitEncryptController.java:334)]:使用旧方式对身份证先进行解密:39 条.
    // 2024-03-12 17:36:25.071[历史数据加密2-39][INFO ][grp.basic3.common.controller.JitEncryptController.encryptIdenNos(JitEncryptController.java:334)]:使用旧方式对身份证先进行解密:39 条.
    // 2024-03-12 17:36:25.071[历史数据加密3-39][INFO ][grp.basic3.common.controller.JitEncryptController.encryptIdenNos(JitEncryptController.java:334)]:使用旧方式对身份证先进行解密:39 条.
    // 2024-03-12 17:36:25.071[历史数据加密4-39][INFO ][grp.basic3.common.controller.JitEncryptController.encryptIdenNos(JitEncryptController.java:334)]:使用旧方式对身份证先进行解密:39 条.
    // 2024-03-12 17:36:25.071[历史数据加密5-36][INFO ][grp.basic3.common.controller.JitEncryptController.encryptIdenNos(JitEncryptController.java:334)]:使用旧方式对身份证先进行解密:36 条.
    // 2024-03-12 17:36:25.159[历史数据加密4-39][INFO ][grp.basic3.common.controller.JitEncryptController.updateEncryptResult2DB(JitEncryptController.java:284)]:将加密结果批量更新到库中:UPDATE BAS_PERSON_INFO SET iden_no=?,is_jit_encrypt=1  WHERE rowid=?  ;--args=[iden_no, rowid] 影响条数=39
    // 2024-03-12 17:36:25.225[历史数据加密3-39][INFO ][grp.basic3.common.controller.JitEncryptController.updateEncryptResult2DB(JitEncryptController.java:284)]:将加密结果批量更新到库中:UPDATE BAS_PERSON_INFO SET iden_no=?,is_jit_encrypt=1  WHERE rowid=?  ;--args=[iden_no, rowid] 影响条数=39
    // 2024-03-12 17:36:25.225[历史数据加密2-39][INFO ][grp.basic3.common.controller.JitEncryptController.updateEncryptResult2DB(JitEncryptController.java:284)]:将加密结果批量更新到库中:UPDATE BAS_PERSON_INFO SET iden_no=?,is_jit_encrypt=1  WHERE rowid=?  ;--args=[iden_no, rowid] 影响条数=39
    // 2024-03-12 17:36:25.225[历史数据加密1-39][INFO ][grp.basic3.common.controller.JitEncryptController.updateEncryptResult2DB(JitEncryptController.java:284)]:将加密结果批量更新到库中:UPDATE BAS_PERSON_INFO SET iden_no=?,is_jit_encrypt=1  WHERE rowid=?  ;--args=[iden_no, rowid] 影响条数=39
    // 2024-03-12 17:36:25.225[历史数据加密5-36][INFO ][grp.basic3.common.controller.JitEncryptController.updateEncryptResult2DB(JitEncryptController.java:284)]:将加密结果批量更新到库中:UPDATE BAS_PERSON_INFO SET iden_no=?,is_jit_encrypt=1  WHERE rowid=?  ;--args=[iden_no, rowid] 影响条数=36
    // 2024-03-12 17:36:25.231[http-nio-9000-exec-2][INFO ][grp.basic3.common.controller.JitEncryptController.getDataList(JitEncryptController.java:376)]:查询目标表中需要被加密的历史数据：SELECT iden_no,iden_type_code,iden_type_name, rowid FROM BAS_PERSON_INFO t WHERE mof_div_code=? AND  nvl(is_jit_encrypt, 2) = 2 and rownum <=?  ;--args=[430100000, 20000] 返回条数=0 影响时间=5
    // 2024-03-12 17:36:25.232[http-nio-9000-exec-2][INFO ][grp.basic3.common.controller.JitEncryptController.jitEncryptTableParallel(JitEncryptController.java:258)]:历史数据加密(多线程)结束，耗时=188.

    @RequestMapping(value = "/encrypt/table/parallel", method = RequestMethod.POST)
    @ApiOperation(value = "历史数据加密(多线程)，用于对表中已存储的数据进行加密。")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "type", value = "业务数据类型，1-单位信息，2-人员信息", dataType = "String", required = true, paramType = "query"),
            @ApiImplicitParam(name = "tableName", value = "表名(如BAS_AGENCY_INFO)", dataType = "String", required = true, paramType = "query"),
            @ApiImplicitParam(name = "mofDviCode", value = "区划编码(如43000000)", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "threads", value = "线程数量,默认5", defaultValue = "5", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "rowNum", value = "每次循环查询处理的条数,默认2w条", defaultValue = "20000", dataType = "String", paramType = "query")})
    public ReturnData jitEncryptTableParallel(@RequestParam(value = "type") String type,
                                              @RequestParam(value = "tableName") String tableName,
                                              @RequestParam(value = "mofDviCode", required = false) String mofDviCode,
                                              @RequestParam(value = "threads", required = false, defaultValue = "5") int threads,
                                              @RequestParam(value = "rowNum", required = false, defaultValue = "20000") int rowNum) {
        long t1 = System.currentTimeMillis();
        log.info("历史数据加密(多线程)开始.");
        String typeAgency = "1";
        String typePerson = "2";
        threads = threads <= 0 ? 5 : threads;
        rowNum = rowNum <= 0 ? 20000 : rowNum;
        if (!StrUtil.equals(type, typeAgency) && !StrUtil.equals(type, typePerson)) {
            return BSResultUtil.returnError(BSErrorCodeEnum.PARAM_ERROR, "业务数据类型Type值必须为：1-单位信息，2-人员信息！");
        }
        Session se = SessionUtil.getSession();
        tableName = tableName.toUpperCase();

        /**1、查询吉大正元加密配置*/
        Set<String> encryptFields = getEncryptFields(se, type, typeAgency, typePerson);
        if (ObjectUtil.isEmpty(encryptFields)) {
            return BSResultUtil.returnError(BSErrorCodeEnum.ERROR, "加密字段配置为空。");
        }
        /**2、对于配置中有，而表中实际没有的字段进行移除，比如工资系统的 ic_id 在基础库表中是没有的*/
        removeInvalidFields(tableName, encryptFields);
        if (ObjectUtil.isEmpty(encryptFields)) {
            return BSResultUtil.returnError(BSErrorCodeEnum.ERROR, "目标表中没有对应的加密字段，请检查配置是否正确？");
        }
        /**2、根据 is_jit_encrypt(标识数据是否已经进行了吉大正元加密,1是)标识字段循环查询业务数据，每次1万条.*/
        Set<String> selectFields = new LinkedHashSet<>();
        selectFields.addAll(encryptFields);
        if (StrUtil.equals(typePerson, type)) {
            // 人员信息时，同时将证件类型也查询出来，后续需要进行旧方式的解密
            selectFields.add("iden_type_code");
            selectFields.add("iden_type_name");
        }
        // 总执行条数
        AtomicInteger totalExecuteCount = new AtomicInteger();
        // 统计失败次数
        // 当执行子线程任务失败次数连续超过10次时，退出循环，防止因为网络问题、或者其它未知问题，导致死循环.
        AtomicInteger continuousFailCount = new AtomicInteger(0);
        int maxFailCount = 10;
        while (true) {
            List<Map<String, Object>> dataList = getDataList(tableName, selectFields, mofDviCode, "", "", rowNum);
            if (ObjectUtil.isEmpty(dataList)) {
                break;
            }
            CountDownLatch countDownLatch = new CountDownLatch(threads);
            int pageSize = dataList.size() / threads + (dataList.size() % threads > 0 ? 1 : 0);
            List<List<Map<String, Object>>> partitions = ListUtils.partition(dataList, pageSize);
            for (int i = 0; i < threads; i++) {
                List<Map<String, Object>> mapList = (i <= partitions.size() - 1) ? partitions.get(i) : new ArrayList<>();
                if (ObjectUtil.isEmpty(mapList)) {
                    countDownLatch.countDown();
                    continue;
                }
                JitEncryptRunnable jitEncryptRunnable = new JitEncryptRunnable(se, continuousFailCount, countDownLatch, tableName, encryptFields, type, typeAgency, typePerson, mapList);
                jitEncryptRunnable.setName("历史数据加密" + (i + 1) + "-" + mapList.size());
                jitEncryptRunnable.start();
            }
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                String stackTrace = ExceptionUtils.getStackTrace(e);
                log.error("历史数据加密(多线程)执行失败：{}", stackTrace);
                return BSResultUtil.returnError(BSErrorCodeEnum.ERROR, "历史数据加密(多线程)执行失败：" + stackTrace);
            }
            if (continuousFailCount.get() >= maxFailCount) {
                log.warn("历史数据加密(多线程)任务失败次数已连续超过{}次，不再继续执行。", maxFailCount);
                return BSResultUtil.returnError(BSErrorCodeEnum.ERROR, "历史数据加密(多线程)任务失败次数已连续超过" + maxFailCount + "次，不再继续执行");
            }
            totalExecuteCount.addAndGet(dataList.size());
        }
        log.info("历史数据加密(多线程)结束，共处理{}条 耗时={}.", totalExecuteCount.get(), System.currentTimeMillis() - t1);
        return BSResultUtil.returnSuccess(BSErrorCodeEnum.SUCCESS, "操作成功，共处理" + totalExecuteCount.get() + "条，耗时"+ (System.currentTimeMillis() - t1) + "毫秒。");
    }

    /**
     * 将重新加密后的结果更新到库中，同时将标记 is_jit_encrypt 置为 1
     *
     * @param tableName
     * @param encryptFields
     * @param dataList
     */
    private void updateEncryptResult2DB(@RequestParam("tableName") String tableName,
                                        Set<String> encryptFields,
                                        List<Map<String, Object>> dataList) {
        StringBuilder updateSql = new StringBuilder("UPDATE ");
        List<Object> updateParams = new ArrayList<>();
        updateSql.append(tableName).append(" SET ");
        for (String encryptField : encryptFields) {
            updateSql.append(encryptField).append("=?,");
            updateParams.add(encryptField);
        }
        updateSql.append("is_jit_encrypt=1 ");
        updateSql.append(" WHERE rowid=? ");
        updateParams.add("rowid");
        int[] ints = rpDAO.batchExecute(updateSql.toString(), updateParams.toArray(new String[0]), dataList);
        int sum = Arrays.stream(ints).sum();
        log.info("将加密结果批量更新到库中:{} ;--args={} 影响条数={}", updateSql, updateParams, sum);
    }

    /**
     * 查询吉大正元加密配置中的加密字段信息
     *
     * @param se
     * @param type
     * @param typeAgency
     * @param typePerson
     * @return
     */
    private Set<String> getEncryptFields(Session se, @RequestParam("type") String type, String typeAgency, String typePerson) {
        Set<String> encryptFields = new LinkedHashSet<>();
        JitConfigInfo jitConfigInfo = jitEncryptionHelper.getJitConfigInfo(se);
        if (StrUtil.equals(typeAgency, type)) {
            encryptFields.addAll(jitConfigInfo.listAgencyEncryptField());

        } else if (StrUtil.equals(typePerson, type)) {
            encryptFields.addAll(jitConfigInfo.listPersonEncryptField());
        }
        log.info("查询吉大正元加密配置中的加密字段信息：{}", encryptFields);
        return encryptFields;
    }

    /**
     * 使用旧方式对身份证先进行解密(单位信息不需要)
     *
     * @param dataList
     */
    public static void encryptIdenNos(List<Map<String, Object>> dataList) {
        // 数据为空时，跳过.
        if (ObjectUtils.isEmpty(dataList)) {
            return;
        }
        for (Map<String, Object> dataMap : dataList) {
            // 暂时只对身份证号码进行加密，其他证件类型跳过
            Object iden_type_code = dataMap.get("iden_type_code");
            Object iden_type_name = dataMap.get("iden_type_name");
            if (StringUtils.equals("01", String.valueOf(iden_type_code))
                    || StringUtils.contains(String.valueOf(iden_type_code), "身份证")
                    || StringUtils.contains(String.valueOf(iden_type_name), "身份证")) {
                // 解密身份证
                for (String fieldName : Encryption3.fields) {
                    if (dataMap.containsKey(fieldName) && dataMap.get(fieldName) != null) {
                        dataMap.put(fieldName, Encryption3.encrypt(dataMap.get(fieldName).toString()));
                    }
                }
            }
        }
        log.info("使用旧方式对身份证先进行解密:{} 条.", dataList.size());
    }

    /**
     * 根据 is_jit_encrypt(标识数据是否已经进行了吉大正元加密,1是)标识字段循环查询业务数据，每次1万条.
     *
     * @param tableName
     * @param selectFields
     * @param mofDviCode
     * @param agencyCode
     * @param version
     * @param rowNum       ：每次查询处理的条数
     * @return
     */
    private List<Map<String, Object>> getDataList(@RequestParam("tableName") String tableName,
                                                  Set<String> selectFields,
                                                  @RequestParam(value = "mofDviCode", required = false) String mofDviCode,
                                                  @RequestParam(value = "agencyCode", required = false) String agencyCode,
                                                  @RequestParam(value = "version", required = false) String version,
                                                  int rowNum) {
        long t1 = System.currentTimeMillis();
        StringBuilder queryDataSql = new StringBuilder("SELECT ");
        List<Object> queryDataArgs = new ArrayList<>();
        for (String field : selectFields) {
            queryDataSql.append(field).append(",");
        }
        queryDataSql.append(" rowid FROM ").append(tableName).append(" t WHERE ");
        if (StrUtil.isNotBlank(agencyCode) && StrUtil.isNotBlank(mofDviCode)) {
            queryDataSql.append("agency_code=? AND ");
            queryDataArgs.add(agencyCode);
        }
        if (StrUtil.isNotBlank(mofDviCode)) {
            queryDataSql.append("mof_div_code=? AND ");
            queryDataArgs.add(mofDviCode);
        }
        if (StrUtil.isNotBlank(version)) {
            queryDataSql.append("version=? AND ");
            queryDataArgs.add(version);
        }
        queryDataSql.append(" nvl(is_jit_encrypt, 2) = 2 and rownum <=? ");
        queryDataArgs.add(rowNum);
        List<Map<String, Object>> dataList = rpDAO.queryForList(queryDataSql.toString(), queryDataArgs.toArray());
        log.info("查询目标表中需要被加密的历史数据：{} ;--args={} 返回条数={} 影响时间={}", queryDataSql, queryDataArgs, dataList.size(), System.currentTimeMillis() - t1);
        return dataList;
    }

    /**
     * 对于配置中有，而表中实际没有的字段进行移除，比如工资系统的 ic_id 在基础库表中是没有的
     *
     * @param tableName
     * @param fields
     */
    private void removeInvalidFields(@RequestParam("tableName") String tableName, Set<String> fields) {
        String queryColumns = "SELECT t.COLUMN_NAME FROM user_tab_columns t where t.TABLE_NAME=? ";
        List<Map<String, Object>> columns = rpDAO.queryForList(queryColumns, new Object[]{tableName});
        List<String> columnNames = columns.stream().map(item -> MapUtil.getStr(item, "column_name").toLowerCase()).collect(Collectors.toList());
        Iterator<String> iterator = fields.iterator();
        while (iterator.hasNext()) {
            String next = iterator.next();
            if (!columnNames.contains(next)) {
                iterator.remove();
                log.info("对配置中有而表中实际没有的字段进行移除:{}", next);
            }
        }
    }

    private class JitEncryptRunnable extends Thread {
        private Session se;
        private AtomicInteger continuousFailCount;
        private CountDownLatch countDownLatch;
        private String tableName;
        private Set<String> encryptFields;
        private String type;
        private String typeAgency;
        private String typePerson;
        private List<Map<String, Object>> dataList;

        public JitEncryptRunnable(Session se,
                                  AtomicInteger continuousFailCount,
                                  CountDownLatch countDownLatch,
                                  String tableName,
                                  Set<String> encryptFields,
                                  String type,
                                  String typeAgency,
                                  String typePerson,
                                  List<Map<String, Object>> dataList) {
            this.se = se;
            this.continuousFailCount = continuousFailCount;
            this.countDownLatch = countDownLatch;
            this.tableName = tableName;
            this.encryptFields = encryptFields;
            this.type = type;
            this.typeAgency = typeAgency;
            this.typePerson = typePerson;
            this.dataList = dataList;
        }

        @Override
        public void run() {
            try {
                /**使用旧方式对身份证先进行解密(单位信息不需要)*/
                if (StrUtil.equals(typePerson, type)) {
                    encryptIdenNos(dataList);
                }
                /**然后使用吉大正元进行加密*/
                boolean isSync = false;
                if (StrUtil.equals(typeAgency, type)) {
                    isSync = jitEncryptionHelper.jitBatchEncryptAgency(se, dataList, false);
                } else if (StrUtil.equals(typePerson, type)) {
                    isSync = jitEncryptionHelper.jitBatchEncryptPerson(se, dataList, false);
                }
                if (isSync) {
                    /**将重新加密后的结果更新到库中，同时将标记 is_jit_encrypt 置为 1 */
                    updateEncryptResult2DB(tableName, encryptFields, dataList);
                    continuousFailCount.set(0);
                } else {
                    continuousFailCount.addAndGet(1);
                }
            } catch (Exception e){
                continuousFailCount.addAndGet(1);
                throw e;
            } finally {
                countDownLatch.countDown();
            }
        }
    }

}
