CREATE OR REPLACE PROCEDURE PROC_BAS_TAB_ADD_COL_WMX(p_add_tab_name     IN VARCHAR2, --待添加的表名[必填]，如 dept，不区分大小写.
                                                     p_add_col_name     IN VARCHAR2, --待添加的列名[必填]，如 loc，不区分大小写.
                                                     p_add_col_type     IN VARCHAR2, --待添加的列类型[必填]，如 varcahr2(64)
                                                     p_add_col_comments IN VARCHAR2 --待添加的列注释[可选]，如 位置
                                                     ) IS
  --用于给指定的表添加指定的字段，支持设置注释
  --表不存在时，抛出异常；字段已存在时，不再添加。
  --@authot WangMaoXiong
  --@date 2024/09/10

  v_add_tab_is_exist NUMBER := 0; -- 待添加的表是否存在
  v_add_col_is_exist NUMBER := 0; -- 待添加的列是否存在
  v_execute_sql      VARCHAR2(1024) := ''; -- 自动拼接并执行的SQL，如：Alter Table xxx Add xxx NUMBER(1);
BEGIN
SELECT COUNT(1)
INTO v_add_tab_is_exist
FROM user_tables t
WHERE t.TABLE_NAME = UPPER(TRIM(p_add_tab_name));
IF v_add_tab_is_exist <= 0 THEN
    raise_application_error(-20003,
                            '待添加字段的表不存在，请检查：' || p_add_tab_name);
END IF;

SELECT COUNT(1)
INTO v_add_col_is_exist
FROM user_tab_columns t
WHERE t.TABLE_NAME = UPPER(TRIM(p_add_tab_name))
  AND t.COLUMN_NAME = upper(TRIM(p_add_col_name));
IF v_add_col_is_exist <= 0 THEN
    v_execute_sql := 'ALTER TABLE ' || p_add_tab_name || ' ADD ' ||
                     p_add_col_name || ' ' || p_add_col_type;
EXECUTE IMMEDIATE v_execute_sql;
IF p_add_col_comments IS NOT NULL AND
       LENGTH(TRIM(p_add_col_comments)) > 0 THEN
      v_execute_sql := 'comment on column ' || TRIM(p_add_tab_name) || '.' ||
                       TRIM(p_add_col_name) || ' is ''' ||
                       p_add_col_comments || '''';
EXECUTE IMMEDIATE v_execute_sql;
END IF;
END IF;
END;


CALL PROC_BAS_TAB_ADD_COL_WMX('BAS_AGENCY_INFO', 'ACCT_SET_ID', 'VARCHAR2(60)', '');
CALL PROC_BAS_TAB_ADD_COL_WMX('BAS_PERSON_INFO', 'IS_PFLAG2', 'NUMBER(1)', '备用字段2 1是2否');

