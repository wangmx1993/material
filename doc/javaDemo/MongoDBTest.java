import com.mongodb.*;
import com.mongodb.client.ListDatabasesIterable;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * MongoDB 数据库测试---包括 MongoDB 服务端连接，数据库新建、查询、删除 等
 * Created by Administrator on 2018/9/17 0017.
 *
 * @author wangMaoXiong
 */
public class MongoDBTest {

    /**
     * 当 MongoDB 服务器没进行安全认证时，则客户端可以免密连接。
     * 连接没有进行安全认证的 MongoDB 服务器
     *
     * @return
     */
    public static MongoClient getMongoClientNoCheck() {
        /**MongoClient 是线程安全的，可以在多个线程中共享同一个实例
         * 一个 MongoClient 相当于一个客户端，一个客户端可以有多个连接*/
        MongoClient mongoClient = null;
        try {
            /** new MongoClient 创建客户端的时候，可以传入 MongoClientOptions 客户端配置选项
             * 所以可以将设置全部事先设置好
             */
            MongoClientOptions.Builder build = new MongoClientOptions.Builder();
            /**与目标数据库能够建立的最大连接数为50*/
            build.connectionsPerHost(50);

            /**如果当前所有的连接都在使用中，则每个连接上可以有50个线程排队等待*/
            build.threadsAllowedToBlockForConnectionMultiplier(50);

            /**一个线程访问数据库的时候，在成功获取到一个可用数据库连接之前的最长等待时间为，此处为 2分钟
             * 如果超过 maxWaitTime 都没有获取到连接的话，该线程就会抛出 Exception
             * */
            build.maxWaitTime(1000 * 60 * 2);

            /**设置与数据库建立连接时最长时间为1分钟*/
            build.connectTimeout(1000 * 60 * 1);
            MongoClientOptions mongoClientOptions = build.build();

            /** 将 MongoDB 服务器的 ip 与端口先封装好
             * 连接 MongoDB 服务端地址，实际项目中应该放到配置文件进行配置
             * */
            ServerAddress serverAddress = new ServerAddress("localhost", 27017);

            /**
             * 通过 ServerAddress 与 MongoClientOptions 创建连接到 MongoDB 的数据库实例
             * MongoClient(String host, int port)：
             *      1）host：MongoDB 服务端 IP
             *      2）port：MongoDB 服务端 端口，默认为 27017
             *      3）即使 MongoDB 服务端关闭，此时也不会抛出异常，只有到真正调用方法是才会
             *      4）连接 MongoDB 服务端地址，实际项目中应该放到配置文件进行配置
             * MongoClient(final ServerAddress addr, final MongoClientOptions options)
             * 重载了很多构造方法，这只是其中两个常用的
             *      */
            mongoClient = new MongoClient(serverAddress, mongoClientOptions);
        } catch (MongoException e) {
            e.printStackTrace();
        }
        return mongoClient;
    }

    /**
     * 如果 MongoDB 开启安全认证，则连接时需要认证。
     * 连接进行安全认证的 MongoDB 服务器，此时需要验证账号密码
     * 1）注意：如果 MongoDB 服务器未开启安全认证，则即使连接的时候使用了 账号密码，则也不受影响，同样成功
     *
     * @return
     */
    public static MongoClient getMongoClientCheck() {
        /**MongoClient 是线程安全的，可以在多个线程中共享同一个实例
         * 一个 MongoClient 相当于一个客户端，一个客户端可以有多个连接
         * */
        MongoClient mongoClient = null;
        try {
            /** new MongoClient 创建客户端的时候，可以传入 MongoClientOptions 客户端配置选项
             * 所以可以将设置全部事先设置好
             */
            MongoClientOptions.Builder build = new MongoClientOptions.Builder();
            /**与目标数据库能够建立的最大连接数为50*/
            build.connectionsPerHost(50);

            /**如果当前所有的连接都在使用中，则每个连接上可以有50个线程排队等待*/
            build.threadsAllowedToBlockForConnectionMultiplier(50);

            /**一个线程访问数据库的时候，在成功获取到一个可用数据库连接之前的最长等待时间为，此处为 2分钟
             * 如果超过 maxWaitTime 都没有获取到连接的话，该线程就会抛出 Exception
             * */
            build.maxWaitTime(1000 * 60 * 2);

            /**设置与数据库建立连接时最长时间为1分钟*/
            build.connectTimeout(1000 * 60 * 1);
            MongoClientOptions mongoClientOptions = build.build();

            /** 将 MongoDB 服务器的 ip 与端口先封装好
             * 连接 MongoDB 服务端地址，实际项目中应该放到配置文件进行配置
             * */
            ServerAddress serverAddress = new ServerAddress("localhost", 27017);

            /** MongoCredential：表示 MongoDB 凭据、证书
             * createScramSha1Credential(final String userName, final String source, final char[] password)
             *      1）userName：登录的用户名
             *      2）source：用户需要验证的数据库名称，注意账号当时在哪个数据库下创建，则此时就去哪个库下面进行验证，否则即使账号密码正确也无济于事
             *      3）password：用户的密码
             *      4）实际开发中也应该放到配置文件中进行配置
             * 同理还有：
             * createCredential(final String userName, final String database, final char[] password)
             * createScramSha256Credential(final String userName, final String source, final char[] password)
             * createMongoCRCredential(final String userName, final String database, final char[] password)
             * createMongoX509Credential(final String userName)
             * createMongoX509Credential()
             * createPlainCredential(final String userName, final String source, final char[] password)
             * createGSSAPICredential(final String userName)
             * A、如果 MongoDB 服务端未开启安全认证，这里设置的账号密码连接时也不受影响，同样连接成功
             * B、如果 MongoDB 服务端开启了安全认证，但是账号密码是错误的，则此时不会里面抛异常，等到正在 CRUD 时就会抛异常：Exception authenticating
             * C、如下所示，这是事项在 admin 数据库中创建好的 管理员账号 root
             */
            MongoCredential credential = MongoCredential.createCredential(
                    "root", "admin", "root".toCharArray());
            /** MongoClient(final ServerAddress addr, final MongoCredential credential, final MongoClientOptions options)
             * 1）addr：MongoDB 服务器地址
             * 2）credential：MongoDB 安全认证证书
             * 3）options：MongoDB 客户端配置选项
             */
            mongoClient = new MongoClient(serverAddress, credential, mongoClientOptions);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return mongoClient;
    }

    /**
     * 获取 MongoDB 的一些信息
     * 可以使用 com.mongodb.MongoClient 的 API 来获取一些信息
     */
    public static void showMongoDBInfo() {
        /** MongoClient(String host, int port)：直接指定 MongoDB IP 与端口进行连接
         * 实际开发中应该将 MongoDB 服务器地址配置在配置文件中*/
        MongoClient mongoClient = new MongoClient("127.0.0.1", 27017);
        ServerAddress serverAddress = mongoClient.getAddress();
        System.out.println("serverAddress>>>" + serverAddress);//输出：127.0.0.1:27017

        String connectPoint = mongoClient.getConnectPoint();
        System.out.println("connectPoint>>>" + connectPoint);

        int bsonObjectSize = mongoClient.getMaxBsonObjectSize();
        System.out.println("bsonObjectSize>>>" + bsonObjectSize);

        mongoClient.close();
    }

    /**
     * 删除指定数据库
     * 使用 com.mongodb.client.MongoDatabase#drop() 进行删除
     *
     * @param databaseName 被删除数据库名称，存在时会被删除(连同数据库中的数据一并删除)；不存在时不受影响
     */
    public static void dropDatabase(String databaseName) {
        if (databaseName != null && !"".equals(databaseName)) {
            /** MongoClient(String host, int port)：直接指定 MongoDB IP 与端口进行连接
             * 实际应用中应该将 MongoDB 服务器地址配置在配置文件中*/
            MongoClient mongoClient = new MongoClient("127.0.0.1", 27017);

            /**getDatabase(String databaseName)：获取指定的数据库
             * 如果此数据库不存在，则会自动创建，此时存在内存中，服务器不会存在真实的数据库文件，show dbs 命令 看不到
             * 如果再往其中添加数据，服务器则会生成数据库文件，磁盘中会真实存在，show dbs 命令 可以看到
             *
             * 注意 MongoDatabase 相当于一个 MongoDB 连接，连接可以有多个
             * MongoClient 相当于一个客户端，客户端可以只有一个，也可有多个
             * */
            MongoDatabase mongoDatabase = mongoClient.getDatabase(databaseName);

            /**删除当前所在的数据库
             * 1）即使数据库中有集合，集合中有文档，整个数据库都会删除，show dbs 不会再有
             * 2）如果待删除的数据库实际没有存在，即 show dbs 看不到，也不影响，不抛异常
             *
             * 也可以使用 MongoClient 的 dropDatabase(String dbName) 方法进行删除
             */
            mongoDatabase.drop();

            /**关闭 MongoDB 客户端连接，释放资源*/
            mongoClient.close();
        }
    }

    /**
     * 删除指定数据库
     * 使用 com.mongodb.Mongo#dropDatabase(java.lang.String) 进行删除
     *
     * @param databaseName 被数据库的名称，存在时，连同数据库中的内容一并删除，不存在时不受影响
     */
    public static void delDatabase(String databaseName) {
        if (databaseName != null && !"".equals(databaseName)) {
            /** MongoClient(String host, int port)：直接指定 MongoDB IP 与端口进行连接
             * 实际开发中应该将 MongoDB 服务器地址配置在配置文件中*/
            MongoClient mongoClient = new MongoClient("127.0.0.1", 27017);

            /** dropDatabase(String dbName) :删除 MongoDB 下的指定数据库
             * 数据库中内容全部会被删除，show dbs 也不会再有
             * 也可以使用 MongoDatabase 的 drop() 方法，删除当前数据库
             * 方法一执行，数据库 show dbs 就不会再有
             * */
            mongoClient.dropDatabase(databaseName);

            /**关闭 MongoDB 客户端连接，释放资源*/
            mongoClient.close();
        }
    }

    /**
     * 获取 MongoDB 服务端所有数据库名字
     * 使用：com.mongodb.MongoClient#listDatabaseNames()
     *
     * @return
     */
    public static List<String> findAllDBNames() {
        List<String> dbNameList = new ArrayList<String>();
        /** MongoClient(String host, int port)：直接指定 MongoDB IP 与端口进行连接
         * 实际开发中应该将 MongoDB 服务器地址配置在配置文件中*/
        MongoClient mongoClient = new MongoClient("127.0.0.1", 27017);

        /**getDatabase(String databaseName)：获取指定的数据库
         * 如果此数据库不存在，则会自动创建，此时存在内存中，服务器不会存在真实的数据库文件，show dbs 命令 看不到
         * 如果再往其中添加数据，服务器则会生成数据库文件，磁盘中会真实存在，show dbs 命令 可以看到
         * listDatabaseNames()：获取 MongoDB 服务端所有数据库
         * 先返回 迭代器 MongoIterable，在根据迭代器获取 游标 MongoCursor
         * 最后遍历游标进行取值
         * */
        MongoIterable<String> mongoIterable = mongoClient.listDatabaseNames();
        MongoCursor<String> mongoCursor = mongoIterable.iterator();
        while (mongoCursor.hasNext()) {
            String dbName = mongoCursor.next();
            System.out.println("dbName>>>" + dbName);
            dbNameList.add(dbName);
        }
        /**
         * 控制台输出示例：
         * dbName>>>admin
         * dbName>>>config
         * dbName>>>java
         * dbName>>>local
         */
        mongoClient.close();
        return dbNameList;
    }

    /**
     * 获取 MongoDB 服务端所有数据库 文档对象
     * 使用 com.mongodb.MongoClient#listDatabases()
     *
     * @return ：返回的 Document 包含了数据库的详细信息
     */
    public static List<Document> findAllDBs() {
        List<Document> dbList = new ArrayList<Document>();
        /** MongoClient(String host, int port)：直接指定 MongoDB IP 与端口进行连接
         * 实际开发中应该将 MongoDB 服务器地址配置在配置文件中*/
        MongoClient mongoClient = new MongoClient("127.0.0.1", 27017);

        /**getDatabase(String databaseName)：获取指定的数据库
         * 如果此数据库不存在，则会自动创建，此时存在内存中，服务器不会存在真实的数据库文件，show dbs 命令 看不到
         * 如果再往其中添加数据，服务器则会生成数据库文件，磁盘中会真实存在，show dbs 命令 可以看到
         * listDatabaseNames()：获取 MongoDB 服务端所有数据库
         * 先返回 迭代器 MongoIterable，在根据迭代器获取 游标 MongoCursor
         * 最后便利游标进行取值
         * mongoClient.listDatabases()：原理同上，只是返回的最终结果不是 String,而是 Document
         * */

        ListDatabasesIterable<Document> databasesIterable = mongoClient.listDatabases();
        MongoCursor<Document> mongoCursor = databasesIterable.iterator();
        while (mongoCursor.hasNext()) {
            Document db = mongoCursor.next();
            System.out.println(db.toJson());
            dbList.add(db);
        }
        /**
         * 输出内容示例：
         * { "name" : "admin", "sizeOnDisk" : 32768.0, "empty" : false }
         * { "name" : "config", "sizeOnDisk" : 73728.0, "empty" : false }
         * { "name" : "local", "sizeOnDisk" : 73728.0, "empty" : false }
         * { "name" : "mydb1", "sizeOnDisk" : 65536.0, "empty" : false }
         * { "name" : "mydb2", "sizeOnDisk" : 1.99491584E8, "empty" : false }
         */
        mongoClient.close();
        return dbList;
    }

    @Test
    public void testGetMongoClientNoCheck() {
        MongoClient mongoClient = getMongoClientNoCheck();
        int size = mongoClient.getMaxBsonObjectSize();
        // 控制台输出如下：
        // 信息: Monitor thread successfully connected to server with description ServerDescription{address=localhost:27017, type=STANDALONE, state=CONNECTED, ok=true, version=ServerVersion{versionList=[4, 0, 2]}, minWireVersion=0, maxWireVersion=7, maxDocumentSize=16777216, logicalSessionTimeoutMinutes=30, roundTripTimeNanos=2454952}
        // MaxBsonObjectSize >>>16777216
        // MongoDB 服务端地址：localhost:27017
        //
        System.out.println("MaxBsonObjectSize >>>" + size);
        System.out.println("MongoDB 服务端地址：" + mongoClient.getAddress().toString());
    }

    @Test
    public void testGetMongoClientCheck() {
        MongoClient mongoClient = getMongoClientCheck();
        int size = mongoClient.getMaxBsonObjectSize();
        // 控制台输出：
        // MaxBsonObjectSize >>>16777216
        // MongoDB 服务端地址：localhost:27017
        System.out.println("MaxBsonObjectSize >>>" + size);
        System.out.println("MongoDB 服务端地址：" + mongoClient.getAddress().toString());
    }

    @Test
    public void testShowMongoDBInfo(){
        // 控制台输出：
        // serverAddress>>>127.0.0.1:27017
        // connectPoint>>>127.0.0.1:27017
        // bsonObjectSize>>>16777216
        showMongoDBInfo();
    }

    public static void main(String[] args) {
        findAllDBs();
    }
}