
-- 用于比对相同类型的 录入与列表视图的 is_visible、is_enabled、is_required 配置是否一致
-- 比如比较 行政人员的 录入视图与它的 列表视图 是否一致，因为经常有人只修改了其中一个视图，而没有修改它对应的录入或者列表视图
-- 暂时支持比较 is_visible、is_enabled、is_required ，可以自行再添加
declare
view_id1 gap_ui_column.id%type :='a95317c6fbce4ab486a577e905b0e74a'; -- 列表视图 id
view_id2 gap_ui_column.id%type :='5f0e4ea747fd4f26a99df69eff2262c1'; -- 录入视图 id，以谁的配置为准，就放的视图ID到这里
up_sql varchar2(2048);

cursor vrows is SELECT
  T11.id as id1,T12.id as id2,
  t11.field_code as field_code1,t12.field_code as field_code2,
  t11.title as title1,t12.title as title2,
  t11.is_visible as is_visible1,t12.is_visible as is_visible2,
  t11.is_enabled as is_enabled1,t12.is_enabled as is_enabled2,
  t11.is_required as is_required1,t12.is_required as is_required2,
  t11.view_id
   FROM
  (SELECT T1.* FROM gap_ui_column t1 where t1.view_id=view_id1) t11,
  (SELECT T2.* FROM gap_ui_column t2 where t2.view_id=view_id2) t12
where t11.field_code=t12.field_code and (t11.is_visible != t12.is_visible OR t11.is_enabled != t12.is_enabled Or t11.is_required != t12.is_required);

begin
  for vrow in vrows loop
     up_sql := 'update gap_ui_column set ';
     if vrow.is_visible1 != vrow.is_visible2 then
       up_sql := up_sql || 'is_visible=' || vrow.is_visible2 || ',';
     end if;

     if vrow.is_enabled1 != vrow.is_enabled2 then
       up_sql := up_sql || 'is_enabled=' || vrow.is_enabled2 || ',';
     end if;

     if vrow.is_required1 != vrow.is_required2 then
       up_sql := up_sql || 'is_required=' || vrow.is_required2 || ',';
     end if;

     up_sql := substr(up_sql, 0,length(up_sql)-1);-- 去掉结尾多余的逗号

     up_sql := up_sql || ' where id=''' || vrow.id1 || ''' and title=''' || vrow.title1 || ''' and view_id=''' || vrow.view_id || ''';' ;

     dbms_output.put_line(up_sql);-- 打印 sql
     up_sql :='';-- 重新置空
  end loop;
end;
