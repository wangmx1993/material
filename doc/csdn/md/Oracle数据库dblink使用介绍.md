# dblink 介绍

1、‌dblink（全称database link）是Oracle数据库中用于连接不同数据库实例的一种机制‌，用户可以在一个数据库实例中直接查询或操作另一个数据库实例中的表、视图或存储过程、函数等等。‌

2、dblink 主要有以下几个方面的作用：

- ‌跨数据库操作‌：允许用户在一个数据库实例中直接访问并操作另一个数据库实例中的数据。

- ‌简化数据管理‌：通过dblink可以方便地管理和维护分布在多个数据库中的数据，而无需在每个数据库中重复相同的数据操作。
- ‌提高效率‌：通过dblink可以减少数据传输的延迟，提高数据处理的效率。

3、比如从一个库将数据迁移到另一个库，不用导出、导入，直接使用 dblink 速度很快。

4、Oracle 中的存储过程、函数、触发器等等，都不能直接跨库操作，必须借助 dblink 链接才行。

5、如果在触发器、存储过程、函数等内部使用 dblink，需要注意，如果后续 dblink 发生变化，比如目标数据库的地址、账号、密码等信息发生了变化，
    则使用dblink的地方也会跟着报错。

# 权限说明

1. 创建数据库链接的账号必须有 CREATE DATABASE LINK 或 CREATE PUBLIC DATABASE LINK 的权限，
2. 用来登录到远程数据库的账号必须有CREATE SESSION权限。CONNECT 角色具有 CREATE DATABASE LINK、CREATE SESSION 权限，DBA 角色具有 CREATE PUBLIC DATABASE LINK 权限。
3. 用户权限操作可以参考：https://wangmaoxiong.blog.csdn.net/article/details/90690400。

```
-- 查询当前登录用户是否具有创建、删除dblink 链接的权限
-- CREATE DATABASE LINK
-- CREATE PUBLIC DATABASE LINK  （public表示所创建的dblink对于数据库中的所有用户都可以使用，否则只能是谁创建的谁使用）
-- DROP PUBLIC DATABASE LINK
select * from session_privs t WHERE t.privilege LIKE '%LINK%'; 
```

# 查询已有的 dblink

```
select * from dba_db_links;-- 查询所有用户下的 dblink 链接
select * from user_db_links;-- 查询当前登录用户下的 dblink 链接
```
![](../images/202403181613.png)

# 创建 dblink

```
格式：CREATE [PUBLIC] DATABASE LINK 链接名 CONNECT TO 目标数据库用户名 IDENTIFIED BY "目标数据库用户密码"  USING '目标数据库连接地址';
-- PUBLIC：表示是否是公用的dblink链接，公用数据库链接对于数据库中的所有用户可用，否则私有链接仅对创建它的用户可用。不能由一个用户给另外一个用户授权私有数据库链接。一个数据库链接要么是公用，要么是私有。
-- Oracle数据库版本大于等于11G时，密码必须试用双引号包裹。SELECT * FROM v$version t;-- 查询数据库版本
```
**示例1**

![](../images/20250312163132.png)
1、本地的服务器机子上安装了Oracle19C数据库服务，它上面安装了各个业务系统使用的开发库，比如 cs_frame、cs_element，现在需要在 cs_frame 库中访问 cs_element 库中。此时登录 cs_frame 用户，然后创建 dblink 如下。

```
示例1：CREATE DATABASE LINK dblink_cs_element_wmx CONNECT TO cs_element IDENTIFIED BY "1" USING '10.104.65.181:1521/orcl';

示例2：与上面效果一致，写法与tnsnames.ora文件相同
CREATE DATABASE LINK dblink_cs_element_wmx2 CONNECT TO cs_element IDENTIFIED BY "1" 
using '(DESCRIPTION =
          (ADDRESS_LIST =
            (ADDRESS = (PROTOCOL = TCP)(HOST = 10.104.65.181 )(PORT = 1521))
          )
          (CONNECT_DATA =
            (SERVICE_NAME = orcl)
          )
        )';
```

![](../images/20250312164733.png)

2、也可以使用客户端工具创建，比如 PLSQL Developer，文件-> 数据库链接，然后填写即可，效果和使用sql脚本是一样的。

```
-- Drop existing database link 
drop public database link DBLINK_CS_ELEMENT_WMX3;
-- Create database link 
create public database link DBLINK_CS_ELEMENT_WMX3 connect to CS_ELEMENT  using '10.104.65.181:1521/orcl';
```

![](../images/PixPin_2025-03-12_17-51-35.gif)

# 通过db_link操作目标库

1. 创建完成后，便可以在 cs_frame 库中通过 dblink 访问 cs_element 目标库了。与操作本库基本一样，区别就是在目标库的表名后面使用@符合带上dblink的链接名称即可。

```
格式：SELECT * FROM 目标库表名@db_link链接名;-- 通过dblink跨库查询
示例-查询：SELECT T.* FROM emp@dblink_cs_element_wmx t;
示例-更新：UPDATE emp@dblink_cs_element_wmx t SET sal = 4000 WHERE ename = 'SCOTT';
示例-删除：DELETE FROM emp@dblink_cs_element_wmx t WHERE empno = 7499;
其他比如数据插入，以及在 plsql、触发器、存储过程、函数里面访问也是完全一样。
```

# 删除 dblink

```
-- 删除 dblink 链接
-- 如果创建时指定为了public，则删除是也需要指定;反之创建时如果没有指定public，则删除时也不要指定
格式：drop [public] database link 连接名;
示例：DROP database link dblink_cs_element_wmx;
```

# 配合同义词使用

1. 同义词介绍：https://wangmaoxiong.blog.csdn.net/article/details/95479263。
2. 

```
格式：CREATE SYNONYM 同义词名 FOR 表名@数据库链接名;
示例：CREATE SYNONYM syno_emp_by_dblink_cs_element_wmx2 FOR emp@dblink_cs_element_wmx2;
--使用：然后就可以使用同义词名代替原来的 emp@dblink_cs_element_wmx2 的了
SELECT T.*,ROWID FROM syno_emp_by_dblink_cs_element_wmx2 t;
```

