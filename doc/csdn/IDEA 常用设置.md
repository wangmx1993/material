不同版本，略有差异，大体相同

# 1、设置项目打开位置

1、通常有以下三种
![image](images/20240113085556.png)

# 2、启动时如何打开项目

![image](images/20240113091027.png)

# 3、修改注释颜色

![image](images/20240113093004.png)

# 4、修改窗口的字体和大小

![image](images/20240113094302.png)

# 5、修改编辑区的字体和大小

1、下面两个地方都可以设置编辑区的字体与大小，控制台也是同理：

- File–>settings–>Editor–>Font -> font: 设置字体，size：设置大小
- File–>settings–>Editor–>Color Scheme -> Color Scheme Font -> font: 设置字体，size：设置大小

![image](images/20240113094548.png)

![image](images/20240113094622.png)

# 6、设置项目文件编码

1、每当创建一个新项目第一件事情就是设置好项目的编码再写代码，否则中文会出现乱码。

![image](images/20240527085738.png)