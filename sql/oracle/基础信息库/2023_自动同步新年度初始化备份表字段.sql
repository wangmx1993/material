CREATE OR REPLACE PROCEDURE PROC_SYNC_BAS_FORMER_WMX(p_from_table_name in varchar2, --部标表，如 BAS_PERSON_INFO
                                                     p_to_table_name   in varchar2, --备份表，如 BAS_PERSON_INFO_FORMER
                                                     --是否已自动执行脚本，当且仅当为1时已自动执行
                                                     --不已自动执行时，手动从输出的日志中复制脚本即可.
                                                     p_is_auto_run in number) is

  v_sql varchar2(1024); -- 自动拼接 DDL SQL 并执行
begin
--自定义函数：自动同步新年度初始化操作备份表字段，防止备份表正式表少字段而导致初始化失败。
  for vrow in (SELECT t1.COLUMN_NAME,
                      t1.DATA_TYPE,
                      t1.DATA_LENGTH,
                      t1.DATA_PRECISION,
                      t1.DATA_SCALE
                 FROM user_tab_columns t1
                where TABLE_NAME = p_from_table_name
                  and not exists
                (SELECT 1
                         FROM user_tab_columns t2
                        where TABLE_NAME = p_to_table_name
                          and t2.COLUMN_NAME = t1.column_name)) loop

    v_sql := 'alter table ' || p_to_table_name || ' add ' ||
             vrow.COLUMN_NAME || ' ' || vrow.data_type;

    --数字类型
    if vrow.data_type = 'NUMBER' then
      --指定了长度时
      if vrow.data_precision is not null then
        --当是小数时
        if vrow.data_scale > 0 then
          v_sql := v_sql || '(' || vrow.data_precision || ',' ||
                   vrow.data_scale || ')';
        else
          v_sql := v_sql || '(' || to_char(vrow.data_precision) || ')';
        end if;
      else
        v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
      end if;
      --文本类型
    elsif vrow.data_type in ('CHAR', 'VARCHAR2', 'NVARCHAR2', 'CLOB') and
          vrow.DATA_LENGTH is not null then
      v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
    end if;

    if p_is_auto_run = 1 then
      execute immediate (v_sql);
      dbms_output.put_line('--已自动执行');
    end if;
    dbms_output.put_line(v_sql || ';');

  end loop;
end;

/


--调用存储过程自动同步表字段
--调用存储过程自动同步表字段
--调用存储过程自动同步表字段
CALL PROC_SYNC_BAS_FORMER_WMX('BAS_PERSON_INFO', 'BAS_PERSON_INFO_FORMER', 1);
CALL PROC_SYNC_BAS_FORMER_WMX('BAS_PERSON_EXT', 'BAS_PERSON_EXT_FORMER', 1);
CALL PROC_SYNC_BAS_FORMER_WMX('BAS_PERSON_ASSIST_INFO', 'BAS_PERSON_ASSIST_INFO_FORMER', 1);
CALL PROC_SYNC_BAS_FORMER_WMX('BAS_AGENCY_INFO', 'BAS_AGENCY_INFO_FORMER', 1);
CALL PROC_SYNC_BAS_FORMER_WMX('BAS_AGENCY_EXT', 'BAS_AGENCY_EXT_FORMER', 1);
