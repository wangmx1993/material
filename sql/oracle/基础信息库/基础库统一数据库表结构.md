
# 基础库统一数据库表结构

# 需求说明

1、同一个系统在多个省份使用时，各自都加了一些个性化的字段，以及部标规范中又新增了字段。

2、现在需要将各个省份的表结构加上部标规范进行汇总，求一个并集，以保证重新统一后大家都能正常使用。

3、将基础库使用的表、列整理到 excel 中。

4、是否校验列：
    部标的字段全部为 '是'。
    代码里强制需要的为 '是'，比如自己加的audit_id，肯定校验，为 '是'。
    现场自己加的不管。

5、可否为空列：
    部标有的字段，以部标文档要求为准。
    部标没有的字段，几个环境中只要有一个可以为空，则算做可以为空。
    
6、注释信息以部标注释优先，没有时用数据库表维护的注释。

7、同一个表字段多个环境都有时，优先使用部标，其次是湖南，按着优先级递减。

![](images/2023030705.png)

# 数据维护

1、第一步：先建一个表，用于将几个环境的表结构进行汇总到一起。
    
    -- Create table
    create table WMX_TEMP (
      table_name     VARCHAR2(42) not null,
      column_name    VARCHAR2(60) not null,
      comments       VARCHAR2(640),
      db_name        VARCHAR2(8) not null,
      data_type      VARCHAR2(16) not null,
      data_length    NUMBER,
      data_precision NUMBER,
      data_scale     NUMBER,
      nullable       VARCHAR2(8) not null,
      table_cn_name  VARCHAR2(128) not null
    );
    -- Add comments to the columns 
    comment on column WMX_TEMP.table_name      is '表英文名';
    comment on column WMX_TEMP.column_name      is '列名';
    comment on column WMX_TEMP.comments      is '列注释';
    comment on column WMX_TEMP.db_name      is '数据来源库';
    comment on column WMX_TEMP.data_type      is '数据类型';
    comment on column WMX_TEMP.data_length      is '长度';
    comment on column WMX_TEMP.data_precision      is '浮点型整数长度';
    comment on column WMX_TEMP.data_scale      is '浮点型小数长度';
    comment on column WMX_TEMP.nullable      is '是否可以为空';
    comment on column WMX_TEMP.table_cn_name      is '表中文名';


![](images/2023030701.png)

2、第二步：从各个库中抽取表结构数据，然后复制到汇总表。

    SELECT
     'hunan' db_name,
     t.table_name,
     t.column_name,
     t.data_type,
     t.data_length,
     t.data_precision,
     t.data_scale,
     t.nullable,
     t2.comments,
     t.table_name table_cn_name
      FROM user_tab_columns T
      left join user_col_comments t2
        on t.TABLE_NAME = t2.TABLE_NAME
       and t.COLUMN_NAME = t2.COLUMN_NAME
     where t.TABLE_NAME in ('BAS_PERSON_EXT', 'BAS_AUDIT_BILL')
     order by t.TABLE_NAME, t.COLUMN_NAME;

![](images/2023030702.png)

3、第三步：从部标规范最新 word 文档中复制表结构到汇总表中，这里因为 word 文档中定义的数据类型和 Oracle 中是不一样的，
所以需要特殊处理：比如将 Integer(19,2) -> Number(19,2)，同时还需处理长度、精度。注意其中是否必填和数据库的 nullable 
含义是相反的，也需要处理，部标规范文档格式如下： 

![](images/2023030703.png)

    SELECT t.data_type,
           (case
             when instr(data_type, 'String') > 0 then
              'VARCHAR2'
             else
              case
                when instr(data_type, 'Integer') > 0 then
                 'NUMBER'
                else
                 case
                   when instr(data_type, 'Date') > 0 then
                    'DATE'
                   else
                    case
                      when instr(data_type, 'Decimal') > 0 or data_type = 'Currency' then
                       'NUMBER'
                      else
                       data_type
                    end
                 end
              end
           end) 类型,
           
           (case
             when instr(data_type, 'String') > 0 then
              substr(data_type,
                     instr(data_type, '(') + 1,
                     instr(data_type, ')') - instr(data_type, '(') - 1)
             else
              case
                when instr(data_type, 'Integer') > 0 then
                 '22'
                else
                 case
                   when instr(data_type, 'Date') > 0 then
                    null
                   else
                    case
                      when instr(data_type, 'Decimal') > 0 or data_type = 'Currency' then
                       '22'
                      else
                       data_type
                    end
                 end
              end
           end) data_length,
           
           (case
             when instr(data_type, 'String') > 0 then
              null
             else
              case
                when instr(data_type, 'Integer') > 0 then
                 substr(data_type,
                                 instr(data_type, '(') + 1,
                                 instr(data_type, ')') - instr(data_type, '(') - 1)
                else
                 case
                   when instr(data_type, 'Date') > 0 then
                    null
                   else
                    case
                      when data_type = 'Currency' then
                       '18'
                      else
                        case
                          when instr(data_type, 'Decimal') > 0then
                           substr(data_type,
                                 instr(data_type, '(') + 1,
                                 instr(data_type, ',') - instr(data_type, '(') - 1)
                          else
                           null
                    end
                    end
                 end
              end
           end) data_precision,
           
           (case
             when instr(data_type, 'String') > 0 then
              null
             else
              case
                when instr(data_type, 'Integer') > 0 then
                 '0'
                else
                 case
                   when instr(data_type, 'Date') > 0 then
                    null
                   else
                    case
                      when data_type = 'Currency' then
                       '2'
                      else
                       case
                         when instr(data_type, 'Decimal') > 0 then
                          substr(data_type,
                                 instr(data_type, ',') + 1,
                                 instr(data_type, ')') - instr(data_type, ',') - 1)
                         else
                          null
                       end
                    end
                 end
              end
           end) data_scale
    
      FROM wmx_temp T
     where t.db_name = 'bubiao'
       and t.data_type is not null
     group by t.data_type, t.data_length, t.data_precision, t.data_scale;
    
    --如果查询结果解析数据类型没有问题，则可以进行更新。
    update wmx_temp t set  t.data_type = 
           (case
             when instr(data_type, 'String') > 0 then
              'VARCHAR2'
             else
              case
                when instr(data_type, 'Integer') > 0 then
                 'NUMBER'
                else
                 case
                   when instr(data_type, 'Date') > 0 then
                    'DATE'
                   else
                    case
                      when instr(data_type, 'Decimal') > 0 or data_type = 'Currency' then
                       'NUMBER'
                      else
                       data_type
                    end
                 end
              end
           end),
           data_length = 
           (case
             when instr(data_type, 'String') > 0 then
              substr(data_type,
                     instr(data_type, '(') + 1,
                     instr(data_type, ')') - instr(data_type, '(') - 1)
             else
              case
                when instr(data_type, 'Integer') > 0 then
                 '22'
                else
                 case
                   when instr(data_type, 'Date') > 0 then
                    null
                   else
                    case
                      when instr(data_type, 'Decimal') > 0 or data_type = 'Currency' then
                       '22'
                      else
                       data_type
                    end
                 end
              end
           end) ,
           
           data_precision = 
           (case
             when instr(data_type, 'String') > 0 then
              null
             else
              case
                when instr(data_type, 'Integer') > 0 then
                 substr(data_type,
                                 instr(data_type, '(') + 1,
                                 instr(data_type, ')') - instr(data_type, '(') - 1)
                else
                 case
                   when instr(data_type, 'Date') > 0 then
                    null
                   else
                    case
                      when data_type = 'Currency' then
                       '18'
                      else
                        case
                          when instr(data_type, 'Decimal') > 0then
                           substr(data_type,
                                 instr(data_type, '(') + 1,
                                 instr(data_type, ',') - instr(data_type, '(') - 1)
                          else
                           null
                    end
                    end
                 end
              end
           end) ,
           
           data_scale = 
           (case
             when instr(data_type, 'String') > 0 then
              null
             else
              case
                when instr(data_type, 'Integer') > 0 then
                 '0'
                else
                 case
                   when instr(data_type, 'Date') > 0 then
                    null
                   else
                    case
                      when data_type = 'Currency' then
                       '2'
                      else
                       case
                         when instr(data_type, 'Decimal') > 0 then
                          substr(data_type,
                                 instr(data_type, ',') + 1,
                                 instr(data_type, ')') - instr(data_type, ',') - 1)
                         else
                          null
                       end
                    end
                 end
              end
           end)
    
     where t.db_name = 'bubiao'
       and t.data_type is not null;
    
# 数据抽取    

1、确保数据维护正确之后，比如：表名、列名全部转大写，前后不要有空格，列类型和长度维护正确了，是否可为NULL维护正确，
同一个表的中文名维护相同等等。

2、复制查询结果到 excel 中即可，或者直接导出也行。

    SELECT Tab3.*
      FROM (SELECT row_number() over(partition by 表名, 列名 order by tab2.优先级 asc) as xh,
                   tab2.*
              FROM (
                    --同一个表字段相同时，优先使用部标，其次是湖南
                    SELECT t.db_name,
                            (case
                              when db_name = 'bubiao' then
                               1
                              else
                               case
                                 when db_name = 'hunan' then
                                  2
                                 else
                                  case
                                    when db_name = 'henan' then
                                     3
                                    else
                                     case
                                       when db_name = 'liaoning' then
                                        4
                                       else
                                        case
                                          when db_name = 'gansu' then
                                           6
                                          else
                                           7
                                        end
                                     end
                                  end
                               end
                            end) 优先级,
                            '人员信息业务表' 页签名称,
                            t.table_name 表名,
                            t.table_cn_name 表中文名,
                            t.column_name 列名,
                            t.data_type 列类型,
                            (case
                              when data_type = 'NUMBER' then
                               case
                               --指定了长度时
                                 when data_precision is not null then
                                  case
                                  --当是小数时
                                    when data_scale > 0 then
                                     data_precision || ',' || data_scale
                                    else
                                     to_char(data_precision)
                                  end
                                 else
                                  to_char(data_length)
                               end
                              when data_type in
                                   ('CHAR', 'VARCHAR2', 'NVARCHAR2', 'CLOB') then
                               to_char(data_length)
                              else
                               null
                            end) 长度,
                            
                            (case
                            --以部标是否允许为空优先
                              when exists (SELECT 1
                                      FROM wmx_temp T2
                                     where t2.db_name = 'bubiao'
                                       and t2.table_name = t.TABLE_NAME
                                       and t2.column_name = t.COLUMN_NAME
                                       and rownum <= 1) then
                               (SELECT t2.nullable
                                  FROM wmx_temp T2
                                 where t2.db_name = 'bubiao'
                                   and t2.table_name = t.TABLE_NAME
                                   and t2.column_name = t.COLUMN_NAME
                                   and rownum <= 1)
                              else
                               case
                               --部标没有的字段，几个环境中只要有一个可以为空，则算做可以为空。
                                 when exists (SELECT 1
                                         FROM wmx_temp T2
                                        where t2.db_name != 'bubiao'
                                          and t2.table_name = t.TABLE_NAME
                                          and t2.column_name = t.COLUMN_NAME
                                          and t2.nullable = '否') then
                                  '否'
                                 else
                                  t.nullable
                               end
                            end) 可否为空,
                            
                            (case
                            --以部标注释优先
                              when exists (SELECT 1
                                      FROM wmx_temp T2
                                     where t2.db_name = 'bubiao'
                                       and t2.table_name = t.TABLE_NAME
                                       and t2.column_name = t.COLUMN_NAME
                                       and rownum <= 1) then
                               (SELECT t2.comments
                                  FROM wmx_temp T2
                                 where t2.db_name = 'bubiao'
                                   and t2.table_name = t.TABLE_NAME
                                   and t2.column_name = t.COLUMN_NAME
                                   and rownum <= 1)
                              else
                               case
                               --存在有效注释时不再处理
                                 when comments is not null and comments not like '%?%' then
                                  comments
                                 else --不存在注释，或者是乱码时
                                  case
                                    when exists
                                    --从其它相同表中获取
                                     (select *
                                            from (SELECT 1
                                                    FROM wmx_temp T2
                                                   where t2.table_name = t.TABLE_NAME
                                                     and t2.column_name = t.COLUMN_NAME
                                                     and t2.comments is not null
                                                     and t2.comments not like '%?%'
                                                   order by length(t2.comments) desc)
                                           where rownum <= 1) then
                                     (SELECT t2.comments
                                        FROM wmx_temp T2
                                       where t2.table_name = t.TABLE_NAME
                                         and upper(trim(t2.column_name)) = t.COLUMN_NAME
                                         and t2.comments is not null
                                         and t2.comments not like '%?%'
                                         and rownum <= 1)
                                    else
                                     case
                                       when exists
                                       --从其它相同列中获取
                                        (select *
                                               from (SELECT 1
                                                       FROM wmx_temp T2
                                                      where t2.column_name = t.COLUMN_NAME
                                                        and t2.comments is not null
                                                        and t2.comments not like '%?%'
                                                      order by length(t2.comments) desc)
                                              where rownum <= 1) then
                                        (SELECT t2.comments
                                           FROM wmx_temp T2
                                          where t2.column_name = t.COLUMN_NAME
                                            and t2.comments is not null
                                            and t2.comments not like '%?%'
                                            and rownum <= 1)
                                       else
                                        comments
                                     end
                                  end
                               end
                            end) 注释,
                            
                            (case
                              when exists (SELECT 1
                                      FROM wmx_temp T2
                                     where t2.db_name = 'bubiao'
                                       and t2.table_name = t.TABLE_NAME
                                       and t2.column_name = t.COLUMN_NAME
                                       and rownum <= 1) then
                               '是'
                              else
                               case
                                 when instr(TABLE_NAME, '_AGENCY_') > 0 and exists
                                  (SELECT 1
                                         FROM wmx_temp T2
                                        where t2.db_name = 'bubiao'
                                          and t2.table_name in
                                              ('BAS_AGENCY_INFO', 'BAS_AGENCY_EXT')
                                          and t2.column_name = t.COLUMN_NAME
                                          and rownum <= 1) then
                                  '是'
                                 else
                                  case
                                    when instr(TABLE_NAME, '_PERSON_') > 0 and exists
                                     (SELECT 1
                                            FROM wmx_temp T2
                                           where t2.db_name = 'bubiao'
                                             and t2.table_name in
                                                 ('BAS_PERSON_INFO', 'BAS_PERSON_EXT')
                                             and t2.column_name = t.COLUMN_NAME
                                             and rownum <= 1) then
                                     '是'
                                    else
                                     '否'
                                  end
                               end
                            end) 是否部标,
                            
                            (case
                              when exists (SELECT 1
                                      FROM wmx_temp T2
                                     where t2.db_name = 'bubiao'
                                       and t2.table_name = t.TABLE_NAME
                                       and t2.column_name = t.COLUMN_NAME
                                       and rownum <= 1) then
                               '是'
                              else
                               case
                                 when instr(TABLE_NAME, '_AGENCY_') > 0 and exists
                                  (SELECT 1
                                         FROM wmx_temp T2
                                        where t2.db_name = 'bubiao'
                                          and t2.table_name in
                                              ('BAS_AGENCY_INFO', 'BAS_AGENCY_EXT')
                                          and t2.column_name = t.COLUMN_NAME
                                          and rownum <= 1) then
                                  '是'
                                 else
                                  case
                                    when instr(TABLE_NAME, '_PERSON_') > 0 and exists
                                     (SELECT 1
                                            FROM wmx_temp T2
                                           where t2.db_name = 'bubiao'
                                             and t2.table_name in
                                                 ('BAS_PERSON_INFO', 'BAS_PERSON_EXT')
                                             and t2.column_name = t.COLUMN_NAME
                                             and rownum <= 1) then
                                     '是'
                                    else
                                     case
                                       when column_name in ('AUDIT_ID',
                                                            'IS_END',
                                                            'APPLY_ID',
                                                            'FROM_ASSET_ID',
                                                            'UI_GROUP',
                                                            'BIZ_KEY',
                                                            'FORM_ID',
                                                            'FROM_PER_ID',
                                                            'FROM_AGENCY_ID',
                                                            'FROM_AGENCY_CODE',
                                                            'AGENCY_CODE',
                                                            'CHANGE_TYPE_CODE',
                                                            'IS_DELETED',
                                                            'IS_ENABLED',
                                                            'UI_CODE',
                                                            'MOF_DIV_CODE',
                                                            'CLASS_FLAG',
                                                            'FROM_UI_CODE',
                                                            'OLD_EXP_CRI_ID',
                                                            'FISCAL_YEAR',
                                                            'END_DATE',
                                                            'IS_LEAF',
                                                            'LEVEL_NO',
                                                            'LEVEL_NUM',
                                                            'VERSION') then
                                        '是'
                                       else
                                        '否'
                                     end
                                  end
                               end
                            end) 是否校验
                    
                      FROM wmx_temp T
                     where t.table_name in ('BAS_PERSON_ASSIST_INFO')
                     order by t.table_name, t.column_name) tab2) tab3
     where xh = 1;



![](images/2023030704.png)

