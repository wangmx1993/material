
--生产环境
--生产环境
--生产环境

-- 为了防止全表数据太大，每个区划单独执行
declare
  target_mof_div_code varchar2(9) := '431230000'; --需要处理的目标区划
  target_fiscal_year  varchar2(4) := '2023'; --需要处理的目标年度
  --target_agency_code  varchar2(21) := '002001'; --需要处理的目标单位(不想逐个单位执行时，可以全局注释)
  is_exists_audit     number := 0; --单位是否存在审核单
  v_audit_id          varchar2(38) := null; --21年审核单Id
  v_is_end            number(1) := null; --审核单是否终审

  cursor vrows is
  --查询刚复制生成的单位
    SELECT t.mof_div_code, t.agency_code, count(1) c
      FROM bas_person_info t
     where t.mof_div_code = target_mof_div_code
       and t.fiscal_year = target_fiscal_year
       and t.version = target_fiscal_year || 'rcdtwh'
       --and t.agency_code = target_agency_code
       and t.create_user_name = '2022年迁移到23年'
     group by t.mof_div_code, t.agency_code
     order by t.agency_code;
begin
  for vrow in vrows loop
    --查询单位在目标年度是否存在审核单
    SELECT count(1)
      into is_exists_audit
      FROM bas_audit_bill t
     where t.mof_div_code = vrow.mof_div_code
       and t.agency_code = vrow.agency_code
       and t.fiscal_year = target_fiscal_year
       and t.ui_group = 'BAS_PERSON';

    --单位存在审核单时,更新人员信息的audit_id
    if is_exists_audit > 0 then
      -- 查询单位最新的审核单信息
      SELECT audit_id, is_end
        into v_audit_id, v_is_end
        FROM (SELECT t.audit_id, t.is_end
                FROM bas_audit_bill t
               where t.mof_div_code = vrow.mof_div_code
                 and t.agency_code = vrow.agency_code
                 and t.fiscal_year = target_fiscal_year
                 and t.ui_group = 'BAS_PERSON'
               order by t.create_time desc)
       where rownum <= 1;

      dbms_output.put_line(vrow.mof_div_code || '=>' || vrow.agency_code || '=>' ||
                           vrow.c || ' => audit_id=' || v_audit_id ||
                           ' => is_end=' || v_is_end);

      --更新人员信息的audit_id
      update bas_person_info t
         set t.audit_id = v_audit_id, t.is_end = v_is_end
       where t.mof_div_code = vrow.mof_div_code
         and t.agency_code = vrow.agency_code
         and t.version = target_fiscal_year || 'rcdtwh'
         and t.fiscal_year = target_fiscal_year;
      --每个单位提交一次
      commit;

      insert into bas_person_audit
        (AUDIT_ID, PER_ID, AGENCY_CODE, MOF_DIV_CODE, FISCAL_YEAR)
        SELECT t.audit_id,
               t.per_id,
               t.agency_code,
               t.mof_div_code,
               t.fiscal_year
          FROM bas_person_info t
         where t.mof_div_code = vrow.mof_div_code
           and t.agency_code = vrow.agency_code
           and t.version = target_fiscal_year || 'rcdtwh'
           and t.fiscal_year = target_fiscal_year;
      --每个单位提交一次
      commit;
    end if;
  
  end loop;

end;
