# html2canvas 插件概述与下载

​1、<mark>html2canvas 允许您直接在用户浏览器上拍摄网页或其部分的“屏幕截图”</mark>，屏幕截图是基于DOM 的，因此对于真实的表示可能不是100%准确，因为它没有生成实际的屏幕截图，而是根据页面上可用的信息构建屏幕截图。

2、脚本将遍历加载它的页面的 DOM，它收集所有元素的信息，然后使用这些信息来构建页面的表示，换句话说，它实际上并不获取页面的屏幕截图，而是根据从DOM读取的属性构建页面的表示。因此，它只能正确地呈现它理解的属性，这意味着可能有许多CSS属性不能正常工作，有关支持的CSS属性的完整列表，请查看[支持的功能页面](http://html2canvas.hertzen.com/features/ "支持的功能页面")。

3、脚本使用的所有图像都需要驻留在同一个原点下，以便能够在没有代理的帮助下读取它们。类似地，如果页面上有其他canvas元素，这些元素已经被跨源内容污染，它们将变脏，不再被html2canvas读取。

4、该脚本不呈现插件内容，如Flash或javaapplet。

5、该库应该可以在以下浏览器上正常工作: Firefox 3.5+，Google Chrome，Opera 12+，IE9+，Edge，Safari 6+

6、[html2canvas](http://html2canvas.hertzen.com/ "html2canvas")  官网：[http://html2canvas.hertzen.com/](http://html2canvas.hertzen.com/ "http://html2canvas.hertzen.com/")

7、`html2canvas 官网安装文档：`[Getting Started | html2canvas](http://html2canvas.hertzen.com/getting-started "http://html2canvas.hertzen.com/getting-started")

8、`html2canvas Github 开源下载地址：`[Releases · niklasvh/html2canvas · GitHub](https://github.com/niklasvh/html2canvas/releases "https://github.com/niklasvh/html2canvas/releases")，如 [Release v1.0.0-rc.7 · niklasvh/html2canvas · GitHub](https://github.com/niklasvh/html2canvas/releases/tag/v1.0.0-rc.7 "https://github.com/niklasvh/html2canvas/releases/tag/v1.0.0-rc.7")

​

# html2canvas 使用快速入门

1、html2canvas 的使用非常简单，格式如下([官网](http://html2canvas.hertzen.com/getting-started "官网"))：

| html2canvas(element, options).then(function(canvas) {  <br>    document.body.appendChild(canvas);  <br>});                                                                                                  |
|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| //1）element：是需要转为 canvas 的 DOM 元素对象，比如整个 html、body，或者某个 div 等等，想要转换谁，就传入谁 <br/> //2）options：可选的参数，比如 backgroundColor 设置画布背景色，imageTimeout 设置图片加载超时时间等等 //3）canvas：为转换后的 Canvas 对象，可以做任意的其它操作，比如下载，转 PDF 等等。 |

```bash
<script type="text/javascript" src="./html2canvas.js"></script>
<script type="text/javascript">
    /**
     * html 转为 Canvas
     * 1、document.body：表示将整个 body 页面转化为 Canvas，可以任意的 Dom 对象
     * 2、如果网页中有跨域的图片，则必须设置 allowTaint: true - 允许交叉源图像污染画布
     * 3、logging: false：为调试目的启用日志记录，开发测试时可以开启，上线后应该设置为 false。
     * 4、then函数中的 canvas 参数是转换成功后的 Canvas 对象，可以做任意的其它操作，比如展示、或者下周、转 PDF 等
     */
    function html2Canvas() {
        html2canvas(document.body, {
            allowTaint: true,
            logging: false
        }).then(function (canvas) {
            //将 Canvas 追加到页面中展示
            document.body.appendChild(canvas);
        });
    }
</script>
```

2、完整演示源码：

https://github.com/wangmaoxiong/thymeleafapp/blob/master/src/main/resources/static/html2canvas/helloWorld.html https://github.com/wangmaoxiong/thymeleafapp/blob/master/src/main/resources/static/html2canvas/html2canvas.js

![](../images/c0abe41a1ae2fa9.gif)

# html2canvas 配置选项

1、官网文档：[Options | html2canvas](http://html2canvas.hertzen.com/configuration "Options | html2canvas")

| 参数名称                   | 默认值                       | 描述                                                     |
| ---------------------- | ------------------------- | ------------------------------------------------------ |
| allowTaint             | false                     | 是否允许交叉源图像污染画布，即是否允许跨域，比如引用了百度服务器的图片，如果设置为 false，则不会转换。 |
| background             | #fff                      | 画布背景色，如果在DOM中没有指定，则默认为 null，表示透明。                      |
| canvas                 | `null`                    | 要用作绘图基础的现有画布元素                                         |
| foreignObjectRendering | `false`                   | 如果浏览器支持，是否使用ForeignObject渲染                            |
| imageTimeout           | `15000`                   | 加载图像超时（毫秒）。设置为0可禁用超时。                                  |
| ignoreElements         | `(element) => false`      | 从呈现中移除匹配元素的谓词函数。                                       |
| logging                | `true`                    | 为调试目的启用日志记录，开发测试时可以开启，上线后应该设置为 false。                  |
| onclone                | `null`                    | 在克隆文档以进行呈现时调用的回调函数可用于修改将呈现的内容，而不会影响原始源文档。              |
| proxy                  | undefined                 | 要用于加载跨源图像的代理的Url。如果保留为空，则不会加载交叉源图像。                    |
| removeContainer        | `true`                    | 是否清除html2canvas临时创建的克隆DOM元素                            |
| scale                  | `window.devicePixelRatio` | 用于渲染的比例。默认为浏览器设备像素比率。                                  |
| useCORS                | `false`                   | 亲测解决跨域使用 allowTaint:true 是有效的，而 useCORS:true，并不能解决跨域   |
| width                  | `Element` width           | 画布的宽度                                                  |
| height                 | `Element` height          | 画布的高度                                                  |
| x                      | `Element` x-offset        | 裁剪画布x坐标                                                |
| y                      | `Element` y-offset        | 裁剪画布y坐标                                                |
| scrollX                | `Element` scrollX         | 渲染元素时要使用的x滚动位置（例如，如果元素使用position:fixed）                |
| scrollY                | `Element` scrollY         | 渲染元素时要使用的y滚动位置（例如，如果元素使用position:fixed）                |
| windowWidth            | `Window.innerWidth`       | 呈现元素时要使用的窗口宽度，这可能会影响媒体查询等内容                            |
| windowHeight           | `Window.innerHeight`      | 呈现元素时使用的窗口高度，这可能会影响媒体查询等内容                             |

2、html2canvas 支持与不支持的 CSS 属性和值列表：[Features | html2canvas](http://html2canvas.hertzen.com/features "Features | html2canvas")


