
-- 数据检查
-- 单位信息存在待创建工作流程的数据，但是单位当前又已经存在在途的审核单的数据
-- 这是不对的，这就会导致单位岗送审时，会再次重复创建工作流。
SELECT T.Agency_Id,
       t.biz_key,
       t.agency_code,
       t.agency_name,
       t.mof_div_code,
       t.fiscal_year,
       t.version,
       t.audit_id,
       (select b2.audit_id
          from (select row_number() over(partition by b.mof_div_code, b.agency_code, b.version order by b.create_time desc) as xh,
                       b.audit_id
                  from bas_audit_bill b
                 where t.mof_div_code = b.mof_div_code
                   and t.agency_code = b.agency_code
                   and t.version = b.version
                   and b.is_end = 2
                   and b.ui_group = 'BAS_AGENCY') b2
         where b2.xh = 1) "修正后的aduid_id",
       t.is_end,
       t.is_deleted,
       t.is_enabled,
       t.start_date,
       t.end_date,
       t.create_time,
       t.update_time,
       rowid
  FROM bas_agency_info t
 where t.mof_div_code = '430500000'
   and t.version = '2023rcdtwh'
   and t.is_deleted = 2
   and t.audit_id = '0'
   and t.is_end = 2
   and exists (select 1
          from bas_audit_bill b
         where t.mof_div_code = b.mof_div_code
           and t.agency_code = b.agency_code
           and t.version = b.version
           and b.is_end = 2
           and b.ui_group = 'BAS_AGENCY')
 order by t.agency_code;

-- 数据修复。
-- 将业务数据修改为当前最新的在途审核单ID值
update bas_agency_info t
   set t.audit_id =
       (select b2.audit_id
          from (select row_number() over(partition by b.mof_div_code, b.agency_code, b.version order by b.create_time desc) as xh,
                       b.audit_id
                  from bas_audit_bill b
                 where t.mof_div_code = b.mof_div_code
                   and t.agency_code = b.agency_code
                   and t.version = b.version
                   and b.is_end = 2
                   and b.ui_group = 'BAS_AGENCY') b2
         where b2.xh = 1)
 where t.mof_div_code = '430500000'
   and t.version = '2023rcdtwh'
   and t.is_deleted = 2
   and t.audit_id = '0'
   and t.is_end = 2
   and exists (select 1
          from bas_audit_bill b
         where t.mof_div_code = b.mof_div_code
           and t.agency_code = b.agency_code
           and t.version = b.version
           and b.is_end = 2
           and b.ui_group = 'BAS_AGENCY');
