
-- 单位信息日常动态维护版本，单位基本信息存在有效数据，但是扩展信息表没有数据。
-- 提供脚本进行处理：
-- 1、如果单位有 上一年 的有效单位扩展信息，则直接从 2上一年 年复制。
-- 2、如果单位没有 上一年 的有效单位扩展信息，则直接新增补充一条。
-- 20221214 湖南生产环境已经执行此脚本，修复了 2022年与 2023 年的数据

declare
  i                  number := 0; --统计操作的个数
  target_fiscal_year number := 2022; --需要处理的目标年度(修改这一处就可以执行)
  pre_ext_agency_id  bas_agency_ext.agency_id%type := null; --上一年版本的单位ID
  cursor vrows is
  --查询目标年度有基本信息，却没有扩展信息没有的单位
    SELECT t1.mof_div_code,
           t1.agency_code,
           t1.agency_id,
           t1.biz_key,
           t1.version,
           t1.fiscal_year
      FROM bas_agency_info t1
     where t1.version = target_fiscal_year || 'rcdtwh'
       and t1.fiscal_year = to_char(target_fiscal_year)
       and t1.is_deleted = 2
       and not exists (SELECT 1
              FROM bas_agency_ext t2
             where t2.agency_id = t1.agency_id);
begin
  for vrow in vrows loop
    i := i + 1;
    dbms_output.put_line(i || ' ' || vrow.mof_div_code || '=>' ||
                         vrow.agency_code || '=>' || vrow.agency_id || '=>' ||
                         vrow.version);
    pre_ext_agency_id := null; --每次循环时先清空

    --查询上一年是否存在单位扩展信息
    for pre in (SELECT agency_id
                  FROM bas_agency_ext t
                 where t.version = (target_fiscal_year - 1) || 'rcdtwh'
                   and t.fiscal_year = to_char(target_fiscal_year - 1)
                   and t.is_deleted = 2
                   and t.mof_div_code = vrow.mof_div_code
                   and t.agency_code = vrow.agency_code
                   and rownum <= 1) loop
      pre_ext_agency_id := pre.agency_id;
    end loop;
    if pre_ext_agency_id is not null then
      dbms_output.put_line('    从上一年复制：' || pre_ext_agency_id);
          insert into BAS_AGENCY_EXT (AGENCY_ID, BIZ_KEY, ADMIN_STAF_NUM, INST_STAF_NUM, WORKER_STAF_NUM, ACTU_STAF_NUM, ACTUNOW_STAF_NUM, START_DATE, END_DATE, IS_ENABLED, UPDATE_TIME, IS_DELETED, DR_NUM, MASTER_NUM, UNDERGR_NUM, ACADEMY_NUM, ORDINARY_HIGH_SCH_STUD_NUM, JUNIOR_HIGH_SCH_STUD_NUM, PRIARY_SCH_STUD_NUM, KINDERGARTEN_CHILDREN_NUM, OTHER_STUD_NUM, CREATE_TIME, CREATE_USER_ID, CREATE_USER_CODE, CREATE_USER_NAME, UPDATE_USER_ID, UPDATE_USER_CODE, UPDATE_USER_NAME, LIXIU, TUIXIU, WAIPIN, YISHU, STAF_NUM, AGENCY_CODE, MOF_DIV_CODE, ACTU_CAR_NUM, ACTU_DIRE_CAR_NUM, WHERE_OTHERS_NUM, REFE_CIVI_STAF_NUM, BALA_INST_STAF_NUM, SURV_SUB, OTHER_SUB, ADV_ONLY_NUM, SURV_LIVI_ALLO_EXP, STAF_MONTH_NUM, RE_SPEC_POST_SUB, ACTU_RETT_STAF_NUM, RETIRE_ONLY_NUM, TEMP_SCH_EXT_TEACHER_NUM, LEAVE_ONLY_NUM, WATER_RATE, POWER_RATE, HEAT_FEE_RATE, PROPERTY_FEE_RATE, UPKEEP_RATE, LABOR_WAGE_RATE, ORDINARY_EMPLOYEES_NUM, ORDINARY_EMPLOYEES_SUB, TOTAL_PERFORMANCE_NUM, TOTAL_PERFORMANCE_SUB, ADV_ONLY_RATE, ZSZFDW, PERFORBONUS_TOTAL_SUB, MONTHLY_BONUS_SUB, LXJ, SPEC_POST_SUB, AGENCY_FUNCTION, AGENCY_EXE_ID, FISCAL_YEAR, ADMIN_WORKER_STAF_NUM, NST_WORKER_STAF_NUM, LAW_NUM, ENROLL_SCH_STUD_NUM, LODG_SCH_STUD_NUM, FIN_BGT_PAY_MONEY, SPECIAL_PAY_MONEY, FUND_BGT_PAY_MONEY, FIN_SPECIAL_INCOME, FIN_ASSIST_INCOME, ANN_TOTAL_INCOME, ANN_TOTAL_PAY, ANN_PER_PAY, ANN_PUB_PAY, ANN_SPECIAL_PAY, VERSION, VERSION_NAME, CJRJYJ)
          select
            vrow.agency_id,
            vrow.biz_key, ADMIN_STAF_NUM, INST_STAF_NUM, WORKER_STAF_NUM, ACTU_STAF_NUM, ACTUNOW_STAF_NUM, START_DATE, END_DATE, IS_ENABLED, UPDATE_TIME, IS_DELETED, DR_NUM, MASTER_NUM, UNDERGR_NUM, ACADEMY_NUM, ORDINARY_HIGH_SCH_STUD_NUM, JUNIOR_HIGH_SCH_STUD_NUM, PRIARY_SCH_STUD_NUM, KINDERGARTEN_CHILDREN_NUM, OTHER_STUD_NUM, CREATE_TIME, CREATE_USER_ID, CREATE_USER_CODE, CREATE_USER_NAME, UPDATE_USER_ID, UPDATE_USER_CODE, UPDATE_USER_NAME, LIXIU, TUIXIU, WAIPIN, YISHU, STAF_NUM,
            vrow.agency_code,
            vrow.mof_div_code, ACTU_CAR_NUM, ACTU_DIRE_CAR_NUM, WHERE_OTHERS_NUM, REFE_CIVI_STAF_NUM, BALA_INST_STAF_NUM, SURV_SUB, OTHER_SUB, ADV_ONLY_NUM, SURV_LIVI_ALLO_EXP, STAF_MONTH_NUM, RE_SPEC_POST_SUB, ACTU_RETT_STAF_NUM, RETIRE_ONLY_NUM, TEMP_SCH_EXT_TEACHER_NUM, LEAVE_ONLY_NUM, WATER_RATE, POWER_RATE, HEAT_FEE_RATE, PROPERTY_FEE_RATE, UPKEEP_RATE, LABOR_WAGE_RATE, ORDINARY_EMPLOYEES_NUM, ORDINARY_EMPLOYEES_SUB, TOTAL_PERFORMANCE_NUM, TOTAL_PERFORMANCE_SUB, ADV_ONLY_RATE, ZSZFDW, PERFORBONUS_TOTAL_SUB, MONTHLY_BONUS_SUB, LXJ, SPEC_POST_SUB, AGENCY_FUNCTION, AGENCY_EXE_ID,
            vrow.fiscal_year, ADMIN_WORKER_STAF_NUM, NST_WORKER_STAF_NUM, LAW_NUM, ENROLL_SCH_STUD_NUM, LODG_SCH_STUD_NUM, FIN_BGT_PAY_MONEY, SPECIAL_PAY_MONEY, FUND_BGT_PAY_MONEY, FIN_SPECIAL_INCOME, FIN_ASSIST_INCOME, ANN_TOTAL_INCOME, ANN_TOTAL_PAY, ANN_PER_PAY, ANN_PUB_PAY, ANN_SPECIAL_PAY,
            vrow.version,
            vrow.version || '日常动态维护版本', CJRJYJ
          from BAS_AGENCY_EXT where agency_id = pre_ext_agency_id;
    else
      --上一年没有时，直接新增补充一条
      insert into BAS_AGENCY_EXT (AGENCY_ID, BIZ_KEY, ADMIN_STAF_NUM, INST_STAF_NUM, WORKER_STAF_NUM, ACTU_STAF_NUM, ACTUNOW_STAF_NUM, START_DATE, END_DATE, IS_ENABLED, UPDATE_TIME, IS_DELETED, DR_NUM, MASTER_NUM, UNDERGR_NUM, ACADEMY_NUM, ORDINARY_HIGH_SCH_STUD_NUM, JUNIOR_HIGH_SCH_STUD_NUM, PRIARY_SCH_STUD_NUM, KINDERGARTEN_CHILDREN_NUM, OTHER_STUD_NUM, CREATE_TIME, CREATE_USER_ID, CREATE_USER_CODE, CREATE_USER_NAME, UPDATE_USER_ID, UPDATE_USER_CODE, UPDATE_USER_NAME, LIXIU, TUIXIU, WAIPIN, YISHU, STAF_NUM, AGENCY_CODE, MOF_DIV_CODE, ACTU_CAR_NUM, ACTU_DIRE_CAR_NUM, WHERE_OTHERS_NUM, REFE_CIVI_STAF_NUM, BALA_INST_STAF_NUM, SURV_SUB, OTHER_SUB, ADV_ONLY_NUM, SURV_LIVI_ALLO_EXP, STAF_MONTH_NUM, RE_SPEC_POST_SUB, ACTU_RETT_STAF_NUM, RETIRE_ONLY_NUM, TEMP_SCH_EXT_TEACHER_NUM, LEAVE_ONLY_NUM, WATER_RATE, POWER_RATE, HEAT_FEE_RATE, PROPERTY_FEE_RATE, UPKEEP_RATE, LABOR_WAGE_RATE, ORDINARY_EMPLOYEES_NUM, ORDINARY_EMPLOYEES_SUB, TOTAL_PERFORMANCE_NUM, TOTAL_PERFORMANCE_SUB, ADV_ONLY_RATE, ZSZFDW, PERFORBONUS_TOTAL_SUB, MONTHLY_BONUS_SUB, LXJ, SPEC_POST_SUB, AGENCY_FUNCTION, AGENCY_EXE_ID, FISCAL_YEAR, ADMIN_WORKER_STAF_NUM, NST_WORKER_STAF_NUM, LAW_NUM, ENROLL_SCH_STUD_NUM, LODG_SCH_STUD_NUM, FIN_BGT_PAY_MONEY, SPECIAL_PAY_MONEY, FUND_BGT_PAY_MONEY, FIN_SPECIAL_INCOME, FIN_ASSIST_INCOME, ANN_TOTAL_INCOME, ANN_TOTAL_PAY, ANN_PER_PAY, ANN_PUB_PAY, ANN_SPECIAL_PAY, VERSION, VERSION_NAME, CJRJYJ)
      SELECT
      vrow.agency_id,
      vrow.biz_key, '0', '0', '0', '0', '0', to_date('20-10-2021 15:08:29', 'dd-mm-yyyy hh24:mi:ss'), to_date('12-12-2099', 'dd-mm-yyyy'), '1', to_date('02-12-2022 14:09:40', 'dd-mm-yyyy hh24:mi:ss'), '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', to_date('01-12-2022 14:49:18', 'dd-mm-yyyy hh24:mi:ss'), '136491', '433127198907280333', '脚本迁移', '136491', '433127198907280333', '脚本迁移', null, null, null, null, '0',
			vrow.agency_code,
      vrow.mof_div_code, '0', '0', '0', '0', '0', null, null, '0', null, '0', null, '0', '0', '0', '0', null, null, null, null, null, null, '0', null, '0', null, null, null, null, null, null, null, null, null,
			vrow.fiscal_year, '0', '0', '0', '0', '0', null, null, null, null, null, null, null, null, null, null,
			vrow.version,
      vrow.version || '日常动态维护版本', null
      from dual where 1=1;
    end if;

    --提交
    commit;
  end loop;

end;
