# GitToolBox 插件介绍

"GitToolBox" 是一款强大的 IntelliJ IDEA 插件，旨在提供更丰富和便捷的 Git 版本控制功能。该插件为开发者提供了许多增强的 Git 工具和功能，帮助他们更高效地进行版本控制管理和协作开发。  
这个插件的主要功能和特点包括：

1. **增强的 Git 日志**：提供更全面、可定制的 Git 日志视图，支持快速检查提交历史、分支情况等，并可显示详细的提交信息。

2. **增强的 Diff 工具**：集成了更多强大的文件差异比较工具，可以更直观地查看代码变更，便于代码审查和合并操作。

3. **分支管理**：提供更便捷的分支管理功能，包括创建、切换、合并、删除分支等操作，同时提供了可视化的分支关系图。

4. **Pull Request 集成**：能够直接在 IDE 中查看和管理 GitHub 或 GitLab 上的 Pull Request，方便进行代码审查和合并操作。

5. **改进的提交操作**：提供更丰富的提交操作功能，如提交部分文件、提交交互式修改等，使提交更加灵活和细粒度化。

6. **更多辅助功能**：支持快速切换远程分支、查看未提交的变更、集成代码质量检测等功能，提升开发体验。

总之，"GitToolBox" 插件通过增强 IntelliJ IDEA 对 Git 版本控制的支持，提供了更多便捷、高效的工具和功能，帮助开发者更好地管理和协作开发代码。对于使用 Git 进行版本控制的开发团队来说，这个插件能够有效提升开发效率和代码质量。

官网：

1. https://gittoolbox.lukasz-zielinski.com/

2. https://plugins.jetbrains.com/plugin/7499-gittoolbox/versions

3. https://github.com/zielu/GitToolBox/wiki/Manual

# 插件安装

进入 **Settings** **\->** **Plugins** **\->** **Marketplace** \*\*-> \*\*在搜索框输入`GitToolBox`，点击`Installed`安装，效果如下图：  

![图片](images/DM_20240328173012_002.png)

![图片](images/DM_20240328173012_003.png)

# 插件使用

GitToolBox插件安装好后，在Git管理的代码中直接能够看到这行代码的作者，时间，Commit信息，效果如下图：  

![图片](images/DM_20240328173012_001.GIF)