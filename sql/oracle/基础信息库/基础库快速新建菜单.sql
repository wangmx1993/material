
-- 全局替换新旧功能ID，以及名称、功能 URL 即可
-- 新功能ID=D46C1ABDC4172992E055000000000001
-- 复制旧功能ID=816 的角色，菜单、工作流权限

--  1、添加功能
insert into gap_ui_module (CODE, NAME, URL, MODULE_CAT_ID, IS_ACCOUNT, MODULE_TYPE, POWER_SIZE, IS_DELETED, REMARK, WEB_SERVER_ID, ICON, IS_SPECIAL, IS_PORTLET, IS_REPORT, IS_ELEMENT, IS_WFMONITOR, IS_USE_UIVIEW, IS_BPM, IS_MODULECONTAINER, OPEN_TYPE, TODO_ENABLED, TODO_SERVER_ID, IS_PORTLETCONTAINER, UIVIEW_GROUP_CODE, PARAMETER, IS_VIEW_GROUP_MANAGE, DATA_SOURCE, CARD_CATE_ID, REPORT_CATE, ID)
select
'Bas_common_interface_config', '对外公共接口配置', 'grp/html/presonInfo/commonInterfaceConfig.html', MODULE_CAT_ID, IS_ACCOUNT, MODULE_TYPE, POWER_SIZE, IS_DELETED, REMARK, WEB_SERVER_ID, ICON, IS_SPECIAL, IS_PORTLET, IS_REPORT, IS_ELEMENT, IS_WFMONITOR, IS_USE_UIVIEW, IS_BPM, IS_MODULECONTAINER, OPEN_TYPE, TODO_ENABLED, TODO_SERVER_ID, IS_PORTLETCONTAINER, UIVIEW_GROUP_CODE, PARAMETER, IS_VIEW_GROUP_MANAGE, DATA_SOURCE,
CARD_CATE_ID, REPORT_CATE, 'D46C1ABDC4172992E055000000000001'
from gap_ui_module where id='816';


-- 2、角色对功能，复制原来"(module_id=816)"功能相同的角色.
-- plsql 脚本请单独执行
declare
   cursor vrows is SELECT t.role_id,t.module_id FROM gap_role_module t where t.module_id in('816');
begin
   for vrow in vrows loop
     insert into gap_role_module (ROLE_ID, MODULE_ID) values (vrow.role_id, 'D46C1ABDC4172992E055000000000001');
      commit;
	  dbms_output.put_line('[对外公共接口配置]角色设置成功!');
   end loop;
end;


-- 3、新建菜单，在原来"(module_id=816)"功能菜单相同的位置新建此菜单.
-- plsql 脚本请单独执行
declare
   cursor vrows is SELECT t.* FROM gap_menu t where t.module_id in('816');
begin
   for vrow in vrows loop
     insert into gap_menu (NAME, MENU_GROUP_ID, PARENT_ID, DISP_ORDER, IS_SEPERATOR, ICON, IS_SHOW_NUM, IS_PARENT_SUM, MODULE_LEVEL, SHORTCUT_KEY, IS_VISIBLE, MENU_TYPE, MODULE_ID, PARAMETER, UIVIEW_GROUP_ID, ID)
     values ('对外公共接口配置', vrow.MENU_GROUP_ID, vrow.PARENT_ID,
     (SELECT max(t2.disp_order) +1 FROM gap_menu t2 where t2.parent_id=vrow.PARENT_ID),
     0, null, 0, 0, 0, null, 1, vrow.MENU_TYPE, 'D46C1ABDC4172992E055000000000001', null, null, 20211201 || vrow.ID);
     dbms_output.put_line('[对外公共接口配置]菜单新建成功!');
     commit;
   end loop;
end;

