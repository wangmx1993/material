CREATE OR REPLACE PROCEDURE PROC_GAP_UI_COLUMN_ADD_COL3(p_view_code        in varchar2, --UI视图表中的CODE，会字段处理 xxx_detail 的录入视图
                                                        p_query_table_code in varchar2, --UI视图表中的查询表名，用于进一步限制查询视图的条件，可以为空。
                                                        p_field_code       in varchar2, --字段英文名称
                                                        p_title            in varchar2, --字段中文名称
                                                        p_is_enabled       in number, --是否可编辑
                                                        p_is_required      in number, --是否必填
                                                        p_data_type        in varchar2, --数据类型,列表视图如果配置的单选框，则自动会转为下拉框
                                                        p_max_length       in number, --最大允许长度,没有时传0即可
                                                        p_dec_len          in varchar2, --金额的小数位数,没有时传0即可
                                                        p_ele_code         in varchar2, --要素简称，没有时传null即可
                                                        p_ele_show_type    in varchar2, --要素显示方式，没有时传0即可
                                                        p_ele_level_num    in varchar2) --要素显示级次，没有时传1即可
 is

  v_server_code     varchar2(38) := 'basic-web'; --web服务名称
  v_server_id_count number := 0; --web服务ID个数，只能有1个
  v_server_id       varchar2(40) := '0'; --web服务ID

  view_vrows sys_refcursor; --系统引用游标，遍历UI视图
  view_vrow  gap_ui_view%rowtype; --声明UI视图游标接收变量

  v_id                   varchar2(36); -- 插入字段的字段 id，guid 自动生成
  v_disp_order           number := 999; --字段的顺序号，再原视图最大序号的基础上 +5
  v_row_sapn             varchar2(60) := 1; --新增字段的跨行数
  v_is_have_target_field number := 0; --准备新增的字段是否已经存在了，如果已经存在，则不再新增
  v_is_have_field        number := 0; --视图是否本身含有列信息，如果视图本身没有列信息，则不新增

begin
  --第一步：查询web服务ID，如果没有查询到，则直接抛异常，后续不再继续操作.
  SELECT count(1)
    into v_server_id_count
    FROM GAP_SYSTEM_SERVER t
   where upper(t.code) = upper(v_server_code)
     and t.is_enabled = 1;
  if v_server_id_count != 1 then
    raise_application_error(-20001,
                            '查询基础库web服务ID失败，请检查存储过程编写是否有误！');
  end if;

  SELECT T.Id
    into v_server_id
    FROM GAP_SYSTEM_SERVER t
   where upper(t.code) = upper(v_server_code);
  dbms_output.put_line('web服务ID=' || v_server_id);

  --第二步：查询整个服务所有功能下对应的视图(默认只为省本级和公共视图添加)
  open view_vrows for
    SELECT *
      FROM gap_ui_view
     where gap_ui_view.id in
           (SELECT GAP_UI_VIEW_GROUP_VIEW.View_Id
              FROM GAP_UI_VIEW_GROUP_VIEW
             where GAP_UI_VIEW_GROUP_VIEW.VIEW_GROUP_ID in
                   (select t.id
                      from GAP_UI_VIEW_GROUP t
                     start with t.code in
                                (SELECT distinct t.uiview_group_code
                                   FROM gap_ui_module t
                                  where t.web_server_id = v_server_id
                                    and t.uiview_group_code is not null)
                    connect by prior t.id = t.parent_id))
       and gap_ui_view.code in (p_view_code, p_view_code || '_detail')
       --and gap_ui_view.tenant_id in ('0', '430000000') --默认只为省本级和公共视图添加
       and gap_ui_view.name not like '%单位新增%' --单位新增申请页面不进行修改.
       and gap_ui_view.name not like '%单位申请%' --单位变更、撤销申请页面不进行修改.
       and case --有限定条件时拼接限定条件，尽可能的不要影响别人的配置
             when p_query_table_code is not null then
              case
                when gap_ui_view.query_table_code = p_query_table_code then
                 1
                else
                 2
              end
             else
              1
           end = 1;

  --第三步：遍历UI视图游标
  loop
    fetch view_vrows
      into view_vrow;
    exit when view_vrows%notfound; -- 游标读取不到值时退出循环

    --如果视图已经有目标列了，则不再新增
    SELECT count(1)
      into v_is_have_target_field
      FROM GAP_UI_COLUMN
     where GAP_UI_COLUMN.view_id = view_vrow.id
       and GAP_UI_COLUMN.Field_Code = p_field_code;

    if v_is_have_target_field < 1 then
      --如果视图本身没有列信息，也不新增
      SELECT count(1)
        into v_is_have_field
        FROM GAP_UI_COLUMN
       where GAP_UI_COLUMN.view_id = view_vrow.id
         and GAP_UI_COLUMN.Is_Visible = 1;

      if v_is_have_field > 0 then
        v_id := sys_guid(); --主键
        SELECT nvl(max(Tc.Disp_Order), 0) + 1
          into v_disp_order
          FROM GAP_UI_COLUMN tc
         where tc.view_id = view_vrow.id
           and tc.is_visible = 1
           and tc.field_code not in
               ('uOpration',
                'opration',
                'fujian',
                'affix_type',
                'operateWatch_code'); --新字段排在业务字段最末尾，但是排在附件等列的前面

        SELECT nvl(max(to_number(tc.col_level)), 1)
          into v_row_sapn
          FROM GAP_UI_COLUMN tc
         where tc.view_id = view_vrow.id
           and tc.is_visible = 1; --如果是多表头，则设置跨行数

        --新增字段，如果列信息与实际环境对不上上，自行调整即可

        insert into GAP_UI_COLUMN
          (FIELD_CODE,
           NAME,
           DISP_ORDER,
           TITLE,
           DATA_TYPE,
           IS_VISIBLE,
           IS_ENABLED,
           DEFAULT_VALUE,
           COLUMN_WIDTH,
           COLUMN_HEIGHT,
           ALIGNMENT,
           DEC_LEN,
           ELE_CODE,
           ELE_SHOW_TYPE,
           ELE_LEVEL_NUM,
           ELE_CODE_PRIMARY,
           ELE_CODE_RELATED,
           IS_CALCULATED,
           IS_REQUIRED,
           EXPRESSION,
           PARENT_ID,
           ROW_SPAN,
           COL_SPAN,
           COL_LEVEL,
           MAX_LENGTH,
           MIN_LENGTH,
           IS_FROZEN,
           REMARK,
           IS_COLUMN_SORT,
           TIPS,
           IS_LOADELEMENT,
           SWHERE,
           ELE_BOUND_VALUE,
           ELE_DISPLAY_VALUE,
           ROW_WRAP,
           ID,
           VIEW_ID,
           JUMP_GROUP_ID)
          select lower(p_field_code) as field_code,
                 p_title as name,
                 v_disp_order as DISP_ORDER, -- 动态赋值
                 p_title as TITLE,
                 --列表视图不建议配置单选框，建议配置下拉框
                 (case
                   when view_vrow.view_type = 3 and p_data_type = 3 then
                    '1'
                   else
                    p_data_type
                 end) as DATA_TYPE,
                 1 as IS_VISIBLE,
                 p_is_enabled as IS_ENABLED,
                 null as DEFAULT_VALUE,
                 '120' as COLUMN_WIDTH,
                 null as COLUMN_HEIGHT,
                 '0' as ALIGNMENT,
                 p_dec_len as DEC_LEN,
                 p_ele_code as ELE_CODE,
                 p_ele_show_type as ELE_SHOW_TYPE,
                 p_ele_level_num as ELE_LEVEL_NUM,
                 null as ELE_CODE_PRIMARY,
                 null as ELE_CODE_RELATED,
                 (case
                   when p_data_type = 8 then
                    1
                   else
                    0
                 end) as IS_CALCULATED, -- 金额计算，其他不计算
                 p_is_required as IS_REQUIRED,
                 null as EXPRESSION,
                 '0' as PARENT_ID,
                 v_row_sapn as ROW_SPAN, -- 动态赋值
                 '1' as COL_SPAN,
                 '1' as COL_LEVEL,
                 (case
                   when p_max_length is null then
                    0
                   else
                    p_max_length
                 end) as MAX_LENGTH, --默认为 0
                 0 as MIN_LENGTH,
                 0 as IS_FROZEN,
                 '批量新增' || to_char(sysdate, 'yyyymmdd') as REMARK, --做个特殊记号，方便批量查询新增好的字段
                 1 as IS_COLUMN_SORT,
                 null as TIPS,
                 1 as IS_LOADELEMENT,
                 null as SWHERE,
                 null as ELE_BOUND_VALUE,
                 null as ELE_DISPLAY_VALUE,
                 null as ROW_WRAP,
                 v_id as ID, -- 动态赋值
                 view_vrow.id as VIEW_ID,
                 null JUMP_GROUP_ID
            from dual;

        dbms_output.put_line('view_id=' || view_vrow.id || ' view_code=' ||
                             view_vrow.code || ' view_name=' ||
                             view_vrow.name || ' col_id=' || v_id || ' ' ||
                             p_field_code || ' ' || v_disp_order || ' ' ||
                             v_row_sapn);
        commit;
      end if;
    end if;
  end loop;
  close view_vrows; --关闭UI视图游标

end;


/





--单位基本信息
CALL PROC_GAP_UI_COLUMN_ADD_COL3('201001', null, 'agency_leader_per_cont', '单位负责人联系方式', 1, 0, 0, 60, 0, null, null, null);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('201001', null, 'disc_year', '停用年度', 1, 0, 0, 4, 0, null, null, null);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('201001', null, 'fi_leader', '财务负责人', 1, 1, 0, 20, 0, null, null, null);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('201001', null, 'fi_leader_cont', '财务负责人联系方式', 1, 0, 0, 60, 0, null, null, null);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('201001', null, 'fi_lessor', '财务联系人', 1, 0, 0, 20, 0, null, null, null);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('201001', null, 'fi_lessor_cont', '财务联系人联系方式', 1, 0, 0, 60, 0, null, null, null);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('201001', null, 'is_person_quo', '是否定员定额', 1, 0, 3, 0, 0, 'Bool', 3, 9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('201001', null, 'is_refe_civ_serv_law_mana', '是否参照公务员法管理', 1, 0, 3, 0, 0, 'Bool', 3, 9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('201001', null, 'refe_type_code','参照类型',1,0,5,0,0,'RefeType',3,9);

--单位扩展信息
CALL PROC_GAP_UI_COLUMN_ADD_COL3('201002', null, 'actu_non_staff_num','实有编外人数',1,0,7,0,0,null,1,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('201002', null, 'actu_ret_num','实有离退休人数',1,0,7,0,0,null,1,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('201002', null, 'an_ca_qu_med_uni_ret','退休人员医保单位缴费年人均定额',1,0,7,0,2,null,1,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('201002', null, 'an_pe_ca_qu_med_uni_em_esta','编制内在职人员医保单位缴费年人均定额',1,0,7,0,2,null,1,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('201002', null, 'an_pe_ca_uni_ret_per','离休人员医保单位缴费年人均定额',1,0,7,0,2,null,1,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('201002', null, 'med_ins_un_pro_in_ser_est','编制内在职人员医保单位缴费比例',1,0,7,0,6,null,1,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('201002', null, 'oth_worker_staf_num','人员编制数-其他',1,0,7,0,0,null,1,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('201002', null, 'pay_pro_med_uni_ret','退休人员医保单位缴费比例',1,0,8,0,6,null,1,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('201002', null, 'pay_pro_med_uni_ret_per','离休人员医保单位缴费比例',1,0,8,0,6,null,1,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('201002', null, 'pay_pro_work_rel_uni_em_est','编制内在职人员工伤保险单位缴费比例',1,0,8,0,6,null,1,1);


--人员基本信息
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202005', null, 'internal_dep_code','单位内部机构',1,0,5,0,0,null,3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202006', null, 'internal_dep_code','单位内部机构',1,0,5,0,0,null,3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202007', null, 'internal_dep_code','单位内部机构',1,0,5,0,0,null,3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202008', null, 'internal_dep_code','单位内部机构',1,0,5,0,0,null,3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202009', null, 'internal_dep_code','单位内部机构',1,0,5,0,0,null,3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202005', null, 'is_other_rig_exp_per','是否保民生及其他刚性支出人员',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202006', null, 'is_other_rig_exp_per','是否保民生及其他刚性支出人员',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202007', null, 'is_other_rig_exp_per','是否保民生及其他刚性支出人员',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202008', null, 'is_other_rig_exp_per','是否保民生及其他刚性支出人员',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202009', null, 'is_other_rig_exp_per','是否保民生及其他刚性支出人员',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202005', null, 'is_whe_par_endo_ins','是否参加养老保险',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202006', null, 'is_whe_par_endo_ins','是否参加养老保险',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202007', null, 'is_whe_par_endo_ins','是否参加养老保险',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202008', null, 'is_whe_par_endo_ins','是否参加养老保险',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202009', null, 'is_whe_par_endo_ins','是否参加养老保险',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202005', null, 'is_whe_par_med_ins','是否参加医疗保险',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202006', null, 'is_whe_par_med_ins','是否参加医疗保险',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202007', null, 'is_whe_par_med_ins','是否参加医疗保险',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202008', null, 'is_whe_par_med_ins','是否参加医疗保险',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202009', null, 'is_whe_par_med_ins','是否参加医疗保险',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202005', null, 'is_whe_par_work_ins','是否参加工伤保险',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202006', null, 'is_whe_par_work_ins','是否参加工伤保险',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202007', null, 'is_whe_par_work_ins','是否参加工伤保险',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202008', null, 'is_whe_par_work_ins','是否参加工伤保险',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202009', null, 'is_whe_par_work_ins','是否参加工伤保险',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202005', null, 'one_merit_ser_bon','立功受奖一次性奖金',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202006', null, 'one_merit_ser_bon','立功受奖一次性奖金',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202007', null, 'one_merit_ser_bon','立功受奖一次性奖金',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202008', null, 'one_merit_ser_bon','立功受奖一次性奖金',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202009', null, 'one_merit_ser_bon','立功受奖一次性奖金',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('000000', 'VW_BAS_PERSON', 'internal_dep_code','单位内部机构',1,0,5,0,0,null,3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('000000', 'VW_BAS_PERSON', 'is_other_rig_exp_per','是否保民生及其他刚性支出人员',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('000000', 'VW_BAS_PERSON', 'is_whe_par_endo_ins','是否参加养老保险',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('000000', 'VW_BAS_PERSON', 'is_whe_par_med_ins','是否参加医疗保险',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('000000', 'VW_BAS_PERSON', 'is_whe_par_work_ins','是否参加工伤保险',1,0,3,0,0,'Bool',3,9);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('000000', 'VW_BAS_PERSON', 'one_merit_ser_bon','立功受奖一次性奖金',1,0,8,11,2,null,0,1);

--人员扩展信息
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202005', null, 'basic_per_award','基础绩效奖',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202006', null, 'basic_per_award','基础绩效奖',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202007', null, 'basic_per_award','基础绩效奖',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202008', null, 'basic_per_award','基础绩效奖',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202009', null, 'basic_per_award','基础绩效奖',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202005', null, 'regu_sub','规范津贴补贴',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202006', null, 'regu_sub','规范津贴补贴',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202007', null, 'regu_sub','规范津贴补贴',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202008', null, 'regu_sub','规范津贴补贴',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202009', null, 'regu_sub','规范津贴补贴',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202005', null, 'reformative_sub','改革性补贴',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202006', null, 'reformative_sub','改革性补贴',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202007', null, 'reformative_sub','改革性补贴',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202008', null, 'reformative_sub','改革性补贴',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202009', null, 'reformative_sub','改革性补贴',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202005', null, 'achievement_bonus','绩效奖金',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202006', null, 'achievement_bonus','绩效奖金',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202007', null, 'achievement_bonus','绩效奖金',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202008', null, 'achievement_bonus','绩效奖金',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202009', null, 'achievement_bonus','绩效奖金',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202005', null, 'annual_ass_award','年度考核奖',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202006', null, 'annual_ass_award','年度考核奖',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202007', null, 'annual_ass_award','年度考核奖',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202008', null, 'annual_ass_award','年度考核奖',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202009', null, 'annual_ass_award','年度考核奖',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202005', null, 'year_lump_sunm_bon','年终一次性奖金',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202006', null, 'year_lump_sunm_bon','年终一次性奖金',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202007', null, 'year_lump_sunm_bon','年终一次性奖金',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202008', null, 'year_lump_sunm_bon','年终一次性奖金',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202009', null, 'year_lump_sunm_bon','年终一次性奖金',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202005', null, 'on_sup_pay_med_in_re','医疗保险退休人员一次性补缴经费',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202006', null, 'on_sup_pay_med_in_re','医疗保险退休人员一次性补缴经费',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202007', null, 'on_sup_pay_med_in_re','医疗保险退休人员一次性补缴经费',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202008', null, 'on_sup_pay_med_in_re','医疗保险退休人员一次性补缴经费',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('202009', null, 'on_sup_pay_med_in_re','医疗保险退休人员一次性补缴经费',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('000000', 'VW_BAS_PERSON', 'basic_per_award','基础绩效奖',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('000000', 'VW_BAS_PERSON', 'regu_sub','规范津贴补贴',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('000000', 'VW_BAS_PERSON', 'reformative_sub','改革性补贴',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('000000', 'VW_BAS_PERSON', 'achievement_bonus','绩效奖金',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('000000', 'VW_BAS_PERSON', 'annual_ass_award','年度考核奖',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('000000', 'VW_BAS_PERSON', 'year_lump_sunm_bon','年终一次性奖金',1,0,8,11,2,null,0,1);
CALL PROC_GAP_UI_COLUMN_ADD_COL3('000000', 'VW_BAS_PERSON', 'on_sup_pay_med_in_re','医疗保险退休人员一次性补缴经费',1,0,8,11,2,null,0,1);
