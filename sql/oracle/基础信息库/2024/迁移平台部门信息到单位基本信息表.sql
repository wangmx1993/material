
--================《方案：单位信息父节点ID改造.docx》./start=============

-- 1、让新字段pid代替原来的业务，用于存储平台单位要素的父节点ID
declare
  cursor vrows is
    SELECT mof_div_code FROM bas_agency_info T group by t.mof_div_code;
begin
  for vrow in vrows loop
    UPDATE bas_agency_info T SET pid = parent_id WHERE t.mof_div_code = vrow.mof_div_code and pid is null;
    commit;
  end loop;
end;
/

-- 2、先补充基础库缺少的部门信息
-- 全局搜索年度进行替换
MERGE INTO bas_agency_info info
USING (SELECT T.*
         FROM ele_agency t
        where t.is_deleted = 2
          and t.is_leaf != 1
          and length(t.ele_code) in (3, 6, 9, 12, 15, 18, 21)) ele
ON (info.mof_div_code = ele.mof_div_code and info.agency_code = ele.ele_code and info.fiscal_year = '2024' and info.version = '2024rcdtwh' and info.is_deleted = 2)
WHEN NOT MATCHED THEN
  INSERT
  --默认补充部门需要的必填字段，如果还有漏掉的，自己给上默认值就行
    (info.AGENCY_ABBREVIATION,
     info.AGENCY_ADM_LEVEL_CODE,
     info.AGENCY_ADM_LEVEL_ID,
     info.AGENCY_ADM_LEVEL_NAME,
     info.AGENCY_CODE,
     info.AGENCY_ID,
     info.AGENCY_LEADER_PER_CONT,
     info.AGENCY_LEADER_PER_NAME,
     info.AGENCY_NAME,
     info.AGENCY_TYPE_CODE,
     info.AGENCY_TYPE_ID,
     info.AGENCY_TYPE_NAME,
     info.AUDIT_ID,
     info.AGENCY_TEL,
     info.AGENCY_IDEN_ID,
     info.AGENCY_IDEN_CODE,
     info.AGENCY_IDEN_NAME,
     info.AGENCY_CLASS_ID,
     info.AGENCY_CLASS_CODE,
     info.AGENCY_CLASS_NAME,
     info.ADMIN_MAN_DEP_ID,
     info.ADMIN_MAN_DEP_CODE,
     info.ADMIN_MAN_DEP_NAME,
     info.BIZ_KEY,
     info.CAPITAL_TYPE_ID,
     info.CAPITAL_TYPE,
     info.CAPITAL_TYPE_NAME,
     info.CHANGE_TYPE_CODE,
     info.CHANGE_TYPE_NAME,
     info.CREATE_MENU_ID,
     info.CREATE_TIME,
     info.CREATE_USER_CODE,
     info.CREATE_USER_ID,
     info.CREATE_USER_NAME,
     info.END_DATE,
     info.executive_acc_id,
     info.executive_acc,
     info.executive_acc_name,
     info.FISCAL_YEAR,
     info.FI_LEADER,
     info.FI_LEADER_CONT,
     info.FI_LESSOR,
     info.FI_LESSOR_CONT,
     info.FIN_MAN_DEP_id,
     info.FIN_MAN_DEP_CODE,
     info.FIN_MAN_DEP_name,
     info.FROM_AGENCY_ID,
     info.IND_CODE,
     info.IND_ID,
     info.IND_NAME,
     info.IS_DELETED,
     info.IS_ENABLED,
     info.IS_END,
     info.IS_LAST_INST,
     info.IS_LEAF,
     info.IS_PRE_DEPAR_FIN_ACCOUNTS,
     info.IS_PRE_FIN_REP_GOV_DEP,
     info.IS_PRE_STA_OWN_ASSE_REP_ADM,
     info.IS_REFE_CIV_SERV_LAW_MANA,
     info.IS_PERSON_QUO,
     info.IS_FISCAL_UNIT,
     info.IS_BUDGETING_UNIT,
     info.LEVEL_NO,
     info.LOCATION,
     info.location_id,
     info.location_name,
     info.MOF_DEP_CODE,
     info.MOF_DEP_ID,
     info.MOF_DEP_NAME,
     info.MOF_DIV_CODE,
     info.MOF_DIV_NAME,
     info.START_DATE,
     info.UI_CODE,
     info.UI_GROUP,
     info.UI_NAME,
     info.UNIFSOC_CRED_CODE,
     info.UPDATE_TIME,
     info.UPDATE_USER_CODE,
     info.UPDATE_USER_ID,
     info.UPDATE_USER_NAME,
     info.VERSION,
     info.VERSION_NAME,
     info.ZIP,
     info.pid)
  VALUES
    (ele.ele_name, --AGENCY_ABBREVIATION
     '99', --AGENCY_ADM_LEVEL_CODE
     (case when
(SELECT ele_.Ele_Id FROM ELE_UNION ele_ where ele_.ele_catalog_code='AgencyAdmLevel' and ele_.ele_code='99' and rownum <= 1) is null
then '99' else (SELECT ele_.Ele_Id FROM ELE_UNION ele_ where ele_.ele_catalog_code='AgencyAdmLevel' and ele_.ele_code='99' and rownum <= 1) end
  ), --AGENCY_ADM_LEVEL_ID
     '无', --AGENCY_ADM_LEVEL_NAME
     ele.ele_CODE, --AGENCY_CODE
     sys_guid(), --AGENCY_ID
     '82666888', --AGENCY_LEADER_PER_CONT
     '某某', --AGENCY_LEADER_PER_NAME
     ele.ele_NAME, --AGENCY_NAME
     '9', --AGENCY_TYPE_CODE
     (case when
(SELECT ele_.ele_id FROM ELE_ADMDEPTYPE ele_ where ele_.ele_code='9' and rownum <= 1) is null
then '9' else (SELECT ele_.ele_id FROM ELE_ADMDEPTYPE ele_ where ele_.ele_code='9' and rownum <= 1) end
  ), --AGENCY_TYPE_ID
     '其他单位', --AGENCY_TYPE_NAME
     '0', --AUDIT_ID
     '01012345678', --AGENCY_TEL
  (case when(SELECT ele_.ele_id FROM ELE_AGENCY_IDEN_REMARK ele_ where ele_.ele_code='09' and rownum <= 1) is null
then '09' else (SELECT ele_.ele_id FROM ELE_AGENCY_IDEN_REMARK ele_ where ele_.ele_code='09' and rownum <= 1) end
  )
  , -- AGENCY_IDEN_ID
     '09', -- AGENCY_IDEN_CODE
     '其他', --AGENCY_IDEN_NAME
    (case when(SELECT ele_.ele_id FROM ELE_AGENCY_CLASS_CODE ele_ where ele_.ele_code='100399' and rownum <= 1) is null
then '100399' else (SELECT ele_.ele_id FROM ELE_AGENCY_CLASS_CODE ele_ where ele_.ele_code='100399' and rownum <= 1) end
  ) , --AGENCY_CLASS_ID
     '100399', --AGENCY_CLASS_CODE
     '其他（财政）', --AGENCY_CLASS_NAME
     (case when
(SELECT ele_.Ele_Id FROM ELE_UNION ele_ where ele_.ele_catalog_code='Supdep' and ele_.ele_code='999' and rownum <= 1) is null
then '999' else (SELECT ele_.Ele_Id FROM ELE_UNION ele_ where ele_.ele_catalog_code='Supdep' and ele_.ele_code='999' and rownum <= 1) end
  ), -- ADMIN_MAN_DEP_ID
     '999', --ADMIN_MAN_DEP_CODE
     '其他部门', --ADMIN_MAN_DEP_NAME
     ele.ele_id, --BIZ_KEY
      (case when
(SELECT ele_.Ele_Id FROM ELE_UNION ele_ where ele_.ele_catalog_code='CapitalType' and ele_.ele_code='9' and rownum <= 1) is null
then '9' else (SELECT ele_.Ele_Id FROM ELE_UNION ele_ where ele_.ele_catalog_code='CapitalType' and ele_.ele_code='9' and rownum <= 1) end
  ), --CAPITAL_TYPE_ID
     '9', --CAPITAL_TYPE
     '其他', --CAPITAL_TYPE_NAME
     '21', --CHANGE_TYPE_CODE
     '单位新增', --CHANGE_TYPE_NAME
     '0', --CREATE_MENU_ID
     sysdate, --CREATE_TIME
     '0', --CREATE_USER_CODE
     '0', --CREATE_USER_ID
     'parent_id改造', --CREATE_USER_NAME
     ele.END_DATE, --END_DATE
    (case when
    (SELECT ele_.ele_id FROM ELE_UNION ele_ where ele_.ele_catalog_code='ExecutiveAcc' and ele_.ele_code='19' and rownum <= 1) is null
    then '19' else (SELECT ele_.ele_id FROM ELE_UNION ele_ where ele_.ele_catalog_code='ExecutiveAcc' and ele_.ele_code='19' and rownum <= 1) end
      )  , --executive_acc_id
     '19', --executive_acc
     '财政总会计制度', --executive_acc_name
     '2024', --FISCAL_YEAR
     '某某', --FI_LEADER
     '82666888', --FI_LEADER_CONT
     '某某', --FI_LESSOR
     '82666888', --FI_LESSOR_CONT
     (case when
(SELECT ele_.ele_id FROM ELE_UNION ele_ where ele_.ele_catalog_code='Supdep' and ele_.ele_code='999' and rownum <= 1) is null
then '999' else (SELECT ele_.ele_id FROM ELE_UNION ele_ where ele_.ele_catalog_code='Supdep' and ele_.ele_code='999' and rownum <= 1) end
  ), --FIN_MAN_DEP_id
    '999' , --FIN_MAN_DEP_code
     '其他部门', --FIN_MAN_DEP_name
     '0', --FROM_AGENCY_ID
     'A0190', --IND_CODE
     (case when
(SELECT ele_.ele_id FROM ELE_IND ele_ where ele_.ele_code='A0190' and rownum <= 1) is null
then 'A0190' else (SELECT ele_.ele_id FROM ELE_IND ele_ where ele_.ele_code='A0190' and rownum <= 1) end
  ), --IND_ID
     '其他农业', --IND_NAME
     2, --IS_DELETED
     1, --IS_ENABLED
     1, --IS_END
     1, --IS_LAST_INST
     2, --IS_LEAF
     1, --IS_PRE_DEPAR_FIN_ACCOUNTS
     1, --IS_PRE_FIN_REP_GOV_DEP
     1, --IS_PRE_STA_OWN_ASSE_REP_ADM
     1, --IS_REFE_CIV_SERV_LAW_MANA
     1, --IS_PERSON_QUO
     1, --IS_FISCAL_UNIT
     1, --IS_BUDGETING_UNIT
     ele.LEVEL_NO, --LEVEL_NO
     ele.mof_div_code, --LOCATION
     nvl(((SELECT mof.mof_div_id FROM bas_mof_div mof where mof.mof_div_code=ele.MOF_DIV_CODE and mof.is_deleted = 2 and rownum <= 1)),ele.MOF_DIV_CODE ),--location_id
     nvl(((SELECT mof.mof_div_name FROM bas_mof_div mof where mof.mof_div_code=ele.MOF_DIV_CODE and mof.is_deleted = 2 and rownum <= 1)),ele.MOF_DIV_CODE ),--location_name
     nvl((SELECT dep.mof_dep_code FROM bas_mof_dep dep where dep.mof_dep_id=ele.mof_dep_id and rownum <=1),' '), --MOF_DEP_CODE
     nvl(ele.mof_dep_id, ' '), --MOF_DEP_ID
     nvl((SELECT dep.mof_dep_name FROM bas_mof_dep dep where dep.mof_dep_id=ele.mof_dep_id and rownum <=1),' '), --MOF_DEP_NAME
     ele.MOF_DIV_CODE, --MOF_DIV_CODE
     nvl(((SELECT mof.mof_div_name FROM bas_mof_div mof where mof.mof_div_code=ele.MOF_DIV_CODE and mof.is_deleted = 2 and rownum <= 1)),ele.MOF_DIV_CODE ), --MOF_DIV_NAME
     ele.START_DATE, --START_DATE
     '201001,201002', --UI_CODE
     'BAS_AGENCY', --UI_GROUP
     '单位基本信息，单位扩展信息', --UI_NAME
     nvl(ele.unifsoc_cred_code, to_char(to_number(to_char(sysdate, 'yymmddhh24miSS')) || lpad(abs(mod(dbms_random.random, 1000)), 6, 0))), --UNIFSOC_CRED_CODE
     sysdate, --UPDATE_TIME
     '0', --UPDATE_USER_CODE
     '0', --UPDATE_USER_ID
     'parent_id改造', --,UPDATE_USER_NAME
     '2024rcdtwh', --VERSION
     '2024日常动态维护版', --VERSION_NAME
     '100000',--zip
     ele.parent_id --pid
     );

-- 3、修改子级单位的 parent_id 等于父级部门的 agency_id
declare
  -- 按区划逐个处理
  cursor vrows is
    SELECT mof_div_code
      FROM bas_agency_info T
     where t.is_deleted = 2
       and LENGTH(AGENCY_CODE) > 3
     group by t.mof_div_code;
begin
  -- 当父级已经在表中存在时，则直接修改自己的parent_id等于父级部门的主键ID
  for vrow_mof_div in vrows loop
    update BAS_AGENCY_INFO A
       set parent_id       =
           (SELECT B.AGENCY_ID
              FROM BAS_AGENCY_INFO B
             WHERE A.MOF_DIV_CODE = B.MOF_DIV_CODE
               and a.fiscal_year = b.fiscal_year
               AND SUBSTR(A.AGENCY_CODE, 1, LENGTH(A.AGENCY_CODE) - 3) =
                   B.AGENCY_CODE
               and B.version like '%rcdtwh' --部门是没有财政供养版本数据的
               AND B.IS_DELETED = '2'
               AND rownum <= 1),
           update_time      = to_date(to_char(sysdate, 'yyyy-mm-dd'),
                                      'yyyy-mm-dd'),
           update_user_name = 'parent_id改造'
     WHERE A.mof_div_code = vrow_mof_div.mof_div_code
       and LENGTH(AGENCY_CODE) > 3
       and a.is_deleted = 2
       AND EXISTS
     (SELECT 1
              FROM BAS_AGENCY_INFO B
             WHERE A.MOF_DIV_CODE = B.MOF_DIV_CODE
               and a.fiscal_year = b.fiscal_year
               AND SUBSTR(A.AGENCY_CODE, 1, LENGTH(A.AGENCY_CODE) - 3) =
                   B.AGENCY_CODE
               and B.version like '%rcdtwh' --部门是没有财政供养版本数据的
               AND B.IS_DELETED = '2'
               and nvl(A.parent_id, '0') != nvl(B.agency_id, '0'));
    commit;
  end loop;
end;
/
--================《方案：单位信息父节点ID改造.docx》./end=============
