
-- 处理单位基本信息中的部门信息，将部门信息设置为终审状态。这样部门变更申请才能进行修改并走流程
UPDATE bas_agency_info T2
   SET t2.is_last_inst     = 1,
       t2.is_end           = 1,
       t2.update_time      = sysdate,
       t2.update_user_name = '更新部门为终审状态'
 WHERE (t2.mof_div_code, t2.agency_code) in
       (SELECT t.mof_div_code, t.ele_code
          FROM ele_agency t
         where t.is_deleted = 2
           and sysdate between t.start_date and t.end_date
           and t.is_leaf != 1);

-- 部门信息：如果平台有正确的统一社会信用代码，且基础库的和它不相等，则将基础库的更新为平台的
UPDATE bas_agency_info T2
   SET t2.unifsoc_cred_code =
       (SELECT ele.unifsoc_cred_code
          FROM ele_agency ele
         where t2.mof_div_code = ele.mof_div_code
           and t2.agency_code = ele.ele_code
           and ele.is_deleted = 2
           and sysdate between ele.start_date and ele.end_date
           and ele.is_leaf != 1
           and length(nvl(ele.unifsoc_cred_code, '0')) = 18
           and t2.unifsoc_cred_code != ele.unifsoc_cred_code
           and rownum <= 1),
       t2.update_time      = sysdate,
       t2.update_user_name = '修正部门统一社会信用代码与平台一致'
 WHERE (t2.mof_div_code, t2.agency_code) in
       (SELECT t.mof_div_code, t.ele_code
          FROM ele_agency t
         where t.is_deleted = 2
           and sysdate between t.start_date and t.end_date
           and t.is_leaf != 1
           and length(nvl(t.unifsoc_cred_code, '0')) = 18
           and t2.unifsoc_cred_code != t.unifsoc_cred_code);

-- 部门信息：如果平台没有正确的统一社会信用代码，且基础库的也不正确，则自动为基础库生成一个18位的。
UPDATE bas_agency_info T2
   SET t2.unifsoc_cred_code = to_char(to_number(to_char(sysdate,
                                                        'yymmddhh24miSS')) ||
                                      lpad(abs(mod(dbms_random.random, 1000)),
                                           6,
                                           0)),
       t2.update_time       = sysdate,
       t2.update_user_name  = '修正部门统一社会信用代码'
 WHERE (t2.mof_div_code, t2.agency_code) in
       (SELECT t.mof_div_code, t.ele_code
          FROM ele_agency t
         where t.is_deleted = 2
           and sysdate between t.start_date and t.end_date
           and t.is_leaf != 1
           and length(nvl(t.unifsoc_cred_code, '0')) != 18
           and length(nvl(t2.unifsoc_cred_code, '0')) != 18);


-- 部门信息：设置是否虚拟单位为是，这样将来页面修改部门时，就不会提示:【统一社会信用代码-xxx】已经存在，请确认后重新论入!
-- 且代码中初始化补充的部门信息是否虚拟单位也是默认赋值的是.
UPDATE bas_agency_info T2
   SET t2.Is_Virtual_Unit  = 1,
       t2.update_time      = sysdate,
       t2.update_user_name = '部门设置是否虚拟单位为是'
 WHERE (t2.mof_div_code, t2.agency_code) in
       (SELECT t.mof_div_code, t.ele_code
          FROM ele_agency t
         where t.is_deleted = 2
           and sysdate between t.start_date and t.end_date
           and t.is_leaf != 1)
   and Is_Virtual_Unit = 2;
