create or replace procedure proc_modify_agency_name(agency_code_in      IN VARCHAR2,
                                                    agency_name_in      IN varchar2,
                                                    agency_type_id_in   IN varchar2, --单位类型
                                                    agency_type_code_in IN varchar2,
                                                    agency_type_name_in IN varchar2,
                                                    mof_dep_id_in       IN varchar2, --业务处室
                                                    mof_dep_code_in     IN varchar2,
                                                    mof_dep_name_in     IN varchar2,
                                                    mof_div_code_in     IN varchar2,
                                                    fiscal_year_in      IN varchar2,
                                                    execute_num_out     OUT number) --影响的条数
 as

  --单位信息同步
  --对于程序中没有写死的表，可以通过存储过程进行编写，当发现漏改或者错改了某张表的单位信息时，只需修改存储过程即可。
Begin
  update BAS_AGENCY_INFO_FORMER
     set agency_name      = agency_name_in,
         agency_type_id   = agency_type_id_in,
         agency_type_code = agency_type_code_in,
         agency_type_name = agency_type_name_in,
         mof_dep_id       = mof_dep_id_in,
         mof_dep_code     = mof_dep_code_in,
         mof_dep_name     = mof_dep_name_in
   where agency_code = agency_code_in
     and mof_div_code = mof_div_code_in
     AND FISCAL_YEAR = fiscal_year_in;

  --获取影响的条数
  execute_num_out := sql%rowcount;
End;

/

create or replace function person_biz_key_generate(mof_div_code in varchar2, -- 区划编码
                                                   agency_code  in varchar2, -- 单位编码
                                                   ui_code      in varchar2, -- 人员表单类型编码
                                                   iden_no      in varchar2) -- 证件号

 return varchar2 is
 -- 获取人员信息 biz_key 值，使用 md5 值保证同一个单位，同一人员类型下，反复新增同一个人时，biz_key 保持不变
  new_biz_key varchar2(64);
begin
  SELECT lower(Utl_Raw.Cast_To_Raw(sys.dbms_obfuscation_toolkit.md5(input_string => iden_no || '_' ||
                                                                                    mof_div_code || '_' ||
                                                                                    agency_code || '_' ||
                                                                                    ui_code)))
    into new_biz_key
    FROM dual;
  return new_biz_key;
end;

/
