
---方式1：：--单个单位-----物理删除------------./start------------------

-- 查询指定区划、单位、人员类型、年度、版本下重复的人员信息(即将被删除清理的数据)
-- 同步修改下面的单位编码(AGENCY_CODE)、区划(MOF_DIV_CODE)、人员类型(UI_CODE)、年度(FISCAL_YEAR)、版本(VERSION) 即可。
select COUNT(1) FROM BAS_PERSON_INFO T1
 WHERE AGENCY_CODE = '001888'
   AND MOF_DIV_CODE = '430000000'
   AND UI_CODE = '202005'
   AND IS_DELETED = 2
   AND FISCAL_YEAR = '2022'
   AND VERSION = '2022ysbz'
   AND EXISTS (SELECT 1
          FROM BAS_PERSON_INFO T2
         WHERE AGENCY_CODE = '001888'
           AND MOF_DIV_CODE = '430000000'
           AND UI_CODE = '202005'
           AND IS_DELETED = 2
           AND FISCAL_YEAR = '2022'
           AND VERSION = '2022ysbz'
           AND T1.IDEN_NO = T2.IDEN_NO
         GROUP BY IDEN_NO
        HAVING COUNT(*) > 1)
   AND ROWID NOT IN (SELECT MAX(ROWID)
                       FROM BAS_PERSON_INFO
                      WHERE AGENCY_CODE = '001888'
                        AND MOF_DIV_CODE = '430000000'
                        AND UI_CODE = '202005'
                        AND IS_DELETED = 2
                        AND FISCAL_YEAR = '2022'
                        AND VERSION = '2022ysbz'
                      GROUP BY IDEN_NO
                     HAVING COUNT(*) > 1);

-- 对指定区划、单位、人员类型、年度、版本下的人员信息数据进行去重，保留最后入库的人员数据
-- 同步修改下面的单位编码(AGENCY_CODE)、区划(MOF_DIV_CODE)、人员类型(UI_CODE)、年度(FISCAL_YEAR)、版本(VERSION) 即可。
DELETE FROM BAS_PERSON_INFO T1
 WHERE AGENCY_CODE = '001888'
   AND MOF_DIV_CODE = '430000000'
   AND UI_CODE = '202005'
   AND IS_DELETED = 2
   AND FISCAL_YEAR = '2022'
   AND VERSION = '2022ysbz'
   AND EXISTS (SELECT 1
          FROM BAS_PERSON_INFO T2
         WHERE AGENCY_CODE = '001888'
           AND MOF_DIV_CODE = '430000000'
           AND UI_CODE = '202005'
           AND IS_DELETED = 2
           AND FISCAL_YEAR = '2022'
           AND VERSION = '2022ysbz'
           AND T1.IDEN_NO = T2.IDEN_NO
         GROUP BY IDEN_NO
        HAVING COUNT(*) > 1)
   AND ROWID NOT IN (SELECT MAX(ROWID)
                       FROM BAS_PERSON_INFO
                      WHERE AGENCY_CODE = '001888'
                        AND MOF_DIV_CODE = '430000000'
                        AND UI_CODE = '202005'
                        AND IS_DELETED = 2
                        AND FISCAL_YEAR = '2022'
                        AND VERSION = '2022ysbz'
                      GROUP BY IDEN_NO
                     HAVING COUNT(*) > 1);

---方式1：：--单个单位-----物理删除------------./end------------------




---方式2：：--整个区划-----物理删除------------./start------------------


-- 查询指定区划下重复的人员信息(即将被删除清理的数据)
-- 同步修改下面的区划(MOF_DIV_CODE)即可。
SELECT COUNT(1)
  FROM BAS_PERSON_INFO T1
 WHERE MOF_DIV_CODE = '430000000'
   AND IS_DELETED = 2
   AND EXISTS
 (SELECT 1
          FROM BAS_PERSON_INFO T2
         WHERE MOF_DIV_CODE = '430000000'
           AND IS_DELETED = 2
           and t1.agency_code = t2.agency_code
           and t1.ui_code = t2.ui_code
           AND T1.IDEN_NO = T2.IDEN_NO
           and t1.fiscal_year = t2.fiscal_year
           and t1.VERSION = t2.VERSION
         GROUP BY FISCAL_YEAR, VERSION, AGENCY_CODE, UI_CODE, IDEN_NO
        HAVING COUNT(*) > 1)
   AND ROWID NOT IN
       (SELECT MAX(ROWID)
          FROM BAS_PERSON_INFO t2
         WHERE MOF_DIV_CODE = '430000000'
           AND IS_DELETED = 2
           and t1.agency_code = t2.agency_code
           and t1.ui_code = t2.ui_code
           AND T1.IDEN_NO = T2.IDEN_NO
           and t1.fiscal_year = t2.fiscal_year
           and t1.VERSION = t2.VERSION
         GROUP BY FISCAL_YEAR, VERSION, AGENCY_CODE, UI_CODE, IDEN_NO
        HAVING COUNT(*) > 1);


-- 对指定区划下的人员信息数据进行去重，保留最后入库的人员数据.
-- 同步修改下面的区划(MOF_DIV_CODE)即可。
-- ext 表请先确定配置好了外键，且设置好了级联删除，删除 info 表的同时，数据库级联删除 ext 表数据
-- ext 表请先确定配置好了外键，且设置好了级联删除，删除 info 表的同时，数据库级联删除 ext 表数据
-- ext 表请先确定配置好了外键，且设置好了级联删除，删除 info 表的同时，数据库级联删除 ext 表数据
DELETE FROM BAS_PERSON_INFO T1
 WHERE MOF_DIV_CODE = '430000000'
   AND IS_DELETED = 2
   AND EXISTS
 (SELECT 1
          FROM BAS_PERSON_INFO T2
         WHERE MOF_DIV_CODE = '430000000'
           AND IS_DELETED = 2
           and t1.agency_code = t2.agency_code
           and t1.ui_code = t2.ui_code
           AND T1.IDEN_NO = T2.IDEN_NO
           and t1.fiscal_year = t2.fiscal_year
           and t1.VERSION = t2.VERSION
         GROUP BY FISCAL_YEAR, VERSION, AGENCY_CODE, UI_CODE, IDEN_NO
        HAVING COUNT(*) > 1)
   AND ROWID NOT IN
       (SELECT MAX(ROWID)
          FROM BAS_PERSON_INFO t2
         WHERE MOF_DIV_CODE = '430000000'
           AND IS_DELETED = 2
           and t1.agency_code = t2.agency_code
           and t1.ui_code = t2.ui_code
           AND T1.IDEN_NO = T2.IDEN_NO
           and t1.fiscal_year = t2.fiscal_year
           and t1.VERSION = t2.VERSION
         GROUP BY FISCAL_YEAR, VERSION, AGENCY_CODE, UI_CODE, IDEN_NO
        HAVING COUNT(*) > 1);


---方式2：：--整个区划-----物理删除------------./end------------------


---方式3：：--整个区划-----逻辑删除------------./start------------------


-- 查询指定区划下重复的人员信息(即将被删除清理的数据)
-- 同步修改下面的区划(MOF_DIV_CODE)即可。
SELECT count(1)
  FROM BAS_PERSON_INFO T1
 WHERE MOF_DIV_CODE = '430000000'
   AND IS_DELETED = 2
   AND EXISTS
 (SELECT 1
          FROM BAS_PERSON_INFO T2
         WHERE MOF_DIV_CODE = '430000000'
           AND IS_DELETED = 2
           and t1.agency_code = t2.agency_code
           and t1.ui_code = t2.ui_code
           AND T1.IDEN_NO = T2.IDEN_NO
           and t1.fiscal_year = t2.fiscal_year
           and t1.VERSION = t2.VERSION
         GROUP BY FISCAL_YEAR, VERSION, AGENCY_CODE, UI_CODE, IDEN_NO
        HAVING COUNT(*) > 1)
   AND ROWID NOT IN
       (SELECT MAX(ROWID)
          FROM BAS_PERSON_INFO t2
         WHERE MOF_DIV_CODE = '430000000'
           AND IS_DELETED = 2
           and t1.agency_code = t2.agency_code
           and t1.ui_code = t2.ui_code
           AND T1.IDEN_NO = T2.IDEN_NO
           and t1.fiscal_year = t2.fiscal_year
           and t1.VERSION = t2.VERSION
         GROUP BY FISCAL_YEAR, VERSION, AGENCY_CODE, UI_CODE, IDEN_NO
        HAVING COUNT(*) > 1);--648

SELECT count(1) FROM bas_person_ext ext where ext.per_id in(
SELECT per_id
  FROM BAS_PERSON_INFO T1
 WHERE MOF_DIV_CODE = '430000000'
   AND IS_DELETED = 2
   AND EXISTS
 (SELECT 1
          FROM BAS_PERSON_INFO T2
         WHERE MOF_DIV_CODE = '430000000'
           AND IS_DELETED = 2
           and t1.agency_code = t2.agency_code
           and t1.ui_code = t2.ui_code
           AND T1.IDEN_NO = T2.IDEN_NO
           and t1.fiscal_year = t2.fiscal_year
           and t1.VERSION = t2.VERSION
         GROUP BY FISCAL_YEAR, VERSION, AGENCY_CODE, UI_CODE, IDEN_NO
        HAVING COUNT(*) > 1)
   AND ROWID NOT IN
       (SELECT MAX(ROWID)
          FROM BAS_PERSON_INFO t2
         WHERE MOF_DIV_CODE = '430000000'
           AND IS_DELETED = 2
           and t1.agency_code = t2.agency_code
           and t1.ui_code = t2.ui_code
           AND T1.IDEN_NO = T2.IDEN_NO
           and t1.fiscal_year = t2.fiscal_year
           and t1.VERSION = t2.VERSION
         GROUP BY FISCAL_YEAR, VERSION, AGENCY_CODE, UI_CODE, IDEN_NO
        HAVING COUNT(*) > 1)
);--648




-- 对指定区划下的人员信息数据进行去重，保留最后入库的人员数据.
-- 同步修改下面的区划(MOF_DIV_CODE)即可。


update bas_person_ext ext set ext.IS_DELETED=1 where ext.per_id in(
SELECT per_id
  FROM BAS_PERSON_INFO T1
 WHERE MOF_DIV_CODE = '430000000'
   AND IS_DELETED = 2
   AND EXISTS
 (SELECT 1
          FROM BAS_PERSON_INFO T2
         WHERE MOF_DIV_CODE = '430000000'
           AND IS_DELETED = 2
           and t1.agency_code = t2.agency_code
           and t1.ui_code = t2.ui_code
           AND T1.IDEN_NO = T2.IDEN_NO
           and t1.fiscal_year = t2.fiscal_year
           and t1.VERSION = t2.VERSION
         GROUP BY FISCAL_YEAR, VERSION, AGENCY_CODE, UI_CODE, IDEN_NO
        HAVING COUNT(*) > 1)
   AND ROWID NOT IN
       (SELECT MAX(ROWID)
          FROM BAS_PERSON_INFO t2
         WHERE MOF_DIV_CODE = '430000000'
           AND IS_DELETED = 2
           and t1.agency_code = t2.agency_code
           and t1.ui_code = t2.ui_code
           AND T1.IDEN_NO = T2.IDEN_NO
           and t1.fiscal_year = t2.fiscal_year
           and t1.VERSION = t2.VERSION
         GROUP BY FISCAL_YEAR, VERSION, AGENCY_CODE, UI_CODE, IDEN_NO
        HAVING COUNT(*) > 1)
);--648



update BAS_PERSON_INFO T1
   set t1.is_deleted = 1
 WHERE MOF_DIV_CODE = '430000000'
   AND IS_DELETED = 2
   AND EXISTS
 (SELECT 1
          FROM BAS_PERSON_INFO T2
         WHERE MOF_DIV_CODE = '430000000'
           AND IS_DELETED = 2
           and t1.agency_code = t2.agency_code
           and t1.ui_code = t2.ui_code
           AND T1.IDEN_NO = T2.IDEN_NO
           and t1.fiscal_year = t2.fiscal_year
           and t1.VERSION = t2.VERSION
         GROUP BY FISCAL_YEAR, VERSION, AGENCY_CODE, UI_CODE, IDEN_NO
        HAVING COUNT(*) > 1)
   AND ROWID NOT IN
       (SELECT MAX(ROWID)
          FROM BAS_PERSON_INFO t2
         WHERE MOF_DIV_CODE = '430000000'
           AND IS_DELETED = 2
           and t1.agency_code = t2.agency_code
           and t1.ui_code = t2.ui_code
           AND T1.IDEN_NO = T2.IDEN_NO
           and t1.fiscal_year = t2.fiscal_year
           and t1.VERSION = t2.VERSION
         GROUP BY FISCAL_YEAR, VERSION, AGENCY_CODE, UI_CODE, IDEN_NO
        HAVING COUNT(*) > 1);



---方式3：：--整个区划-----逻辑删除------------./end------------------






--------甘肃完整去重 SQL 脚本----./start----

-- ext 表请先确定配置好了外键，且设置好了级联删除，删除 info 表的同时，数据库级联删除 ext 表数据
-- ext 表请先确定配置好了外键，且设置好了级联删除，删除 info 表的同时，数据库级联删除 ext 表数据
-- ext 表请先确定配置好了外键，且设置好了级联删除，删除 info 表的同时，数据库级联删除 ext 表数据

-- 查询指定区划、年度、版本下重复的人员信息(即将被删除清理的数据)
-- 同步修改下面的区划(MOF_DIV_CODE)、年度(FISCAL_YEAR)、版本(VERSION) 即可。
SELECT COUNT(1)
  FROM BAS_PERSON_INFO T1
 WHERE MOF_DIV_CODE = '620000000'
   AND IS_DELETED = 2
   AND FISCAL_YEAR = '2023'
   AND VERSION = '2023ysbz'
   AND EXISTS (SELECT 1
          FROM BAS_PERSON_INFO T2
         WHERE MOF_DIV_CODE = '620000000'
           AND IS_DELETED = 2
           AND FISCAL_YEAR = '2023'
           AND VERSION = '2023ysbz'
           and t1.agency_code = t2.agency_code
           and t1.ui_code = t2.ui_code
           AND T1.IDEN_NO = T2.IDEN_NO
         GROUP BY AGENCY_CODE, UI_CODE, IDEN_NO
        HAVING COUNT(*) > 1)
   AND ROWID NOT IN (SELECT MAX(ROWID)
                       FROM BAS_PERSON_INFO t2
                      WHERE MOF_DIV_CODE = '620000000'
                        and t1.agency_code = t2.agency_code
                        and t1.ui_code = t2.ui_code
                        AND T1.IDEN_NO = T2.IDEN_NO
                        AND IS_DELETED = 2
                        AND FISCAL_YEAR = '2023'
                        AND VERSION = '2023ysbz'
                      GROUP BY AGENCY_CODE, UI_CODE, IDEN_NO
                     HAVING COUNT(*) > 1);

-- 备份 ext 表即将被删除的重复数据
create table BAS_PERSON_ext_20221113 as
  SELECT ext.*
    FROM BAS_PERSON_ext ext
   where exists (SELECT 1
            FROM BAS_PERSON_INFO T1
           WHERE MOF_DIV_CODE = '620000000'
             AND IS_DELETED = 2
             AND FISCAL_YEAR = '2023'
             AND VERSION = '2023ysbz'
             AND EXISTS
           (SELECT 1
                    FROM BAS_PERSON_INFO T2
                   WHERE MOF_DIV_CODE = '620000000'
                     AND IS_DELETED = 2
                     AND FISCAL_YEAR = '2023'
                     AND VERSION = '2023ysbz'
                     and t1.agency_code = t2.agency_code
                     and t1.ui_code = t2.ui_code
                     AND T1.IDEN_NO = T2.IDEN_NO
                   GROUP BY AGENCY_CODE, UI_CODE, IDEN_NO
                  HAVING COUNT(*) > 1)
             AND ROWID NOT IN (SELECT MAX(ROWID)
                                 FROM BAS_PERSON_INFO t2
                                WHERE MOF_DIV_CODE = '620000000'
                                  and t1.agency_code = t2.agency_code
                                  and t1.ui_code = t2.ui_code
                                  AND T1.IDEN_NO = T2.IDEN_NO
                                  AND IS_DELETED = 2
                                  AND FISCAL_YEAR = '2023'
                                  AND VERSION = '2023ysbz'
                                GROUP BY AGENCY_CODE, UI_CODE, IDEN_NO
                               HAVING COUNT(*) > 1)
             and t1.per_id = ext.per_id);

-- 备份 info 表即将被删除的重复数据
create table BAS_PERSON_INFO_20221113 as
  SELECT T1.*
    FROM BAS_PERSON_INFO T1
   WHERE MOF_DIV_CODE = '620000000'
     AND IS_DELETED = 2
     AND FISCAL_YEAR = '2023'
     AND VERSION = '2023ysbz'
     AND EXISTS (SELECT 1
            FROM BAS_PERSON_INFO T2
           WHERE MOF_DIV_CODE = '620000000'
             AND IS_DELETED = 2
             AND FISCAL_YEAR = '2023'
             AND VERSION = '2023ysbz'
             and t1.agency_code = t2.agency_code
             and t1.ui_code = t2.ui_code
             AND T1.IDEN_NO = T2.IDEN_NO
           GROUP BY AGENCY_CODE, UI_CODE, IDEN_NO
          HAVING COUNT(*) > 1)
     AND ROWID NOT IN (SELECT MAX(ROWID)
                         FROM BAS_PERSON_INFO t2
                        WHERE MOF_DIV_CODE = '620000000'
                          and t1.agency_code = t2.agency_code
                          and t1.ui_code = t2.ui_code
                          AND T1.IDEN_NO = T2.IDEN_NO
                          AND IS_DELETED = 2
                          AND FISCAL_YEAR = '2023'
                          AND VERSION = '2023ysbz'
                        GROUP BY AGENCY_CODE, UI_CODE, IDEN_NO
                       HAVING COUNT(*) > 1);

-- 对指定区划、年度、版本下的人员信息数据进行去重，保留最后入库的人员数据.
-- 同步修改下面的区划(MOF_DIV_CODE)、年度(FISCAL_YEAR)、版本(VERSION) 即可。
-- ext 表请先确定配置好了外键，且设置好了级联删除，删除 info 表的同时，数据库级联删除 ext 表数据
-- ext 表请先确定配置好了外键，且设置好了级联删除，删除 info 表的同时，数据库级联删除 ext 表数据
-- ext 表请先确定配置好了外键，且设置好了级联删除，删除 info 表的同时，数据库级联删除 ext 表数据
DELETE FROM BAS_PERSON_INFO T1
 WHERE MOF_DIV_CODE = '620000000'
   AND IS_DELETED = 2
   AND FISCAL_YEAR = '2023'
   AND VERSION = '2023ysbz'
   AND EXISTS (SELECT 1
          FROM BAS_PERSON_INFO T2
         WHERE MOF_DIV_CODE = '620000000'
           AND IS_DELETED = 2
           AND FISCAL_YEAR = '2023'
           AND VERSION = '2023ysbz'
           and t1.agency_code = t2.agency_code
           and t1.ui_code = t2.ui_code
           AND T1.IDEN_NO = T2.IDEN_NO
         GROUP BY AGENCY_CODE, UI_CODE, IDEN_NO
        HAVING COUNT(*) > 1)
   AND ROWID NOT IN (SELECT MAX(ROWID)
                       FROM BAS_PERSON_INFO t2
                      WHERE MOF_DIV_CODE = '620000000'
                        and t1.agency_code = t2.agency_code
                        and t1.ui_code = t2.ui_code
                        AND T1.IDEN_NO = T2.IDEN_NO
                        AND IS_DELETED = 2
                        AND FISCAL_YEAR = '2023'
                        AND VERSION = '2023ysbz'
                      GROUP BY AGENCY_CODE, UI_CODE, IDEN_NO
                     HAVING COUNT(*) > 1);

--------甘肃完整去重 SQL 脚本----./end----