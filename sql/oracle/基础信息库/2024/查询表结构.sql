-- 查询目标表列信息，包括数据库类型，长度，是否必填，注释
-- 这在编写文档时很方便。
SELECT t.TABLE_NAME,
       t.COLUMN_NAME,
       (t.DATA_TYPE || CASE
                           WHEN data_type = 'NUMBER' THEN
                               CASE
                                   WHEN data_precision IS NOT NULL THEN --数字指定了长度时
                                       CASE
                                           WHEN data_scale > 0 THEN --数字指定了长度，且指定了精度时
                                               '(' || data_precision || ',' || data_scale || ')'
                                           ELSE -- 数字指定了长度，没有指定精度时
                                               '(' || to_char(data_precision) || ')'
                                           END
                                   ELSE --数字没有指定长度时，默认是22，此时不管
                                       ''
                                   END
                           WHEN data_type IN ('CHAR', 'VARCHAR2', 'NVARCHAR2', 'CLOB') AND
                                t.DATA_LENGTH IS NOT NULL THEN
                               '(' || t.DATA_LENGTH || ')'
                           ELSE
                               ''
           END) AS 字段类型与长度,
       (CASE
            WHEN t.NULLABLE = 'Y' THEN
                '否'
            ELSE
                '是'
           END)    是否必填,
       t2.COMMENTS
FROM user_tab_columns t
         JOIN user_col_comments t2
              ON t.TABLE_NAME = t2.TABLE_NAME
                  AND t.COLUMN_NAME = t2.COLUMN_NAME
WHERE t.TABLE_NAME = 'BAS_AGENCY_EXT_STU'
ORDER BY t.COLUMN_NAME;
