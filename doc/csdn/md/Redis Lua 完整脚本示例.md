# 原子计数

```
local key = KEYS[1] -- 获取键名
local increment = tonumber(ARGV[1])  -- 获取参数值，并转为数字
local current_value = redis.call('GET', key) -- 获取当前值
if current_value then
-- 如果key存在，则转为数字后相加
    current_value = tonumber(current_value)
    redis.call('SET', key, current_value + increment)
    return current_value + increment
else
-- 如果key不存在，则创建并赋值
    redis.call('SET', key, increment)
    return increment
end

-- 执行脚本示例
-- D:\software\Redis-x64-5.0.14.1>redis-cli.exe --eval ./lua/原子计数.lua a1 , 1
-- (integer) 1
-- D:\software\Redis-x64-5.0.14.1>redis-cli.exe --eval ./lua/原子计数.lua a1 , 1
-- (integer) 2
-- D:\software\Redis-x64-5.0.14.1>redis-cli.exe --eval ./lua/原子计数.lua a1 , 1
```

# 模拟转账

```
-- Lua脚本在Redis中执行时，所有的操作都是在一个事务中完成的，因此可以模拟Redis的事务。
-- 假设想要实现一个原子性的转账操作，可以使用Lua脚本来实现

local from_user = ARGV[1]
local to_user = ARGV[2]
local transfer_amount = tonumber(ARGV[3])

-- 两个点表示内容拼接，相当于 Plsql中的 || 
local from_balance = redis.call('GET', 'user:'..from_user..':balance')
local to_balance = redis.call('GET', 'user:'..to_user..':balance')

if from_balance and to_balance then
    from_balance = tonumber(from_balance)
    to_balance = tonumber(to_balance)
    if transfer_amount >0 and from_balance >= transfer_amount then
        redis.call('DECRBY', 'user:'..from_user..':balance', transfer_amount)
        redis.call('INCRBY', 'user:'..to_user..':balance', transfer_amount)
        return 'OK'
    else
        return 'Insufficient balance'
    end
else
    return 'User not found'
end

-- 执行脚本示例
-- D:\software\Redis-x64-5.0.14.1>redis-cli.exe set user:a01:balance 10000
-- OK
-- D:\software\Redis-x64-5.0.14.1>redis-cli.exe set user:b01:balance 100
-- OK
-- D:\software\Redis-x64-5.0.14.1>redis-cli.exe --eval ./lua/转账事务.lua , a01 b01 100
-- "OK"
-- D:\software\Redis-x64-5.0.14.1>redis-cli.exe get user:a01:balance
-- "9900"

-- D:\software\Redis-x64-5.0.14.1>redis-cli.exe get user:b01:balance
-- "200"
```

# 分布式锁

```
local lockKey = KEYS[1] -- 获取锁的键名
local lockValue = ARGV[1] -- 获取锁的值
local lockTimeout = ARGV[2] -- 获取锁的超时时间
if redis.call('SET', lockKey, lockValue, 'NX', 'PX', lockTimeout) then
    -- 锁获取成功，执行关键操作
    -- ...
    redis.call('DEL', lockKey) -- 释放锁
    return 1
else
    return 0 -- 无法获取锁
end
```
