

# 腾讯云 AI 代码助手-Tencent Cloud AI Code Assistant

## 介绍 与 安装

1、腾讯云 AI 代码助手是由腾讯云自研的一款开发编程提效辅助工具，基于混元代码大模型，提供技术对话、代码补全、代码诊断和优化、智能生成代码注释等能力。通过腾讯云 AI 代码助手，开发者可以更高效地解决实际编程问题，提高编程效率和代码质量。

2、[2.21.8]版本开始支持 tencent 部署的 deepseek 满血版。

3、**插件官网地址**：https://plugins.jetbrains.com/plugin/24379-tencent-cloud-ai-code-assistant。

4、安装：File–>Settings->Plugins，切换到Marketplace选项卡下，输入“Tencent Cloud AI Code Assistant“进行搜索，找到后，Install安装即可。

![](images/20250224090645.png)

5、安装后需要先登录，微信登录即可。
![](images/20250224092430.png)

6、接着可以对话，还可以选择大模型，比如 tencent 部署的 deepseek 满血版。

![](images/20250224092733.png)

### 1. 代码补全

智能补全代码信息，高效完成编程工作

- 支持 20 多种语法深度学习，支持注释描述的补全，变量补全，函数补全等；
- 支持单行注释生成代码、函数体内注释生成代码、多行注释生成代码；
- 支持纯函数声明后实现函数、注释与函数声明后实现函数；
- 支持函数间补全完整的新函数实现以及部分实现（生成函数签名或注释、在分支逻辑中进行补全）；

![2-1代码补全.gif](https://cs-res-1258344699.file.myqcloud.com/ai/20240508_SaaS/market_overview/IDEA_CodeCompletion.gif)

### 2. 技术对话

人工智能技术对话，复杂问题轻松解决

- 技术对话集成 IDE，支持将对话代码内容一键插入编译区当中；

![1-1与AI进行技术沟通.gif](https://cs-res-1258344699.file.myqcloud.com/ai/20240508_SaaS/market_overview/IDEA_Chat.gif)

### 3. 规范/修复错误代码

精准修复错误代码，减少漏洞不出差错

- 支持框选或全选编译区代码内容，进行代码规范检查与错误修复；

![1-3修复代码.gif](https://cs-res-1258344699.file.myqcloud.com/ai/20240508_SaaS/market_overview/IDEA_fix.gif)

### 4. 智能生成代码注释

清晰解释既有代码，快速接手历史项目

- 根据代码具体功能与目的，快速生成高可读注释信息；
- 快速识别全量代码内容，自动生成解释目录，降低理解成本；

![1-2生成代码注释.gif](https://cs-res-1258344699.file.myqcloud.com/ai/20240508_SaaS/market_overview/IDEA_Comments.gif)

## 快捷键

| 功能                 | macOS        | Windows        |
| -------------------- | ------------ | -------------- |
| 触发代码补全         | Enter        | Enter          |
| 采纳所有推荐代码     | Tab          | Tab            |
| 按行采纳推荐代码     | Ctrl + ⌘ + → | Ctrl + alt + → |
| 按词采纳推荐代码     | ⌘ + →        | Ctrl + →       |
| 切换至下一个推荐结果 | ⌥ + ]        | Alt + ]        |
| 切换至上一个推荐结果 | ⌥ + [        | Alt + [        |
| 手动触发推荐         | ⌥ + \        | Alt + P        |
| 撤销当前推荐状态     | Esc          | Esc            |

## 快捷指令问答

在对话输入框中，输入 / 或 @ 调用预置的快捷指令：

- /comments：为所选的代码添加文档注释；
- /explain：解释所选代码的工作原理；
- /fix：针对所选代码中的问题提出修复方案；
- /tests：为所选代码生成单元测试；
- /clear：清空当前会话；
- @workspace：询问您的工作空间，将自动引用当前代码；

## 快捷知识库选择

在对话输入框中，输入 # 调用预置的知识库。 知识库涵盖主流的代码仓库集合：

- TDesign: TDesign 组件 API 知识库
- Spring Boot: Spring 开发框架
- Spring AI: 基于 Spring 的 AI 应用框架
- Langchain: 基于Langchain的AI应用工具包
- React: React前端开发框架
- Vue: Vue.js前端开发框架