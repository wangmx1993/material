create or replace view vw_gbm_bs_to_salary as
select bs.AGENCY_ID,
       bs.AGENCY_ID as hold50,
       bs.CREATE_USER_NAME,
       to_char(bs.CREATE_TIME,'yyyy/mm/dd hh24:mi:ss') as create_datetime,
       bs.CREATE_USER_ID  as CREATE_USER, --bs.CREATE_USER,
       to_char(bs.UPDATE_TIME,'yyyy/mm/dd hh24:mi:ss') as last_op_datetime,
       to_char(add_months(bs.UPDATE_TIME,1),'yyyy-mm') as MONTH_STR,
       bs.UPDATE_USER_ID as LAST_OP_USER,
       bs.UPDATE_USER_NAME as last_op_user_name,
       --bs.LAST_VER,
       bs.MOF_DIV_CODE as admdivcode,
       bs.MOF_DIV_CODE as top_org_id,
       bs.IDEN_NO as ic_id,
       bs.PER_NAME as name,
       (case when bs.sex_code = '1' then '男' when bs.sex_code = '2' then '女' end) as sex_name,
       bs.SERV_LEN as work_age,
       to_char(bs.WORK_INIT_DATE,'yyyy-mm-dd') as work_date,
       case when bs.change_type_code = '02' then '1' when bs.change_type_code = '05' then '0' else '' end as istrtc, --bs.M_F50 as istrtc,
      --bs.Csrq as birth_date,
      --bs.C29 as bdep_id,
       ep.ele_name as hold15,        --人员身份名称
       sc.ele_name as schrec_name, --学历
       ryzt.ele_name as hold13,      --人员状态名称
       mz.ele_name as nat_name,                --民族
       (case when ext.sala_agency_code is null then ag.ele_name else salag.ele_name end ) as sal_agency_id,--工资发放单位
       zw.ele_name as pos_name,                     --职务
       case  when bs.change_type_code = '01' then '1' 
             when bs.change_type_code = '04' then '9'
             else '7' end as change_type_id,      ---1新增 7修改 9删除
       case when bs.ui_code = '202005' then  1
            when bs.ui_code = '202006' then  2
            when bs.ui_code = '202007' then  3
            when bs.ui_code = '202008' then  4
            when bs.ui_code = '202009' then  5 end as saltype_id,
       case when bs.ui_code = '202005' then  1
            when bs.ui_code = '202006' then  2
            when bs.ui_code = '202007' then  3
            when bs.ui_code = '202008' then  4
            when bs.ui_code = '202009' then  5 end as pertype_id
 from BAS_AUDIT_BILL gb,BAS_PERSON_INFO bs
      inner join BAS_PERSON_EXT ext on bs.per_id  = ext.per_id
      -- and bs.is_deleted = ext.is_deleted and bs.is_enabled = ext.is_enabled
       --要素关联
      left join ele_union ryzt on bs.per_sta_code = ryzt.ele_code and ryzt.ele_catalog_code = 'PerSta'  --人员状态 离退休在职其他
      left join ELE_SchRec sc on bs.SCH_REC_CODE = sc.ele_code --学历
      left join ele_union  mz on bs.nat_code = mz.ele_code and mz.ele_catalog_code = 'Nat'    --民族
      left join ELE_AGENCY salag on ext.sala_agency_code = salag.ele_code  and salag.tenant_id = bs.mof_div_code  and salag.is_leaf = 1 and salag.is_deleted = 2 --工资发放单位
      left join ELE_AGENCY ag on ext.sala_agency_id = ag.ele_id  and ag.tenant_id = bs.mof_div_code  and ag.is_leaf = 1 and ag.is_deleted = 2 --工资发放单位
      left join ele_union  zw on bs.pos_code = zw.ele_code and zw.ele_catalog_code = 'Pos'     --职务
      left join ELE_PerIde ep on  bs.PER_IDE_CODE =ep.ele_code  and ep.is_leaf = 1             --人员身份
where  bs.audit_id = gb.audit_id
   --and bs.audit_id = ext.audit_id
   --终审条件
     and gb.is_end = 1
  -- and gb.audit_type = 3
     and gb.ui_group='BAS_PERSON'
;
