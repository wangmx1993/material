
declare
  v_sql           varchar2(32767);--自动执行的 DDL 语句
  v_tab_is_exists number := 0;-- 表是否存在
begin
  SELECT count(1)
    into v_tab_is_exists
    FROM user_tables t
   where t.TABLE_NAME = upper('SCHEMA_VERSION');
  if v_tab_is_exists > 0 then
    dbms_output.put_line('表已经存在，不再自动新建.');
  else
    v_sql := 'create table SCHEMA_VERSION (
            id          VARCHAR2(38) not null,
            version     VARCHAR2(200),
            description VARCHAR2(1000),
            type        VARCHAR2(10),
            script      VARCHAR2(200) not null,
            server_name VARCHAR2(200),
            update_by   VARCHAR2(200),
            update_on   DATE,
            create_time DATE not null)';
    execute immediate (v_sql);
    dbms_output.put_line('新建表完成.');
    v_sql := 'alter table SCHEMA_VERSION add primary key (ID)';
    execute immediate (v_sql);
    dbms_output.put_line('创建主键完成.');
  end if;
end;
/

-- Add comments to the table
comment on table SCHEMA_VERSION  is '脚本记录表';
-- Add comments to the columns
comment on column SCHEMA_VERSION.id  is '主键';
comment on column SCHEMA_VERSION.version  is '版本信息';
comment on column SCHEMA_VERSION.description  is '描述信息';
comment on column SCHEMA_VERSION.type  is '类型（sql）';
comment on column SCHEMA_VERSION.script  is '脚本文件名称';
comment on column SCHEMA_VERSION.server_name  is '所属服务（basic、pm、budget、config）';
comment on column SCHEMA_VERSION.update_by  is '更新人';
comment on column SCHEMA_VERSION.update_on  is '更新时间';
comment on column SCHEMA_VERSION.create_time  is '执行时间';
-- Create/Recreate primary, unique and foreign key constraints
