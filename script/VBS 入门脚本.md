

# VBScript 运算符 #

	dim a,b,c
	a=3
	b=5
	c=2
	msgbox a+b-c,,"3+5-2"      '等于6
	msgbox a*b/c,,"3*5/2"	   '等于7.5
	msgbox b mod c,,"5 mod 2"  '等于1

***
	dim a
	a=inputbox("emter The power of 2")
	msgbox 2 ^ a,, "2 ^ " & a  '& 号连接符


# 创建目录 #

	Dim fso, MyFile
	Set fso = CreateObject("Scripting.FileSystemObject") 
	fso.CreateFolder("E:\abc")  '创建父目录，存在时不影响
	fso.CreateFolder("E:\abc\doc") '创建子目录，存在时不影响

# 判断目录是否存在 #	
	
	Dim fso
	Set fso = CreateObject("Scripting.FileSystemObject") 
	fileExi1 = fso.FolderExists("e:/abc")  '判断目录是否存在，可以说绝对路径或者相对路径
	Msgbox fileExi1

# 创建文件 #

	Dim fso, MyFile
	Set fso = CreateObject("Scripting.FileSystemObject") 
	Set MyFile = fso.CreateTextFile("E:\abc\1.txt", True) '在E:\abc目录下创建文件1.txt，abc目录不存在时，不会创建
	
# 判断文件是否存在 #
	
	Dim fso
	Set fso = CreateObject("Scripting.FileSystemObject") 
	fileExi = fso.FileExists("E:\abc\1.txt") '判断文件是否存在，可以是相对路径，如 ../1.txt
	Msgbox fileExi
	
# 写入文件内容 #

	Dim fso, MyFile
	Set fso = CreateObject("Scripting.FileSystemObject") 
	Set MyFile = fso.CreateTextFile("E:\abc\testfile.txt", True) '文件不存在时自动新建，目录不存在时不处理。覆盖内容
	MyFile.WriteLine("新建文件，并写入内容！新内容会覆盖旧内容")
	MyFile.WriteLine("你好，深圳！")
	MyFile.Close

***
	Dim fso, myFile
	Set fso = CreateObject("Scripting.FileSystemObject") 
	Set myFile = fso.OpenTextFile("E:\abc\3.txt",8,true) '文件不存在时自动新建，目录不存在时不处理。追加内容
	myFile.WriteLine("傻瓜ma")

# 获取日期、时间 #

	Dim MyDate
	MyDate = Date    ' MyDate 包含当前系统日期，如 2021/1/8
	Msgbox(MyDate)

***
	Dim myMonth
	myMonth = Month(Now)	'从日期中获取月份
	Msgbox(myMonth)
	
***
	Dim myNow
	myNow = Now    'myNow包含当前系统日期与时间
	Msgbox(myNow)
	
	
***
	Dim myDay
	myDay = Day(Now)	'从日期中获取一月中的天数，如 8
	Msgbox(myDay)

***	
	Dim myTime
	myTime = Time    '获取当前系统时间
	Msgbox(myTime)
	
***	
	Dim myWeekDay
	myWeekDay = WeekDay(Now)	'从日期中获取星期中的天数，0表示星期天
	Msgbox(myWeekDay)
	
***	
	Dim myYear
	myYear = Year(Date)	'从日期中获取年份
	Msgbox(myYear)


# 判断系统盘符是否存在 #

	Dim fso
	Set fso = CreateObject("Scripting.FileSystemObject") 
	Msgbox fso.DriveExists("D:") '判断盘符 D 是否存在

# IF-ELSE 条件语句 #

	Dim result
	If 3 > 2 Then result = "3>2" Else result = "3<2"	'后面的Else如果不需要时，可以省略
	Msgbox result
***

	Msgbox("Start")
	If 3 > 2 Then 
		result = "3>2" 
	Else 
		result = "3<2"
	End If
	Msgbox(result)
***
	Msgbox("Start")
	If 3 > 5 Then 
		result = "3>5" 
		flag = True
	ElseIf 3 > 4 Then
		result = "3>4" 
		flag = True
	ElseIf 3 > 2 Then
		If 2 >1 Then
			result = "2>1"
		Else
			result = "2<1"
		End If
	End If
	Msgbox(result)

# While 循环 #

	Dim Counter
	Counter = 0                '初始化变量。
	While Counter < 3          ' 计数器的值。
	   Counter = Counter + 1   ' 增加计数器。
	   Msgbox (Counter)	 
	   WScript.sleep 1000 '休眠1秒
	Wend  				
	Msgbox ("Game Over")
	
	
# 延时执行 #

	Msgbox ("Hi")
	WScript.sleep 5000	'延时5秒后再执行
	Msgbox ("Hi 2")



# 结束所有 VBScript 进程 #

	taskkill /F /IM wscript.exe

# 运行计算器程序 #

	CreateObject("WScript.Shell").Run("calc")


# 运行 potplay 并播放指定视频文件 #

	Dim shell
	set shell = CreateObject("WScript.Shell") 
	shell.Run("C:\wmx\software\PotPlayer\PotPlayerMini.exe F:\我的个神啊.mp4")  '如果路径有空格，则使用 """C:\wmx\soft ware\1.mp4""" 包裹

# 运行 cmd 命令 ping ip 地址 #

	Dim rsync
	Set rsync = CreateObject("WScript.Shell")
	rsync.Run("%comspec% /c ping 114.114.114.114")

# 运行 cmd 命令查看本机 IP 地址 #

	Dim rsync
	Set rsync = CreateObject("WScript.Shell")
	rsync.Run("%comspec% /k ipconfig /all")

# 定时喝水提示 #

	Dim count,msg,spaceTime
	count = 1
	spaceTime = 30	'间隔多少分钟提醒一次
	While 0 < 1
	Msgbox ("今天第 " & count & " 次喝水，" & "过 " & spaceTime & " 分钟会再次提示！")
	WScript.sleep spaceTime * 60 * 1000		
	count = count+1
	Wend

# 减小系统音量 #

	'系统音量是[0,100]，减音量一次，系统音量下降2
	'比如连续增/减音量25次，如果当前系统音量是50的话，则刚好增/减到100/0
	
	Dim count
	count =2   '音量减低的次数，1次就是 2%，2次就是4%，依此类推
	for i=1 to count
	CreateObject("Wscript.Shell").Sendkeys "爱" ' 减小音量
	next

# 增大系统音量 #

	Dim count
	count =2   '音量调高的次数，1次就是 2%，2次就是4%，依此类推
	for i=1 to count
	CreateObject("Wscript.Shell").Sendkeys "怜" ' 增大音量
	next

# 系统静音与取消静音 #

	Set Ws = CreateObject("Wscript.Shell")
	Ws.Sendkeys "…" ' 静音与取消静音
	



