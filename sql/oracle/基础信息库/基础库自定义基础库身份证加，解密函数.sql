
-- 自定义身份证解密函数
create or replace function jm_iden_no(p_iden_type_code in bas_person_info.iden_type_code%type, --证件类型
                                      p_iden_no        in bas_person_info.iden_no%type) --证件号
  return bas_person_info.iden_no%type is
  v_iden_no bas_person_info.iden_no%type;
begin
  -- 自定义身份证解密函数
  -- 输入参数：证件类型，证件号
  -- 证件类型是 01-身份证时，进行解密，否则原样返回证件号
  if p_iden_type_code = '01' then
    select replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(p_iden_no,
                                                                                                   'D',
                                                                                                   '0'),
                                                                                           'E',
                                                                                           '1'),
                                                                                   'F',
                                                                                   '2'),
                                                                           'G',
                                                                           '3'),
                                                                   '@',
                                                                   '4'),
                                                           'A',
                                                           '5'),
                                                   'B',
                                                   '6'),
                                           'C',
                                           '7'),
                                   'L',
                                   '8'),
                           'M',
                           '9'),
                   ',',
                   'X')
      into v_iden_no
      from dual;
  else
    v_iden_no := p_iden_no;
  end if;
  return v_iden_no;
end;


-- 自定义身份证加密函数

create or replace function fjm_iden_no(p_iden_type_code in bas_person_info.iden_type_code%type, --证件类型
                                       p_iden_no        in bas_person_info.iden_no%type) --证件号
 return bas_person_info.iden_no%type is
  v_iden_no bas_person_info.iden_no%type;
begin
  -- 自定义身份证加密函数
  -- 输入参数：证件类型，证件号
  -- 证件类型是 01-身份证时，进行加密，返回加密后的值；否则原样返回证件号.
  if p_iden_type_code = '01' then
    select replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(p_iden_no,
                                                                                                   '0',
                                                                                                   'D'),
                                                                                           '1',
                                                                                           'E'),
                                                                                   '2',
                                                                                   'F'),
                                                                           '3',
                                                                           'G'),
                                                                   '4',
                                                                   '@'),
                                                           '5',
                                                           'A'),
                                                   '6',
                                                   'B'),
                                           '7',
                                           'C'),
                                   '8',
                                   'L'),
                           '9',
                           'M'),
                   'X',
                   ',')
      into v_iden_no
      from dual;
  else
    v_iden_no := p_iden_no;
  end if;
  return v_iden_no;
end;
