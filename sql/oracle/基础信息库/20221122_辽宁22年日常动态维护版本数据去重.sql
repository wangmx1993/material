
--当人员信息重复时
--优先将 audit_id 不等于0的删掉
--其次是 change_type_name 是信息删除的删掉
--最后删除创建时间更早的一条

declare
  cursor vrows is
  --查询 2022 年 日常动态维护 版本重复的数据
    SELECT VERSION,
           MOF_DIV_CODE,
           FISCAL_YEAR,
           AGENCY_CODE,
           UI_CODE,
           IDEN_NO,
           count(1)
      FROM BAS_PERSON_INFO T2
     WHERE FISCAL_YEAR = '2022'
       AND VERSION = '2022rcdtwh'
       AND IS_DELETED = 2
     GROUP BY VERSION,
              MOF_DIV_CODE,
              FISCAL_YEAR,
              AGENCY_CODE,
              UI_CODE,
              IDEN_NO
    HAVING COUNT(*) > 1;

  v_count                number := 0;
  v_pre_audit_id         BAS_PERSON_INFO.audit_id%type; -- 对比时记录上一条数据的 audit_id 值
  v_pre_change_type_code BAS_PERSON_INFO.change_type_code%type; -- 对比时记录上一条数据的 change_type_code 值
  v_pre_create_time      BAS_PERSON_INFO.create_time%type; -- 对比时记录上一条数据的 create_time 值
  v_pre_per_id           BAS_PERSON_INFO.per_id%type; -- 对比时记录上一条数据的 per_id 值

begin
  for vrow in vrows loop
    --每次处理重复人员前，先置为 null.
    v_pre_audit_id         := null;
    v_pre_change_type_code := null;
    v_pre_create_time      := null;
    v_pre_per_id           := null;

    v_count := v_count + 1;
    dbms_output.put_line(v_count || ' ' || vrow.MOF_DIV_CODE || ' ' ||
                         vrow.AGENCY_CODE || ' ' || vrow.UI_CODE || ' ' ||
                         vrow.IDEN_NO || ' 重复.');

    -- 逐个查询重复的人员信息，然后对重复的人员进行处理。
    -- 思路是：逐个对比前后两条数据，每次都删除其中一条，比如重复了 3 条，先比较1，2条，假如结果是保留第2条，然后比较 2，3条，再删除其中优先级低的，依此类推。
    for person in (select MOF_DIV_CODE,
                          AGENCY_CODE,
                          UI_CODE,
                          IDEN_NO,
                          t.per_id,
                          t.audit_id,
                          t.change_type_code,
                          t.create_time
                     from BAS_PERSON_INFO t
                    where t.mof_div_code = vrow.MOF_DIV_CODE
                      and t.agency_code = vrow.AGENCY_CODE
                      and t.ui_code = vrow.UI_CODE
                      and t.iden_no = vrow.IDEN_NO
                      and t.is_deleted = 2
                      and t.fiscal_year = vrow.fiscal_year
                      and t.version = vrow.version) loop

      --不为 null 表示前一条已经记录了
      if v_pre_per_id is not null then
        --优先将 audit_id 不等于0的删掉
        if v_pre_audit_id != '0' and person.audit_id = '0' then
          UPDATE BAS_PERSON_EXT T
             SET t.is_deleted = 1
           WHERE per_id = v_pre_per_id;
          UPDATE BAS_PERSON_INFO T
             SET t.is_deleted = 1
           WHERE per_id = v_pre_per_id;
          dbms_output.put_line('  per_id ' || v_pre_per_id || ' 对比 ' ||
                               person.per_id || ' 保留 ' || person.per_id ||
                               ' 删除 ' || v_pre_per_id ||
                               ' 优先将 audit_id 不等于0的删掉');

        elsif v_pre_audit_id = '0' and person.audit_id != '0' then
          UPDATE BAS_PERSON_EXT T
             SET t.is_deleted = 1
           WHERE per_id = person.per_id;
          UPDATE BAS_PERSON_INFO T
             SET t.is_deleted = 1
           WHERE per_id = person.per_id;
          dbms_output.put_line('  per_id ' || v_pre_per_id || ' 对比 ' ||
                               person.per_id || ' 保留 ' || v_pre_per_id ||
                               ' 删除 ' || person.per_id ||
                               ' 优先将 audit_id 不等于0的删掉');

          --重新赋值当前保留下来的数据，与后一条继续进行对比
          v_pre_audit_id         := person.audit_id;
          v_pre_change_type_code := person.change_type_code;
          v_pre_create_time      := person.create_time;
          v_pre_per_id           := person.per_id;

          --其次是 change_type_name 是信息删除的
        elsif v_pre_change_type_code = '04' and
              person.change_type_code != '04' then
          UPDATE BAS_PERSON_EXT T
             SET t.is_deleted = 1
           WHERE per_id = v_pre_per_id;
          UPDATE BAS_PERSON_INFO T
             SET t.is_deleted = 1
           WHERE per_id = v_pre_per_id;
          dbms_output.put_line('  per_id ' || v_pre_per_id || ' 对比 ' ||
                               person.per_id || ' 保留 ' || person.per_id ||
                               ' 删除 ' || v_pre_per_id ||
                               ' 其次是 change_type_name 是信息删除的');

        elsif v_pre_change_type_code != '04' and
              person.change_type_code = '04' then
          UPDATE BAS_PERSON_EXT T
             SET t.is_deleted = 1
           WHERE per_id = person.per_id;
          UPDATE BAS_PERSON_INFO T
             SET t.is_deleted = 1
           WHERE per_id = person.per_id;
          dbms_output.put_line('  per_id ' || v_pre_per_id || ' 对比 ' ||
                               person.per_id || ' 保留 ' || v_pre_per_id ||
                               ' 删除 ' || person.per_id ||
                               ' 其次是 change_type_name 是信息删除的');

          --重新赋值当前保留下来的数据，与后一条继续进行对比
          v_pre_audit_id         := person.audit_id;
          v_pre_change_type_code := person.change_type_code;
          v_pre_create_time      := person.create_time;
          v_pre_per_id           := person.per_id;

          --最后删除创建时间更早的一条
        elsif v_pre_create_time < person.create_time then
          UPDATE BAS_PERSON_EXT T
             SET t.is_deleted = 1
           WHERE per_id = v_pre_per_id;
          UPDATE BAS_PERSON_INFO T
             SET t.is_deleted = 1
           WHERE per_id = v_pre_per_id;
          dbms_output.put_line('  per_id ' || v_pre_per_id || ' 对比 ' ||
                               person.per_id || ' 保留 ' || person.per_id ||
                               ' 删除 ' || v_pre_per_id || ' 最后删除创建时间更早的一条');

        elsif v_pre_create_time > person.create_time then
          UPDATE BAS_PERSON_EXT T
             SET t.is_deleted = 1
           WHERE per_id = person.per_id;
          UPDATE BAS_PERSON_INFO T
             SET t.is_deleted = 1
           WHERE per_id = person.per_id;
          dbms_output.put_line('  per_id ' || v_pre_per_id || ' 对比 ' ||
                               person.per_id || ' 保留 ' || v_pre_per_id ||
                               ' 删除 ' || person.per_id || ' 最后删除创建时间更早的一条');
          --重新赋值当前保留下来的数据，与后一条继续进行对比
          v_pre_audit_id         := person.audit_id;
          v_pre_change_type_code := person.change_type_code;
          v_pre_create_time      := person.create_time;
          v_pre_per_id           := person.per_id;

          --如果前后两条数据 audi_id、change_type_code、create_time 都相等，则默认删除上一条
        else
          UPDATE BAS_PERSON_EXT T
             SET t.is_deleted = 1
           WHERE per_id = v_pre_per_id;
          UPDATE BAS_PERSON_INFO T
             SET t.is_deleted = 1
           WHERE per_id = v_pre_per_id;
          dbms_output.put_line('  per_id ' || v_pre_per_id || ' 对比 ' ||
                               person.per_id || ' 保留 ' || person.per_id ||
                               ' 删除 ' || v_pre_per_id ||
                               ' 前后两条数据 audi_id、change_type_code、create_time 都相等，则默认删除上一条');
        end if;
      else
        --第一次循环赋值
        v_pre_audit_id         := person.audit_id;
        v_pre_change_type_code := person.change_type_code;
        v_pre_create_time      := person.create_time;
        v_pre_per_id           := person.per_id;
      end if;

    end loop;
  end loop;
  --  commit; --4、提交事务
end;
