
--===================往指定的根视图组下面插入一个[附件]视图配置===================
-- 某个菜单打开的附件二级页面空白时，就说明可能是缺少视图配置，此时补充上就行。
-- 视图ID：随意定义(数字+字母)，不冲突就行，全局查询并替换。
-- 视图组ID：页面F12查找，将其挂在根视图组下，全局查询并替换。

-- 视图
insert into gap_ui_view (CODE, NAME, TITLE, TENANT_ID, USER_ID, VIEW_TYPE, QUERY_TABLE_CODE, GET_ATTRIBUTE, SAVE_TABLE_CODE, SET_ATTRIBUTE, IS_VISIBLE, IS_CHECKBOX, IS_SHOW_SUM, IS_SHOW_STRIPE, IS_SHOW_UNIT, UNIT, IS_PAGED, COL_NUMBER, UPDATE_TIME, DISP_ORDER, IS_DEFAULT_SELECT, REP_CONTROL_NAME, SPECIALFIELD, REMARK, MENU_ID, URL, IS_SQL_FILTER, SQL, UIVIEW_CATE_ID
, ID, GROUP_ID)
values ('affix', '附件表', '附件表', '0', '0', 7, null, null, null, null, 1, 0, null, null, 1, '元', null, 3, null, null, null, null, null, '附件表', 2100191527, null, null, null, null
, '视图ID', '视图组ID');


-- 视图组与视图关联关系
insert into gap_ui_view_group_view (VIEW_GROUP_ID, VIEW_ID)
values ('视图组ID', '视图ID');


-- 字段列
insert into gap_ui_column (FIELD_CODE, NAME, DISP_ORDER, TITLE, DATA_TYPE, IS_VISIBLE, IS_ENABLED, DEFAULT_VALUE, COLUMN_WIDTH, COLUMN_HEIGHT, ALIGNMENT, DEC_LEN, ELE_CODE, ELE_SHOW_TYPE, ELE_LEVEL_NUM, ELE_CODE_PRIMARY, ELE_CODE_RELATED, IS_CALCULATED, IS_REQUIRED, EXPRESSION, PARENT_ID, ROW_SPAN, COL_SPAN, COL_LEVEL, MAX_LENGTH, MIN_LENGTH, IS_FROZEN, REMARK, IS_COLUMN_SORT, TIPS, IS_LOADELEMENT, SWHERE, ELE_BOUND_VALUE, ELE_DISPLAY_VALUE, ROW_WRAP
, ID, VIEW_ID, JUMP_GROUP_ID, YEAR)
values ('xh', '序号', 1, '序号', '7', 1, 1, null, '120', null, null, '0', null, '0', '0', null, null, null, 0, null, '0', '1', '1', '1', 0, 0, null, '序号', null, null, null, null, null, null, null
, sys_guid(), '视图ID', null, null);

insert into gap_ui_column (FIELD_CODE, NAME, DISP_ORDER, TITLE, DATA_TYPE, IS_VISIBLE, IS_ENABLED, DEFAULT_VALUE, COLUMN_WIDTH, COLUMN_HEIGHT, ALIGNMENT, DEC_LEN, ELE_CODE, ELE_SHOW_TYPE, ELE_LEVEL_NUM, ELE_CODE_PRIMARY, ELE_CODE_RELATED, IS_CALCULATED, IS_REQUIRED, EXPRESSION, PARENT_ID, ROW_SPAN, COL_SPAN, COL_LEVEL, MAX_LENGTH, MIN_LENGTH, IS_FROZEN, REMARK, IS_COLUMN_SORT, TIPS, IS_LOADELEMENT, SWHERE, ELE_BOUND_VALUE, ELE_DISPLAY_VALUE, ROW_WRAP
, ID, VIEW_ID, JUMP_GROUP_ID, YEAR)
values ('affix_title', '标题', 3, '标题', '0', 1, 1, null, '120', null, null, '0', null, '0', '0', null, null, null, 0, null, '0', '1', '1', '1', 0, 0, null, '标题', null, null, null, null, null, null, null
, sys_guid(), '视图ID', null, null);

insert into gap_ui_column (FIELD_CODE, NAME, DISP_ORDER, TITLE, DATA_TYPE, IS_VISIBLE, IS_ENABLED, DEFAULT_VALUE, COLUMN_WIDTH, COLUMN_HEIGHT, ALIGNMENT, DEC_LEN, ELE_CODE, ELE_SHOW_TYPE, ELE_LEVEL_NUM, ELE_CODE_PRIMARY, ELE_CODE_RELATED, IS_CALCULATED, IS_REQUIRED, EXPRESSION, PARENT_ID, ROW_SPAN, COL_SPAN, COL_LEVEL, MAX_LENGTH, MIN_LENGTH, IS_FROZEN, REMARK, IS_COLUMN_SORT, TIPS, IS_LOADELEMENT, SWHERE, ELE_BOUND_VALUE, ELE_DISPLAY_VALUE, ROW_WRAP
, ID, VIEW_ID, JUMP_GROUP_ID, YEAR)
values ('file_name', '附件', 26, '附件', '0', 1, 0, null, '120', null, null, '0', null, '0', '0', null, null, null, 0, null, '0', '1', '1', '1', 0, 0, null, '附件', null, null, null, null, null, null, null
, sys_guid(), '视图ID', null, null);

insert into gap_ui_column (FIELD_CODE, NAME, DISP_ORDER, TITLE, DATA_TYPE, IS_VISIBLE, IS_ENABLED, DEFAULT_VALUE, COLUMN_WIDTH, COLUMN_HEIGHT, ALIGNMENT, DEC_LEN, ELE_CODE, ELE_SHOW_TYPE, ELE_LEVEL_NUM, ELE_CODE_PRIMARY, ELE_CODE_RELATED, IS_CALCULATED, IS_REQUIRED, EXPRESSION, PARENT_ID, ROW_SPAN, COL_SPAN, COL_LEVEL, MAX_LENGTH, MIN_LENGTH, IS_FROZEN, REMARK, IS_COLUMN_SORT, TIPS, IS_LOADELEMENT, SWHERE, ELE_BOUND_VALUE, ELE_DISPLAY_VALUE, ROW_WRAP
, ID, VIEW_ID, JUMP_GROUP_ID, YEAR)
values ('operation', '操作', 31, '操作', '21', 1, 1, null, '120', null, '0', '0', null, '3', null, null, null, 1, 0, null, '0', '1', '1', '1', 0, 0, 0, null, 1, null, 1, null, null, null, null
, sys_guid(), '视图ID', null, null);


-- 视图按钮
insert into gap_ui_button (NAME, DISP_ORDER, TITLE, ICON, IS_ENABLED, EDITABLE_STATUS, FUNCTION_NAME, PARAMS, START_DATE, END_DATE, REMARK, PROPERTY_NAME, JUMP_GROUP_ID, REPORT_URL, ID, VIEW_ID, BUSI_TYPE, IS_OPERATED)
values ('保存', 1, '保存', '/basic-web/grp/images/btnImages/bc.png', 1, 1, 'save', null, null, null, null, 'save', null, null
, Utl_Raw.Cast_To_Raw(sys.dbms_obfuscation_toolkit.md5(input_string => '视图ID' || '01')), '视图ID', null, null);

insert into gap_ui_button (NAME, DISP_ORDER, TITLE, ICON, IS_ENABLED, EDITABLE_STATUS, FUNCTION_NAME, PARAMS, START_DATE, END_DATE, REMARK, PROPERTY_NAME, JUMP_GROUP_ID, REPORT_URL, ID, VIEW_ID, BUSI_TYPE, IS_OPERATED)
values ('取消', 2, '取消', '/basic-web/grp/images/btnImages/qx.png', 1, 1, 'com_btn_cancel', null, null, null, null, 'com_btn_cancel', null, null
, Utl_Raw.Cast_To_Raw(sys.dbms_obfuscation_toolkit.md5(input_string => '视图ID' || '02')), '视图ID', null, null);


-- 视图按钮状态（不需要走工作流时，可以不要打）
insert into gap_ui_button_wf_status (BUTTON_ID, WF_STATUS, ID_COPY)
values (Utl_Raw.Cast_To_Raw(sys.dbms_obfuscation_toolkit.md5(input_string => '视图ID' || '01')), '0', Utl_Raw.Cast_To_Raw(sys.dbms_obfuscation_toolkit.md5(input_string => '视图ID' || '01')));

insert into gap_ui_button_wf_status (BUTTON_ID, WF_STATUS, ID_COPY)
values (Utl_Raw.Cast_To_Raw(sys.dbms_obfuscation_toolkit.md5(input_string => '视图ID' || '02')), '0', Utl_Raw.Cast_To_Raw(sys.dbms_obfuscation_toolkit.md5(input_string => '视图ID' || '02')));
