
-- =========================隐藏人员信息审核岗二级列表视图的日志按钮====./start========
-- 基础库【人员信息-审核岗】二级列表视图的【日志】按钮不再展示(展示了点了也没有反应)，右上角有统一的【查看日志】按钮。
-- F12打开浏览器控制台，找到 /personalview/runBean/uiGroups 接口，从返回值中找到【根视图组ID】然后替换下面的值
UPDATE Gap_Ui_Button T5
   SET t5.is_enabled = 0
 where t5.view_id in (select t4.id
                        from gap_ui_view t4
                       where t4.id in
                             (select t3.view_id
                                from gap_ui_view_group_view t3
                               where t3.view_group_id in
                                     (select t.id
                                        from GAP_UI_VIEW_GROUP t
                                       start with t.id = '根视图组ID'
                                      connect by prior t.id = t.parent_id))
                         and t4.code not like '%_detail'
                         and t4.view_type = 3)
   and t5.function_name = 'com_btn_log'
   and t5.is_enabled = 1;
-- =========================隐藏人员信息审核岗二级列表视图的日志按钮====./end========
