
  -- 存储过程
create or replace procedure pro_del_repeat_data_430100000 as
Begin
  delete from bas_person_info
   where (agency_code, ui_code, IDEN_NO, per_name) in
         (select agency_code, ui_code, IDEN_NO, per_name
            from bas_person_info
           where mof_div_code = '430100000' --区划
             and is_deleted = 2
           group by agency_code, ui_code, IDEN_NO, per_name
          having count(*) > 1)
     and rowid not in (select max(rowid)
                         from bas_person_info
                        where mof_div_code = '430100000' --区划
                          and is_deleted = 2
                        group by agency_code, ui_code, IDEN_NO, per_name
                       having count(*) > 1)
     and mof_div_code = '430100000'; --区划;
  commit;
End;

  --创建定时器任务
declare
  idcard_alert_job_430100000 number; -- job 主键id，不用传，会自动生成
begin
  sys.dbms_job.submit(idcard_alert_job_430100000, -- job 主键id，不用传，会自动生成
                      'pro_del_repeat_data_430100000;', -- 预警信息、指令信息、综合统计的存储过程名称(分号不能少)
                      sysdate, -- job 启动后第一次执行任务的时间
                      'trunc(sysdate,''mi'') + 30/ (24*60)'); --每30分钟执行一次
  commit;
end;

  -- 查询定时器 JOB
select a.* from dba_jobs a where what in ('pro_del_repeat_data_430100000;');

  -- 删除JOB, 根据主键 job 值进行删除.
begin
  dbms_job.remove(4);
  commit;
end;
