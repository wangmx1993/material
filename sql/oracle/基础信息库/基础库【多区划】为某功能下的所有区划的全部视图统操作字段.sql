
-- 为某个功能下的所有区划的全部视图统一添加字段
-- 因为不同区划使用了不同的视图配置，但是它们的功能id是一样的
declare
cursor vrows is
    select t3.id,t3.name from gap_ui_view t3 where t3.group_id in(
        select t.id from GAP_UI_VIEW_GROUP t start with t.code in(select t2.uiview_group_code from gap_ui_module t2 where t2.id in('28213214')) connect by prior t.id = t.parent_id
     ) and t3.view_type=3 and t3.code in('202005','202006','202007','202008','202009');
p number := 10; -- 执行 sql 的条数
v_id varchar2(36) := '0' ; -- 插入字段的字段 id,guid 自动生成
v_disp_order number ;--字段的顺序号，再原视图最大序号的基础上 +5
v_row_sapn varchar2(60); --新增字段的跨行数
begin
  for vrow in vrows loop
    v_id := sys_guid();
    SELECT nvl(max(Tc.Disp_Order),0)+5 into v_disp_order FROM GAP_UI_COLUMN tc where tc.view_id = vrow.id;
    SELECT nvl(max(to_number(tc.col_level)),0) into v_row_sapn FROM GAP_UI_COLUMN tc where tc.view_id = vrow.id and tc.is_visible=1;
    insert into GAP_UI_COLUMN
    (FIELD_CODE, NAME, DISP_ORDER, TITLE, DATA_TYPE, IS_VISIBLE, IS_ENABLED, DEFAULT_VALUE, COLUMN_WIDTH, COLUMN_HEIGHT, ALIGNMENT, DEC_LEN, ELE_CODE, ELE_SHOW_TYPE, ELE_LEVEL_NUM, ELE_CODE_PRIMARY, ELE_CODE_RELATED, IS_CALCULATED, IS_REQUIRED, EXPRESSION, PARENT_ID, ROW_SPAN, COL_SPAN, COL_LEVEL, MAX_LENGTH, MIN_LENGTH, IS_FROZEN, REMARK, IS_COLUMN_SORT, TIPS, IS_LOADELEMENT, SWHERE, ELE_BOUND_VALUE, ELE_DISPLAY_VALUE, ROW_WRAP, ID, VIEW_ID, JUMP_GROUP_ID)
    select
    'uOpration', '操作', v_disp_order, '操作', '0', 1, 1, null, '120', null, '0', '0', null, '1', '1', null, null, 1, 0, null, '0', v_row_sapn, '1', '1', 120, 0, 0, null, 1, null, 1, null, 'id', 'codeName', null, v_id , vrow.id, '0'
    from dual ;
    p := p + 1;
    dbms_output.put_line('序号：' || p ||  '，新增字段id= ' || v_id || '，disp_order=' || v_disp_order || ', row_sapn=' || v_row_sapn || ', 视图名称=' || vrow.name );
  end loop;
end;


-- 为某个功能下的所有区划的全部视图统一修改某字段
declare
cursor vrows is
    select t3.id,t3.name,t3.code,t3.view_type,t3.tenant_id from gap_ui_view t3 where t3.group_id in(
        select t.id from GAP_UI_VIEW_GROUP t start with t.code in(select t2.uiview_group_code from gap_ui_module t2 where t2.id in('28213214')) connect by prior t.id = t.parent_id
     )  and t3.code in('202005','202006','202007','202008','202009','202005_detail','202006_detail','202007_detail','202008_detail','202009_detail');
p number := 10; -- 统计执行 sql 的条数，可以任意定义
begin
  for vrow in vrows loop
    update gap_ui_column t set t.is_enabled=0 where t.field_code='is_auth' and t.view_id=vrow.id;
    dbms_output.put_line('序号：' || p ||  '，修改字段 is_auth 为不可编辑，' || ', 视图名称=' || vrow.name || ',视图ID=' || vrow.id || '，区划=' || vrow.tenant_id);
    p := p+1;
  end loop;
end;


-- 为某个功能下的所有区划下的列表视图添加按钮
declare
cursor vrows is
    select t3.id,t3.name,t3.code,t3.view_type,t3.tenant_id from gap_ui_view t3 where t3.group_id in(
        select t.id from GAP_UI_VIEW_GROUP t start with t.code in(select t2.uiview_group_code from gap_ui_module t2 where t2.id in('28213214')) connect by prior t.id = t.parent_id
     )  and t3.code in('202005','202006','202007','202008','202009','202005_detail','202006_detail','202007_detail','202008_detail','202009_detail') and t3.view_type=3;
p number := 10; -- 统计执行 sql 的条数，可以任意定义
p_btnid1 varchar(38);
p_btnid2 varchar(38);
p_btnid3 varchar(38);
begin
  for vrow in vrows loop
    p_btnid1 := sys_guid();
    p_btnid2 := sys_guid();
    p_btnid3 := sys_guid();

    insert into gap_ui_button (NAME, DISP_ORDER, TITLE, ICON, IS_ENABLED, EDITABLE_STATUS, FUNCTION_NAME, PARAMS, START_DATE, END_DATE, REMARK, PROPERTY_NAME, JUMP_GROUP_ID, REPORT_URL, ID, VIEW_ID)
    values ('全屏编辑', 1, '全屏编辑', '/basic-web/grp/images/btnImages/bd.png', 1, 1, 'com_btn_fullChange', null, null, null, null, 'btn_fullChange', null, null, p_btnid1, vrow.id);

    insert into gap_ui_button (NAME, DISP_ORDER, TITLE, ICON, IS_ENABLED, EDITABLE_STATUS, FUNCTION_NAME, PARAMS, START_DATE, END_DATE, REMARK, PROPERTY_NAME, JUMP_GROUP_ID, REPORT_URL, ID, VIEW_ID)
    values ('批量修改', 2, '批量修改', '/basic-web/grp/images/btnImages/bj.png', 1, 1, 'com_btn_batchEdit', null, null, null, null, 'btn_batchEdit', null, null, p_btnid2, vrow.id);

    insert into gap_ui_button (NAME, DISP_ORDER, TITLE, ICON, IS_ENABLED, EDITABLE_STATUS, FUNCTION_NAME, PARAMS, START_DATE, END_DATE, REMARK, PROPERTY_NAME, JUMP_GROUP_ID, REPORT_URL, ID, VIEW_ID)
    values ('取消全屏编辑', 2, '取消', '/basic-web/grp/images/btnImages/bd.png', 1, 1, 'com_btn_CloseFullChange', null, null, null, null, 'btn_fullChange', null, null, p_btnid3, vrow.id);

    insert into gap_ui_button_wf_status (BUTTON_ID, WF_STATUS, ID_COPY) values (p_btnid1, '0', p_btnid1);
    insert into gap_ui_button_wf_status (BUTTON_ID, WF_STATUS, ID_COPY) values (p_btnid2, '0', p_btnid2);
    insert into gap_ui_button_wf_status (BUTTON_ID, WF_STATUS, ID_COPY) values (p_btnid3, '0', p_btnid3);

    dbms_output.put_line('生成按钮id=' || p_btnid1);
    dbms_output.put_line('生成按钮id=' || p_btnid2);
    dbms_output.put_line('生成按钮id=' || p_btnid3);
    p := p+1;
  end loop;
end;


-- 将基础库【basic-web】人员信息下的按钮名称【信息新增】修改为【新增】
update gap_ui_button t4
   set name = '新增', title = '新增'
 where t4.view_id in
       (select t3.id
          from gap_ui_view t3
         where t3.group_id in
               (select t.id
                  from GAP_UI_VIEW_GROUP t
                 start with t.code in
                            (SELECT distinct t.uiview_group_code
                               FROM gap_ui_module t
                              where t.web_server_id =
                                    (SELECT id
                                       FROM GAP_SYSTEM_SERVER
                                      where upper(code) = upper('basic-web'))
                                and t.uiview_group_code is not null)
                connect by prior t.id = t.parent_id)
           and t3.code like '20200%')
   and t4.name = '信息新增';
   


--为基础库服务(basic-web)人员信息录入菜单下5大人员类型的列表视图添加【撤回】按钮
declare
cursor vrows is
-- 查询基础库服务(basic-web)人员信息录入菜单下5大人员类型的列表视图，需要排除审核岗，以及基础信息查询，以及分级授权查询
select t3.*
  from gap_ui_view t3
 where t3.group_id in
       (select t.id
          from GAP_UI_VIEW_GROUP t
         start with t.code in
                    (SELECT distinct t.uiview_group_code
                       FROM gap_ui_module t
                      where t.web_server_id =
                            (SELECT id
                               FROM GAP_SYSTEM_SERVER
                              where upper(code) = upper('basic-web'))
                        and t.uiview_group_code is not null and t.url='grp/html/presonInfo/personInfo.html')
        connect by prior t.id = t.parent_id)
   and t3.code like '20200%'
   and t3.view_type = 3
   and t3.title not like '基础数据查询%'
   and t3.title not like '分级授权%';

  p number := 10; -- 统计执行 sql 的条数，可以任意定义
  p_btnid1 varchar(38);
begin
  for vrow in vrows loop
    p_btnid1 := sys_guid();

    insert into gap_ui_button (NAME, DISP_ORDER, TITLE, ICON, IS_ENABLED, EDITABLE_STATUS, FUNCTION_NAME, PARAMS, START_DATE, END_DATE, REMARK, PROPERTY_NAME, JUMP_GROUP_ID, REPORT_URL, ID, VIEW_ID)
    values ('撤回', (SELECT max(disp_order) FROM gap_ui_button where view_id=vrow.id) , '撤回', '/basic-web/grp/images/btnImages/ch.png', 1, 1, 'com_btn_cancelAudit', null, null, null, null, 'btn_cancelAudit', null, null, p_btnid1, vrow.id);

    insert into gap_ui_button_wf_status (BUTTON_ID, WF_STATUS, ID_COPY) values (p_btnid1, '1', p_btnid1);

    dbms_output.put_line('生成按钮id=' || p_btnid1);
    p := p+1;
  end loop;
end;



--调整基础库服务(basic-web)人员信息录入菜单下5大人员类型的列表视图的按钮顺序
declare
cursor vrows is
-- 查询基础库服务(basic-web)人员信息录入菜单下5大人员类型的列表视图下的按钮，需要排除审核岗，以及基础信息查询，以及分级授权查询
  SELECT T4.* FROM gap_ui_button t4 where t4.view_id in(
  select t3.id
    from gap_ui_view t3
   where t3.group_id in
         (select t.id
            from GAP_UI_VIEW_GROUP t
           start with t.code in
                      (SELECT distinct t.uiview_group_code
                         FROM gap_ui_module t
                        where t.web_server_id =
                              (SELECT id
                                 FROM GAP_SYSTEM_SERVER
                                where upper(code) = upper('basic-web'))
                          and t.uiview_group_code is not null and t.url='grp/html/presonInfo/personInfo.html')
          connect by prior t.id = t.parent_id)
     and t3.code like '20200%'
     and t3.view_type = 3
     and t3.title not like '基础数据查询%'
     and t3.title not like '分级授权%'
     );
  p number := 10; -- 统计执行 sql 的条数，可以任意定义
begin
  for vrow in vrows loop
      if vrow.title='新增' then
        update gap_ui_button t set t.disp_order = 1 where t.id=vrow.id;
      end if;
     if vrow.title='删除' then
        update gap_ui_button t set t.disp_order = 3 where t.id=vrow.id;
      end if;
     if vrow.title='送审' then
        update gap_ui_button t set t.disp_order = 5 where t.id=vrow.id;
      end if;
     if vrow.title='撤回' then
        update gap_ui_button t set t.disp_order = 7 where t.id=vrow.id;
      end if;
     if vrow.title='调入' then
        update gap_ui_button t set t.disp_order = 9 where t.id=vrow.id;
      end if;
     if vrow.title='调出' then
        update gap_ui_button t set t.disp_order = 11 where t.id=vrow.id;
      end if;
     if vrow.title='全屏编辑' then
        update gap_ui_button t set t.disp_order = 13 where t.id=vrow.id;
      end if;
     if vrow.title='批量修改' then
        update gap_ui_button t set t.disp_order = 15 where t.id=vrow.id;
      end if;
     if vrow.title='转换' then
        update gap_ui_button t set t.disp_order = 17 where t.id=vrow.id;
      end if;
     if vrow.title='导入' then
        update gap_ui_button t set t.disp_order = 19 where t.id=vrow.id;
      end if;
     if vrow.title='导出' then
        update gap_ui_button t set t.disp_order = 21 where t.id=vrow.id;
      end if;
     if vrow.title='变动查看' then
        update gap_ui_button t set t.disp_order = 23 where t.id=vrow.id;
      end if;
     if vrow.title='抽屉' then
        update gap_ui_button t set t.disp_order = 25 where t.id=vrow.id;
      end if;
    dbms_output.put_line('调整按钮顺序，id=' || vrow.id || ',title=' ||vrow.title);
    p := p+1;
  end loop;
end;


