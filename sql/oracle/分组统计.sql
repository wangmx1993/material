
-- Create table
create table PERSON_INFO_WMX (
  agency_code       VARCHAR2(21) not null,
  fiscal_year       VARCHAR2(4) not null,
  ui_code           VARCHAR2(20) not null,
  per_sta_code      VARCHAR2(10) not null,
  basic_mon         NUMBER(18,2) not null,
  achievement_bonus NUMBER(18,2)
);
-- Add comments to the columns 
comment on column PERSON_INFO_WMX.agency_code  is '单位编码';
comment on column PERSON_INFO_WMX.fiscal_year  is '年度';
comment on column PERSON_INFO_WMX.ui_code  is '人员类型:202005-行政、202006-事业';
comment on column PERSON_INFO_WMX.per_sta_code  is '人员状态:1-在职、2-退休';
comment on column PERSON_INFO_WMX.basic_mon  is '基本工资';
comment on column PERSON_INFO_WMX.achievement_bonus  is '绩效奖金';


insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('001001', '2023', '202006', '1', 3543.68, null);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('001001', '2023', '202005', '1', 7101.10, 900.00);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('001001', '2023', '202005', '2', 1992.34, null);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('001001', '2023', '202006', '2', 5221.77, null);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('001001', '2023', '202006', '2', 5076.56, null);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('001001', '2023', '202005', '1', 2461.41, null);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('001001', '2023', '202005', '2', 2134.03, null);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('001001', '2024', '202005', '1', 7553.15, null);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('001001', '2024', '202005', '1', 7029.24, null);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('001001', '2024', '202005', '1', 8729.98, 600.00);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('001001', '2024', '202005', '1', 2801.33, null);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('002011', '2023', '202006', '1', 9735.98, null);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('002011', '2023', '202006', '1', 6730.53, 896.45);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('002011', '2023', '202005', '1', 8328.03, 450.00);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('002011', '2024', '202005', '1', 4890.63, null);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('002011', '2024', '202006', '1', 6730.90, 850.55);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('002011', '2024', '202005', '1', 3588.22, null);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('002011', '2024', '202005', '1', 6811.40, null);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('002011', '2024', '202005', '1', 9851.61, null);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('002011', '2024', '202005', '1', 1792.80, 1200.88);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('002011', '2024', '202005', '1', 6191.22, 1500.00);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('100027', '2023', '202005', '1', 8147.13, null);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('100027', '2023', '202006', '2', 4814.65, null);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('100027', '2023', '202005', '2', 3613.07, 600.00);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('100027', '2023', '202006', '2', 5464.31, 2500.00);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('100027', '2023', '202006', '2', 7154.58, null);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('100027', '2023', '202005', '1', 8246.99, null);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('100027', '2023', '202005', '1', 7139.60, null);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('100027', '2023', '202006', '1', 3344.92, null);
insert into person_info_wmx (AGENCY_CODE, FISCAL_YEAR, UI_CODE, PER_STA_CODE, BASIC_MON, ACHIEVEMENT_BONUS) values ('100027', '2023', '202006', '1', 2045.68, 500.00);

--分组统计方式1——不推荐(子查询效率不高)
SELECT agency_code,
       fiscal_year,
       (SELECT count(1) FROM person_info_wmx T1
         where t1.agency_code = a.agency_code and t1.fiscal_year = a.fiscal_year and t1.ui_code = '202005' and t1.per_sta_code = '1') 
         as "行政在职人员汇总",
       (SELECT count(1) FROM person_info_wmx T1
         where t1.agency_code = a.agency_code and t1.fiscal_year = a.fiscal_year and t1.ui_code = '202005' and t1.per_sta_code = '2') 
         as "行政退休人员汇总",
       (SELECT count(1) FROM person_info_wmx T1
         where t1.agency_code = a.agency_code and t1.fiscal_year = a.fiscal_year and t1.ui_code = '202006' and t1.per_sta_code = '1') 
         as "事业在职人员汇总",
       (SELECT count(1) FROM person_info_wmx T1
         where t1.agency_code = a.agency_code and t1.fiscal_year = a.fiscal_year and t1.ui_code = '202006' and t1.per_sta_code = '2') 
         as "事业退休人员汇总",
       (SELECT nvl(sum(nvl(basic_mon, 0) + nvl(achievement_bonus, 0)), 0)
          FROM person_info_wmx T1
         where t1.agency_code = a.agency_code and t1.fiscal_year = a.fiscal_year and t1.ui_code = '202005' and t1.per_sta_code = '1') 
         as "行政在职工资汇总",
       (SELECT nvl(sum(nvl(basic_mon, 0) + nvl(achievement_bonus, 0)), 0)
          FROM person_info_wmx T1
         where t1.agency_code = a.agency_code and t1.fiscal_year = a.fiscal_year and t1.ui_code = '202005' and t1.per_sta_code = '2') 
         as "行政退休工资汇总",
       (SELECT nvl(sum(nvl(basic_mon, 0) + nvl(achievement_bonus, 0)), 0)
          FROM person_info_wmx T1
         where t1.agency_code = a.agency_code and t1.fiscal_year = a.fiscal_year and t1.ui_code = '202006' and t1.per_sta_code = '1') 
         as "事业在职工资汇总",
       (SELECT nvl(sum(nvl(basic_mon, 0) + nvl(achievement_bonus, 0)), 0)
          FROM person_info_wmx T1
         where t1.agency_code = a.agency_code and t1.fiscal_year = a.fiscal_year and t1.ui_code = '202006' and t1.per_sta_code = '2')
         as "事业退休工资汇总"
  FROM person_info_wmx a
 group by agency_code, fiscal_year 
 order by agency_code, fiscal_year;



--分组统计方式2——推荐
SELECT 
  agency_code, fiscal_year,
  sum(行政在职人员汇总) 行政在职人员汇总,
  sum(行政退休人员汇总) 行政退休人员汇总,
  sum(事业在职人员汇总) 事业在职人员汇总,
  sum(事业退休人员汇总) 事业退休人员汇总,
  sum(行政在职工资汇总) 行政在职工资汇总,
  sum(行政退休工资汇总) 行政退休工资汇总,
  sum(事业在职工资汇总) 事业在职工资汇总,
  sum(事业退休工资汇总) 事业退休工资汇总
 FROM (
  SELECT 
    agency_code,
    fiscal_year,
    (case when ui_code = '202005' and per_sta_code='1' then 1 else 0 end) as 行政在职人员汇总,
    (case when ui_code = '202005' and per_sta_code='2' then 1 else 0 end) as 行政退休人员汇总,
    (case when ui_code = '202006' and per_sta_code='1' then 1 else 0 end) as 事业在职人员汇总,
    (case when ui_code = '202006' and per_sta_code='2' then 1 else 0 end) as 事业退休人员汇总,
    (case when ui_code = '202005' and per_sta_code='1' then nvl(basic_mon,0) + nvl(achievement_bonus, 0) else 0 end) as 行政在职工资汇总,
    (case when ui_code = '202005' and per_sta_code='2' then nvl(basic_mon,0) + nvl(achievement_bonus, 0) else 0 end) as 行政退休工资汇总,
    (case when ui_code = '202006' and per_sta_code='1' then nvl(basic_mon,0) + nvl(achievement_bonus, 0) else 0 end) as 事业在职工资汇总,
    (case when ui_code = '202006' and per_sta_code='2' then nvl(basic_mon,0) + nvl(achievement_bonus, 0) else 0 end) as 事业退休工资汇总
     FROM person_info_wmx 
 ) group by agency_code, fiscal_year 
 order by agency_code, fiscal_year;
 
 
 --分组统计方式3——推荐
 SELECT 
  agency_code,
  fiscal_year,
  sum((case when ui_code = '202005' and per_sta_code='1' then 1 else 0 end)) as 行政在职人员汇总,
  sum((case when ui_code = '202005' and per_sta_code='2' then 1 else 0 end)) as 行政退休人员汇总,
  sum((case when ui_code = '202006' and per_sta_code='1' then 1 else 0 end)) as 事业在职人员汇总,
  sum((case when ui_code = '202006' and per_sta_code='2' then 1 else 0 end)) as 事业退休人员汇总,
  sum((case when ui_code = '202005' and per_sta_code='1' then nvl(basic_mon,0) + nvl(achievement_bonus, 0) else 0 end)) as 行政在职工资汇总,
  sum((case when ui_code = '202005' and per_sta_code='2' then nvl(basic_mon,0) + nvl(achievement_bonus, 0) else 0 end)) as 行政退休工资汇总,
  sum((case when ui_code = '202006' and per_sta_code='1' then nvl(basic_mon,0) + nvl(achievement_bonus, 0) else 0 end)) as 事业在职工资汇总,
  sum((case when ui_code = '202006' and per_sta_code='2' then nvl(basic_mon,0) + nvl(achievement_bonus, 0) else 0 end)) as 事业退休工资汇总
FROM person_info_wmx group by agency_code, fiscal_year 
order by agency_code, fiscal_year;


 