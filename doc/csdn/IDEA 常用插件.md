# 1、Docker

1、Docker 插件提供与Docker的集成，可以用于：

- a）Download and build Docker images(下载并构建Docker镜像)
- b）Create and run Docker containers from pulled images or directly from a Dockerfile(从下载的镜像或直接从Dockerfile创建和运行Docker容器).
- c）Use dedicated Docker run configurations(使用专用Docker运行配置)
- d）Run multicontainer applications using Docker Compose(使用Docker Compose运行多容器应用程序)

2、Docker 插件官网文档：https://www.jetbrains.com/help/idea/docker.html。

3、IDEA 安装Docker插件->重启IDEA -> 连接远程docker服务(连接之前需要先配置下面的Docker服务) ->本地启动之后会列出远程docker容器和镜像。

4、配置docker服务远程连接端口，找到 ExecStart，添加: -H tcp://0.0.0.0:2375 -H unix://var/run/docker.sock

    vi /usr/lib/systemd/system/docker.service

![image](images/20231217175853.png)

5、重启docker服务

    systemctl daemon-reload
    systemctl start docker

3、开放Linux防火墙端口

    firewall-cmd --zone=public --add-port=2375/tcp --permanent

![image](images/2023121702.gif)

# 2、GsonFormat Json 转 Java 类

1、Json 转 Java 实体类，该插件可以根据json内容快速生成实体类，提高开发效率。

2、使用方法：先新建一个类，选中类名->右键->generate(生成)-> GsonFormat，然后输入 JSON，点击OK，即可生成。

![image](images/202401191714.gif)

# 3、POJO to Json

1、与 GsonFormat 功能相反，POJO to Json 用于将 POJO 快速复制为 JSON 文本，比如平时做接口调试时，可以用于快速生成请求体参数。

2、使用方法：选中类->右键-> POJO to Json-即可自动复制到剪贴板中，去其它地方直接 Ctrl+v粘贴即可。

![image](images/202401191729.gif)

# 4、Json Parser 解析插件

1、是一个用于验证和格式化 JSON 字符串的轻量级插件。

![image](images/cd93a928da.gif)

# 5、GenerateAllSetter

1、一键调用实体对象全部的 getter、setter() 方法，在造假数据测试时非常有用。

2、选择实体对象，按 Alt + Enter，即可出现选项。

![image](images/202401191740.png)

# 6、Chinese (Simplified) Language Pack / 中文语言包

1、中文语言包能够为 IDEA 带来完全中文化的界面(只支持IDEA 2020 以上的版本)。

2、https://plugins.jetbrains.com/plugin/13710-chinese-simplified-language-pack

![image](images/202101191751.png)

![image](images/2023071209011412.png)

# 7、Convert YAML and Properties File

1、快速将Properties文件转换为YAML文件，或将YAML文件转换为Properties文件。

- https://gitee.com/xqchen/ConvertYamlAndProperties
- https://gitee.com/link?target=https%3A%2F%2Fplugins.jetbrains.com%2Fplugin%2F13804-convert-yaml-and-properties-file

![image](images/202401201002.gif)

# 8、any-rule

1、快速生成正则表达式，安装之后右击 选择 AnyRule 即可使用。

- https://gitee.com/mirrors/any-rule
- https://plugins.jetbrains.com/plugin/14162-any-rule

![image](images/202401201025.gif)

# 9、CamelCase

1、idea 自带快捷键 CTRL+SHIFT+U 进行大小写的转换，而有时候我们还需要在大小写转换的同时，还支持驼峰与下划线的转换，此时可以借助 CamelCase 插件。

2、Settings -> Plugins -> 然后输入 CamelCase 关键字搜索 -> 对目标插件点击 "Install" 按钮安装 -> 安装完成后重启 IDEA 生效

3、安装 CamelCase 插件之后，可以使用快捷键 SHIFT+ALT+U，选中目标单词，按 u 就会变，直到变成想要的即可。

4、显然默认的快捷键 SHIFT+ALT+U 需要两只手才能操作，有时候不是很方便，所以可以修改它，比如我是 Alt+1。

![image](images/7d0460b6eff.gif)

![image](images/af91a2753c75.png)

# 10、SonarLint 代码检查插件

1、SonarLint 是 SonarQube的单机版，可以作为IDEA的插件进行代码规范检查，也可以连接到SonarQube中生成统计报告。

2、SonarLint 安装：File -> Settings -> plugins -> 搜索**SonarLint**，安装并重启。

![图片](images/8761674706490.png)

3、SonarLint 使用：File -> Settings -> Tools -> SonarLint，可以设置连接到 sonarQube /sonarcloud，设置检查规则等。

![图片](images/202404010959.gif)

![图片](images/20200318160948597.png)

4、**三种问题类型**：

| 问题类型          | 类型   | 备注                  |
| ------------- | ---- | ------------------- |
| Bug           | Bug  | 代码 Bug，影响程序运行(建议处理) |
| Vulnerability | 漏洞   | 漏洞，有可能被攻击(建议处理)     |
| Code smell    | 代码异味 | 可以优化，不然会影响代码的可维护性   |

5、**五个问题等级**

| 严重程度     | 严重性 | 备注                  |
| -------- | --- | ------------------- |
| BLOCKER  | 阻断  | 影响程序正常运行(建议处理)      |
| CRITICAL | 严重  | 可能会影响程序运行，安全等(建议处理) |
| MAJOR    | 主要  | 影响开发效率，代码质量(建议处理)   |
| MINOR    | 次要  | 可能会影响开发效率，代码质量      |
| INFO     | 提示  | 不影响程序，一些建议          |

# 11、copy rest url 复制接口 REST URL

1、插件主页：https://plugins.jetbrains.com/plugin/9436-copy-rest-url。

2、只需右键控制层接口方法，然后选择“复制 REST Url”，它就会将 url 复制到剪贴板。

3、还可以在方法名称上使用 ctrl + alt + R 快捷键。然后您可以将其粘贴到浏览器或您想要的任何位置。

4、该插件仅适用于具有 @RequestMapping、@GetMapping、@PostMapping、@PutMapping、@DeleteMapping @PatchMapping 注释的方法。
