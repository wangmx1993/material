# 注释

1、注释在Lua中用于添加说明和注解。单行注释以`--`开始，多行注释则使用`--[[ ... ]]`

```bash
--单行注释

--[[
多行注释
横杠与方括号之间不要有空格
]]
```

# 内置全局变量 - KEYS 与 ARGV

1、Lua 脚本中通过内置的全局变量 KEYS 数组访问调用脚本时传入的 key，用 1 为基址的形式访问( KEYS[1] ， KEYS[2] ，以此类推)。可以参考下面的《Redis Eval 命令 - 执行 Lua 脚本》

2、Lua 脚本中通过内置的全局变量 ARGV 数组访问调用脚本时传入的附加参数，访问的形式和 KEYS 变量类似( ARGV[1] 、 ARGV[2] ，诸如此类)。可以参考下面的《Redis Eval 命令 - 执行 Lua 脚本》

# **局部变量**

1、变量在Lua中无需显式声明类型。使用local关键字创建局部变量，没有local关键字时表示全局变量。

```bash
local key1 = KEYS[1]
local key2 = KEYS[2]
local value1 = ARGV[1]
local value2 = ARGV[2]
return {key1,key2,value1,value2} --返回一个数据集合
```

# **数据类型**

1、基本数据类型包括整数、浮点数、字符串、布尔值和nil。表是一种非常灵活的数据结构。

```bash
local num = 42
local str = "Hello, Lua!"
local flag = true
local empty = nil
local person = { name = "John", age = 30 }

return {num, str, flag, person.name, person.age}
```

# 字符串处理函数 **&** 内容拼接

1、两个连续的点(..)表示内容拼接，相当于 plsql的 ||。 

```bash
local user_id = tonumber(ARGV[1])
return 'user:'..user_id..':list'  --返回的结果如 user:10:list
```

2、Lua提供了许多字符串处理函数，例如string.sub用于截取子串，string.find用于查找字符串中的子串等 。

tonumber(str)：将字符串转为数字类型

```bash
local text = "Lua programming"
local sub = string.sub(text, 1, 3)
local sub = 'Hello ' .. sub
-- 返回 "Hello Lua"
return sub

-- 调用示例 E:\Redis-x64-5.0.14.1>redis-cli.exe -h 10.104.65.76 -p 6379 --eval lua/helloWorld.lua
-- "Hello Lua"
```

# 条件语句

 1、使用if、else和elseif来实现条件分支。

```bash
local age = tonumber(ARGV[1])
if age < 18 then
    return "underage"
elseif age >= 18 and age < 65 then
    return "adult"
else
    return "old"
end
```

# 循环结构

1、Lua支持for循环、while循环和repeat...until循环。 

```bash
-- 需要自动创建测试的key的个数
local size = tonumber(ARGV[1])
-- 循环创建
for i = 1, size do
    --调用redis内部的set命令创建key，两个连续的点表示内容拼接，相当于 plsql的 ||
    redis.call('SET', 'auto_for_test_key'..i, i)
end
return 'OK'

-- 调用示例
-- D:\software\Redis-x64-5.0.14.1>redis-cli.exe --eval lua/helloWorld.lua , 100
-- "OK"
```

```bash
local size = tonumber(ARGV[1])
repeat
    redis.call('SET', 'auto_repeat_test_key'..size, size)
    size = size - 1
until size <= 0 --repeat至少执行一次，直到满足 until 后面的条件时退出循环
return 'OK'
```

```bash
local size = tonumber(ARGV[1])
while size > 0 do
    redis.call('SET', 'auto_while_test_key'..size, size)
    size = size - 1
end
return 'OK'
```

# **函数 &** function关键字

1、函数在Lua中使用function关键字定义，可以接受参数并返回值。

```bash
local function add(a, b)
    return a + b
end

local v_a = tonumber(ARGV[1])
local v_b = tonumber(ARGV[2])
return add(v_a, v_b)

-- 调用示例
-- D:\software\Redis-x64-5.0.14.1>redis-cli.exe --eval lua/helloWorld.lua , 1 2
-- (integer) 3
-- D:\software\Redis-x64-5.0.14.1>redis-cli.exe --eval lua/helloWorld.lua , 1 -2
-- (integer) -1
```

# **表（table）**

1、表是Lua的核心数据结构，用花括号`{}`定义。表可以包含键值对，键和值可以是任何数据类型。

```bash
local person = { name = "John", age = 30, hobbies = {"Reading", "Gaming"} }
local peron_vo = {person.name, person.age, person.hobbies[1]}
return peron_vo

-- 调用示例
-- D:\software\Redis-x64-5.0.14.1>redis-cli.exe --eval lua/helloWorld.lua
-- 1) "John"
-- 2) (integer) 30
-- 3) "Reading"
```

# **错误 & 异常处理**

**1、**在Lua中，异常处理通常依赖于pcall函数（protected call）和error函数。pcall会捕获函数执行中的错误，并允许你处理它们。

**解决方案1：使用pcall和error**

```bash
local vA = tonumber(ARGV[1])
local vB = tonumber(ARGV[2])

local function divide(a, b)
    if b == 0 then
        error("The divisor cannot be 0")
    else
        return a / b
    end
end

-- status：发生异常时，值会为 nil
-- result：发生异常时，值为异常消息；未发生异常时，表示函数执行的结果;
local status, result = pcall(divide, vA, vB)
if not status then
    return 'Error:' .. result
else
    return result
end

-- 调用示例：
-- E:\Redis-x64-5.0.14.1>redis-cli.exe -h 10.104.65.76 -p 6379 --eval lua/helloWorld.lua , 100 0
-- "Error:user_script:7: The divisor cannot be 0"
-- 
-- E:\Redis-x64-5.0.14.1>redis-cli.exe -h 10.104.65.76 -p 6379 --eval lua/helloWorld.lua , 100 25
-- (integer) 4
```

**解决方案2：使用assert**

```bash
local vA = tonumber(ARGV[1])
local vB = tonumber(ARGV[2])

local function divide(a, b)
    -- assert函数在其条件为false时引发错误
    assert(b ~= 0, "The divisor cannot be 0")
    return a / b
end

-- status：发生异常时，值会为 nil
-- result：发生异常时，值为异常消息；未发生异常时，表示函数执行的结果;
local status, result = pcall(divide, vA, vB)
if not status then
    return 'Error:' .. result
else
    return result
end
```

# **标准库**

1、Lua标准库包含丰富的功能，如文件操作、网络编程、正则表达式、时间处理等。你可以通过内置的模块来使用这些功能，如io、socket等。

## redis.call 调用 Redis 命令

在 Lua 脚本 中调用 Redis 命令：redis.call(command, key [param1, param2…])

```
redis.call('SET', 'user:1', '1012120') -- 设置缓存
local data = redis.call('GET', 'user:1') -- 读取缓存
local keys = redis.call('KEYS', ‘user:’) -- 获取匹配的键
```


