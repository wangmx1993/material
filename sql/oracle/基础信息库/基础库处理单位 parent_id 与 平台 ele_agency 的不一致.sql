
-- 处理基础库单位 parent_id 与 平台 ele_agency 不一致的单位
declare
  i number := 0;
  cursor vrows is
    SELECT T.Parent_Id, t.ele_code, t.mof_div_code
      FROM ele_agency t
     where t.ele_id in (SELECT t2.biz_key
                          FROM bas_agency_info t2
                         where nvl(t2.parent_id, '0') = '0'
                           and length(t2.agency_code) > 3)
       and nvl(t.parent_id, '0') != '0'
       and t.is_deleted !=1;
begin
  for vrow in vrows loop
    update bas_agency_info t
       set t.parent_id = vrow.Parent_Id
     where t.agency_code = vrow.ele_code
       and t.mof_div_code = vrow.mof_div_code;
    dbms_output.put_line('更新【' || vrow.mof_div_code || '-' ||
                         vrow.ele_code || ' 】单位 parent_id= ' ||
                         vrow.Parent_Id);
    i := i + 1;
  end loop;
  dbms_output.put_line('一共更新【' || i || ' 】条数据');
end;
