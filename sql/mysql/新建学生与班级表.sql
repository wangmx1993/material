DROP TABLE IF EXISTS student;
DROP TABLE IF EXISTS classes;

CREATE TABLE IF NOT EXISTS classes(
    id INT PRIMARY KEY auto_increment COMMENT '主键',
    name VARCHAR(64) UNIQUE COMMENT '班号',
    info VARCHAR(126) DEFAULT NULL COMMENT '描述',
    total INT DEFAULT 0 COMMENT '班级人数' 
);

CREATE TABLE IF NOT EXISTS student(
    id INT PRIMARY KEY auto_increment COMMENT '主键',
    name VARCHAR(16) NOT NULL COMMENT '姓名',
    birthday date NOT NULL COMMENT '出生日期',
    classes_name VARCHAR(64) COMMENT '学生所属班级。外键', -- 与参考的外键数据类型必须一致。
    FOREIGN KEY(classes_name) REFERENCES classes(name)
);


INSERT INTO classes VALUES(NULL,'328','重点班',2);
INSERT INTO classes VALUES(NULL,'331','重点班',1);
INSERT INTO classes VALUES(NULL,'338','普通班',0),(NULL,'332','普通班',0);
INSERT INTO student(name, birthday, classes_name) VALUES('张三', '1992-08-15', '328');
INSERT INTO student(name, birthday, classes_name) VALUES('李四', '1992-04-25', '328');
INSERT INTO student(name, birthday, classes_name) VALUES('王五', '1993-03-05', '331');