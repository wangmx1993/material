1、[通义灵码](https://tongyi.aliyun.com/lingma)，是一款基于通义大模型的智能编码辅助工具，提供行级/函数级实时续写、自然语言生成代码、单元测试生成、代码注释生成、代码解释、研发智能问答、异常报错排查等能力，并针对阿里云 SDK/API 的使用场景调优，为开发者带来高效、流畅的编码体验。

- 根据语法和跨文件的上下文，实时生成建议代码。

- 通过自然语言描述你想要的功能，可直接在编辑器区生成代码及相关注释，编码心流不间断。

- 支持 JUnit、Mockito、Spring Test、unit test、pytest 等框架生成单元测试。

- 一键生成方法注释及行间注释，节省你写代码注释的时间，有效提升代码可读性。

- 支持 30 多种语言的识别，选中代码后可自动识别编程语言并生成代码解释。跨越语言的边界，让你编码更自信。

- 研发领域自由问答 AI Coding Chat

- 异常问题排查 Troubleshoot exception

- 技术文档和代码搜索 Massive code and technical documents search

2、IDEA官网插件地址：https://plugins.jetbrains.com/plugin/17809-tongyi-lingma--your-ai-coding-assistant-type-less-code-more-

3、安装：File–>Settings->Plugins，切换到Marketplace选项卡下，输入“TONGYI ”或“TONGYI Lingma”进行搜索，找到TONGYI Lingma插件，Install安装即可。

4、安装后IDEA最右侧有通码义灵，点击“登录”，出现“通义灵码协议”对话框，选择“同意并登录”。

![image](images/2024-10-25_09-46-40.gif)
