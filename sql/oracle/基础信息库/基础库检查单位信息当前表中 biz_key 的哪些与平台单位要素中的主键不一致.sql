

-- 检查单位信息当前表中 biz_key 的哪些与平台单位要素中的主键不一致.
declare
i number :=0;
cursor vrows is 
SELECT t.mof_div_code,
       T.Ele_Id,
       t.ele_code,
       t.ele_name,
       t2.biz_key,
       t2.update_time
  FROM ele_agency t, bas_agency_info t2
 where t.ele_code = t2.agency_code
   and t.mof_div_code = t2.mof_div_code
   and t.ele_id != t2.biz_key
   and t.is_deleted !=1;
begin
  for vrow in vrows loop
     update bas_agency_info t set t.biz_key = vrow.Ele_Id where t.agency_code = vrow.ele_code and t.mof_div_code = vrow.mof_div_code;
     dbms_output.put_line('更新【'|| vrow.mof_div_code || '-' || vrow.ele_code  ||' 】单位 biz_key= ' || vrow.Ele_Id); 
     i := i+1;
  end loop; 
   dbms_output.put_line('一共更新【'|| i ||' 】条数据'); 
end;