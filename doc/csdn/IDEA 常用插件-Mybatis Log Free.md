# Mybatis Log Free插件介绍

1、MyBatis Log Free 是一个免费的用于在 IntelliJ IDEA 中显示 MyBatis 日志的插件。它可以帮助您更方便地查看和分析 MyBatis 的 SQL 执行情况，以及定位潜在的性能问题，提高开发效率。

![图片](images/DM_20240329161942_004.GIF)

# Mybatis Log Free插件安装

1、进入 **Settings** **\->** **Plugins** **\->** **Marketplace** \*\*-> \*\*在搜索框输入`Mybatis Log Free`，点击`Installed`安装。  

MybatisX安装后不需要重启IDEA，可以直接使用。

![图片](images/DM_20240329161942_001.JPG)

2、选择IDEA的`Tools`\->`MyBatis Log Plugin`，打开 Mybatis Log Free 日志面板。

![图片](images/DM_20240329161942_002.jpg)

3、Mybatis Log Free会自动监控MyBatis的日志，并把MyBatis执行的SQL语句中的？替换为对应的参数值，显示在`Mybatis Log`面板中。

![图片](images/DM_20240329161942_003.jpg)