
-- ===============要素库(element)-通用视图配置======./start=======

-- 修正【基础库】系统下面【证件类型】字段的[默认值]配置
UPDATE gfm_col_column v3
   SET v3.default_value =
       (SELECT t2.ele_id || ';' || t2.ele_code || ';' || t2.ele_name
          FROM ELE_UNION t2
         where t2.ele_catalog_code = 'IdenType'
           and t2.ele_code = '01')
 where v3.field_code = 'iden_type_code'
   and v3.ele_code = 'IdenType'
   and v3.default_value is not null
   --只修改基础库的，以免影响其它系统
   and v3.view_id in
       (SELECT v2.id
          FROM gfm_col_view v2
         where v2.busi_id in (SELECT v1.busi_id
                                FROM GFM_COL_BUSI_TYPE v1
                               where v1.sys_type = '0'))
   and exists
 (SELECT 1
          FROM ELE_UNION t2
         where t2.ele_catalog_code = 'IdenType'
           and t2.ele_code = '01')
   and v3.default_value !=
       (SELECT t2.ele_id || ';' || t2.ele_code || ';' || t2.ele_name
          FROM ELE_UNION t2
         where t2.ele_catalog_code = 'IdenType'
           and t2.ele_code = '01');


-- 修正【基础库】系统下面【民族】字段的[默认值]配置
UPDATE gfm_col_column v3
   SET v3.default_value =
       (SELECT t2.ele_id || ';' || t2.ele_code || ';' || t2.ele_name
          FROM ELE_UNION t2
         where t2.ele_catalog_code = 'Nat'
           and t2.ele_code = '01')
 where v3.field_code = 'nat_code'
   and v3.ele_code = 'Nat'
   and v3.default_value is not null
   --只修改基础库的，以免影响其它系统
   and v3.view_id in
       (SELECT v2.id
          FROM gfm_col_view v2
         where v2.busi_id in (SELECT v1.busi_id
                                FROM GFM_COL_BUSI_TYPE v1
                               where v1.sys_type = '0'))
   and exists
 (SELECT 1
          FROM ELE_UNION t2
         where t2.ele_catalog_code = 'Nat'
           and t2.ele_code = '01')
   and v3.default_value !=
       (SELECT t2.ele_id || ';' || t2.ele_code || ';' || t2.ele_name
          FROM ELE_UNION t2
         where t2.ele_catalog_code = 'Nat'
           and t2.ele_code = '01');
-- ===============要素库(element)-通用视图配置======./end=======


-- =============frame库-平台视图配置===========./start==========
-- 修正【基础库】系统下面【证件类型】字段的[默认值]配置
UPDATE gap_ui_column v3
   SET v3.default_value =
       (SELECT t2.ele_id || ';' || t2.ele_code || ';' || t2.ele_name
          FROM CS_ELEMENT.ELE_UNION t2
         where t2.ele_catalog_code = 'IdenType'
           and t2.ele_code = '01')
 where v3.field_code = 'iden_type_code'
   and v3.ele_code = 'IdenType'
   and v3.default_value is not null
   --只修改基础库的，以免影响其它系统
   and v3.view_id in
       (select t3.view_id
          from gap_ui_view_group_view t3
         where t3.view_group_id in
               (select t.id
                  from GAP_UI_VIEW_GROUP t
                 start with t.code in
                            (SELECT v2.uiview_group_code
                               FROM gap_ui_module v2
                              where v2.web_server_id in
                                    (SELECT v1.id
                                       FROM gap_system_server v1
                                      where upper(v1.code) = upper('basic-web'))
                                and v2.uiview_group_code is not null)
                connect by prior t.id = t.parent_id))
   and exists
 (SELECT 1
          FROM CS_ELEMENT.ELE_UNION t2
         where t2.ele_catalog_code = 'IdenType'
           and t2.ele_code = '01')
   and v3.default_value !=
       (SELECT t2.ele_id || ';' || t2.ele_code || ';' || t2.ele_name
          FROM CS_ELEMENT.ELE_UNION t2
         where t2.ele_catalog_code = 'IdenType'
           and t2.ele_code = '01');


-- 修正【基础库】系统下面【民族】字段的[默认值]配置
UPDATE gap_ui_column v3
   SET v3.default_value =
       (SELECT t2.ele_id || ';' || t2.ele_code || ';' || t2.ele_name
          FROM CS_ELEMENT.ELE_UNION t2
         where t2.ele_catalog_code = 'Nat'
           and t2.ele_code = '01')
 where v3.field_code = 'nat_code'
   and v3.ele_code = 'Nat'
   and v3.default_value is not null
   --只修改基础库的，以免影响其它系统
   and v3.view_id in
       (select t3.view_id
          from gap_ui_view_group_view t3
         where t3.view_group_id in
               (select t.id
                  from GAP_UI_VIEW_GROUP t
                 start with t.code in
                            (SELECT v2.uiview_group_code
                               FROM gap_ui_module v2
                              where v2.web_server_id in
                                    (SELECT v1.id
                                       FROM gap_system_server v1
                                      where upper(v1.code) = upper('basic-web'))
                                and v2.uiview_group_code is not null)
                connect by prior t.id = t.parent_id))
   and exists
 (SELECT 1
          FROM CS_ELEMENT.ELE_UNION t2
         where t2.ele_catalog_code = 'Nat'
           and t2.ele_code = '01')
   and v3.default_value !=
       (SELECT t2.ele_id || ';' || t2.ele_code || ';' || t2.ele_name
          FROM CS_ELEMENT.ELE_UNION t2
         where t2.ele_catalog_code = 'Nat'
           and t2.ele_code = '01');
-- =============frame库-平台视图配置===========./end==========
