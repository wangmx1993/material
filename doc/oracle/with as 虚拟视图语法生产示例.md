# 生产示例1

```
WITH FILER_DATA AS
 (
  --查询单位001598全部有效的人员信息
  SELECT A.PER_ID,
          A.AGENCY_CODE,
          A.MOF_DIV_CODE,
          A.VERSION,
          A.IS_DELETED,
          A.PARENT_VERSION_ID
    FROM VW_BAS_PERSON_YSBZ A
   WHERE A.AGENCY_CODE = '001598'
     AND A.MOF_DIV_CODE = '430000000'
     AND A.IS_DELETED = 2)
-- 统计单位在 2023rcdtwh 版本比 2024ysbz 版本多的人员个数
SELECT 'add_count' AS count_type, COUNT(1) AS count_result
  FROM FILER_DATA A
 WHERE A.VERSION = '2023rcdtwh'
   AND NOT EXISTS (SELECT 1
          FROM BAS_PERSON_INFO B
         WHERE A.AGENCY_CODE = B.AGENCY_CODE
           AND A.MOF_DIV_CODE = B.MOF_DIV_CODE
           AND B.VERSION = '2024ysbz'
           AND B.IS_DELETED = 2
           AND A.PER_ID = B.PARENT_VERSION_ID)
UNION ALL
-- 统计单位在 2023rcdtwh 版本比 2024ysbz 版本少的人员个数
SELECT 'reduce_count' AS count_type, COUNT(1) AS count_result
  FROM FILER_DATA A
 WHERE A.VERSION = '2024ysbz' AND NOT EXISTS
 (SELECT 1
    FROM BAS_PERSON_INFO B
   WHERE A.AGENCY_CODE = B.AGENCY_CODE
     AND A.MOF_DIV_CODE = B.MOF_DIV_CODE
     AND B.VERSION = '2023rcdtwh'
     AND B.IS_DELETED = 2
     AND A.PARENT_VERSION_ID = B.PER_ID);
```

![image](images/20241024180730.png)
