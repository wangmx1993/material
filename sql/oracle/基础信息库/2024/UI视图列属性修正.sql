
-- ===============要素库(element)-通用视图配置======./start=======
-- 修正【基础库】系统下面指定字段的[要素简称(ele_code)]配置：
-- 是否编制部门决算-is_pre_depar_fin_accounts
-- 是否编制政府部门财务报告-is_pre_fin_rep_gov_dep
-- 是否编制行政事业单位国有资产报告-is_pre_sta_own_asse_rep_adm
UPDATE gfm_col_column v3
   SET v3.ele_code = '1-是;2-否'
 where v3.field_code in ('is_pre_depar_fin_accounts',
                         'is_pre_fin_rep_gov_dep',
                         'is_pre_sta_own_asse_rep_adm')
   and v3.data_type = '1'
   and (v3.ele_code is null or v3.ele_code = '1-;2-')
      --只修改基础库的，以免影响其它系统
   and v3.view_id in
       (SELECT v2.id
          FROM gfm_col_view v2
         where v2.busi_id in (SELECT v1.busi_id
                                FROM GFM_COL_BUSI_TYPE v1
                               where v1.sys_type = '0'));
-- ===============要素库(element)-通用视图配置======./end=======


-- ===============frame库-平台视图配置=============./start=====
-- 修正【基础库】系统下面指定字段的[要素简称(ele_code)]配置：
-- 是否编制部门决算-is_pre_depar_fin_accounts
-- 是否编制政府部门财务报告-is_pre_fin_rep_gov_dep
-- 是否编制行政事业单位国有资产报告-is_pre_sta_own_asse_rep_adm
UPDATE gap_ui_column v3
   SET v3.ele_code = '1-是;2-否'
 where v3.field_code in ('is_pre_depar_fin_accounts',
                         'is_pre_fin_rep_gov_dep',
                         'is_pre_sta_own_asse_rep_adm')
   and v3.data_type = '1'
   and (v3.ele_code is null or v3.ele_code = '1-;2-')
      --只修改基础库的，以免影响其它系统
   and v3.view_id in
       (select t3.view_id
          from gap_ui_view_group_view t3
         where t3.view_group_id in
               (select t.id
                  from GAP_UI_VIEW_GROUP t
                 start with t.code in
                            (SELECT v2.uiview_group_code
                               FROM gap_ui_module v2
                              where v2.web_server_id in
                                    (SELECT v1.id
                                       FROM gap_system_server v1
                                      where upper(v1.code) = upper('basic-web'))
                                and v2.uiview_group_code is not null)
                connect by prior t.id = t.parent_id));
-- ===============frame库-平台视图配置=============./end======