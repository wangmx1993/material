
-- 人员信息终审时，将审核单对应的人员信息里面audit_id='0'的数据全部置为当前审核单ID
-- 1、目标表：根据单位编码、区划、版本、年度 查询人员信息中audit_id='0'的数据，即需要置为当前流程审核单ID的数据行
-- 2、来源表：根据审核主键ID查询当前最新的审核单信息(同一个单位只取最新的，防止后期报错：ORA-30926：无法在源表中获得一组稳定的行)
-- 3、目标表与来源表数据行匹配之后，更新人员信息中的审核单ID值。
-- bas_audit_bill 表中的主键(audit_id) 对应 bas_person_info 表中的 audit_id，一对多
MERGE INTO (SELECT T.mof_div_code,
                   T.agency_code,
                   t.fiscal_year,
                   t.version,
                   t.audit_id
              FROM bas_person_info t
             where t.agency_code in ('001598', '001888')
               and t.mof_div_code = '430000000'
               and t.version = '2024rcdtwh'
               and t.fiscal_year = '2024'
               and t.audit_id = '0') info
USING (SELECT T3.*
         FROM (SELECT t2.audit_id,
                      t2.is_end,
                      T2.mof_div_code,
                      T2.agency_code,
                      t2.fiscal_year,
                      t2.version,
                      row_number() over(partition by T2.mof_div_code, T2.agency_code, t2.fiscal_year, t2.version order by t2.create_time desc) as xh
                 FROM bas_audit_bill t2
                where t2.audit_id in
                      ('1e496736-0318-417f-9376-ef2874d4d6cc',
                       'd9b65e2f-f16d-4871-af3e-94fed291068c')) t3
        where t3.xh = 1) bill
ON (info.mof_div_code = bill.mof_div_code and info.agency_code = bill.agency_code and info.fiscal_year = bill.fiscal_year and info.version = bill.version)
WHEN MATCHED THEN
  UPDATE SET info.audit_id = bill.audit_id;

