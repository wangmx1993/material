
-- 对单位信息 his 表存在与当前表存在相同的主键 agency_id 时，对 his 表特殊处理。
-- 防止后期当前表备份到 his 表时主键冲突。
-- 解决办法：将 his 表相同的主键加上前缀 '-'。
-- 注意 ext 表会关联 info 表，所以先手动往 bas_agency_info_his 表新增一条 agency_id=123 的数据，用于 bas_agency_ext_his 表临时关联
declare
  i number := 0;
  --为冲突的主键加上前缀
  pre varchar2(16) := 'a';
  cursor vrows is
  -- 查询冲突的主键
  -- 修改点1：修改区划，以实际为准，如 431321000
  -- 修改点2：修改版本，以实际数据版本为准，如 2021rcdtwh
    select t.agency_id
      from bas_agency_info t
     WHERE t.mof_div_code = '431321000'
       and t.version = '2021rcdtwh'
       and t.is_deleted = 2
       and exists (select 1 from bas_agency_info_his t2 where t.agency_id = t2.agency_id);
begin
  for vrow in vrows loop
    -- 将 ext 表临时关联到 123
    update bas_agency_ext_his t
       set t.agency_id = '123'
     where t.agency_id = vrow.agency_id;
    commit;
    -- 修改主表冲突主键
    update bas_agency_info_his t
       set t.agency_id = pre || vrow.agency_id
     where t.agency_id = vrow.agency_id;
    commit;
    -- 修改扩展表冲突主键
    update bas_agency_ext_his t
       set t.agency_id = pre || vrow.agency_id
     where t.agency_id = '123';
    commit;
    i := i + 1;
    dbms_output.put_line('更新 agency_id：' || vrow.agency_id || '=>' || ' ' || pre || vrow.agency_id);
  end loop;
  dbms_output.put_line('一共更新【' || i || ' 】条数据');
  -- 删除临时记录
  delete from  bas_agency_ext_his t where t.agency_id='123';
  commit;
end;
