
declare
  -- 拼接SQL脚本
  v_sql varchar2(1024);
  -- 补充的全部的列信息
  v_cols varchar2(1024);
begin
  -- 2023 自动同步人员草稿箱表字段

  -- 处理人员基本信息表—BAS_PERSON_INFO
  dbms_output.put_line('-- BAS_PERSON_INFO 有而 BAS_PERSON_IMP 缺少的字段 ');
  for vrow in (SELECT t1.COLUMN_NAME,
                      t1.DATA_TYPE,
                      t1.DATA_LENGTH,
                      t1.DATA_PRECISION,
                      t1.DATA_SCALE
                 FROM user_tab_columns t1
                where TABLE_NAME = 'BAS_PERSON_INFO'
                  and not exists
                (SELECT 1
                         FROM user_tab_columns t2
                        where TABLE_NAME = 'BAS_PERSON_IMP'
                          and t2.COLUMN_NAME = t1.column_name)
                order by t1.COLUMN_NAME) loop

    v_sql  := 'alter table BAS_PERSON_IMP add ' || vrow.COLUMN_NAME || ' ' ||
              vrow.data_type;
    v_cols := v_cols || ',' || vrow.COLUMN_NAME;
    --数字类型
    if vrow.data_type = 'NUMBER' then
      --指定了长度时
      if vrow.data_precision is not null then
        --当是小数时
        if vrow.data_scale > 0 then
          v_sql := v_sql || '(' || vrow.data_precision || ',' ||
                   vrow.data_scale || ')';
        else
          v_sql := v_sql || '(' || to_char(vrow.data_precision) || ')';
        end if;
      else
        v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
      end if;
      --文本类型
    elsif vrow.data_type in ('CHAR', 'VARCHAR2', 'NVARCHAR2', 'CLOB') and
          vrow.DATA_LENGTH is not null then
      v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
    end if;
    dbms_output.put_line(v_sql || ';');
  end loop;

  -- 处理人员扩展信息表—BAS_PERSON_EXT
  dbms_output.put_line('-- BAS_PERSON_EXT 有而 BAS_PERSON_IMP 缺少的字段 ');
  v_sql := '';
  for vrow in (SELECT t1.COLUMN_NAME,
                      t1.DATA_TYPE,
                      t1.DATA_LENGTH,
                      t1.DATA_PRECISION,
                      t1.DATA_SCALE
                 FROM user_tab_columns t1
                where TABLE_NAME = 'BAS_PERSON_EXT'
                  and not exists
                (SELECT 1
                         FROM user_tab_columns t2
                        where TABLE_NAME = 'BAS_PERSON_IMP'
                          and t2.COLUMN_NAME = t1.column_name)
                order by t1.COLUMN_NAME) loop

    v_sql  := 'alter table BAS_PERSON_IMP add ' || vrow.COLUMN_NAME || ' ' ||
              vrow.data_type;
    v_cols := v_cols || ',' || vrow.COLUMN_NAME;
    --数字类型
    if vrow.data_type = 'NUMBER' then
      --指定了长度时
      if vrow.data_precision is not null then
        --当是小数时
        if vrow.data_scale > 0 then
          v_sql := v_sql || '(' || vrow.data_precision || ',' ||
                   vrow.data_scale || ')';
        else
          v_sql := v_sql || '(' || to_char(vrow.data_precision) || ')';
        end if;
      else
        v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
      end if;
      --文本类型
    elsif vrow.data_type in ('CHAR', 'VARCHAR2', 'NVARCHAR2', 'CLOB') and
          vrow.DATA_LENGTH is not null then
      v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
    end if;
    dbms_output.put_line(v_sql || ';');
  end loop;

  dbms_output.put_line('-- 手动修改 vw_bas_person_imp 视图，在查询列的末尾加上以下新增的字段(如果是要素，可能需要手动补充 codename)');
  dbms_output.put_line('-- ' || v_cols);

  dbms_output.put_line('-- BAS_PERSON_IMP 有而 vw_bas_person_imp 缺少的字段 ');
  dbms_output.put_line('-- 手动修改 vw_bas_person_imp 视图，在查询列的末尾加上以下新增的字段(如果是要素，可能需要手动补充 codename)');
  for vrow in (SELECT t1.COLUMN_NAME,
                      t1.DATA_TYPE,
                      t1.DATA_LENGTH,
                      t1.DATA_PRECISION,
                      t1.DATA_SCALE
                 FROM user_tab_columns t1
                where TABLE_NAME = 'BAS_PERSON_IMP'
                  and not exists
                (SELECT 1
                         FROM user_tab_columns t2
                        where TABLE_NAME = 'VW_BAS_PERSON_IMP'
                          and t2.COLUMN_NAME = t1.column_name)
                order by t1.COLUMN_NAME) loop
    dbms_output.put_line(',' || vrow.COLUMN_NAME);
  end loop;

end;
