# 禁用 keys 命令

‌生产环境中通常会被限制使用 Redis 的 keys 模糊查询，这个命令会被禁用，而导致无法使用，主要原因是因为该命令会导致性能问题。

1. ‌**性能问题**‌：`keys *`命令需要遍历所有的键来匹配模式，这个过程的时间复杂度是O(N)，其中N是键的总数。在数据量较大的情况下，执行时间会显著增加，导致后续操作等待，从而影响系统的正常运行‌。
2. ‌**阻塞其他操作**‌：由于Redis是单线程运行的，`keys *`命令会阻塞其他操作，直到该命令执行完成。这会导致其他命令无法执行，影响系统的整体性能和响应速度‌13。

# 推荐 scan 命令

为了解决 keys 的性能问题，Redis 提供了`scan`命令作为替代方案。`scan`命令以迭代的方式进行键遍历，不会一次性返回所有匹配的键，从而避免了性能问题。相当于分页。

- ‌**语法**‌：`scan cursor [MATCH pattern] [COUNT count]`

cursor：表示游标，从“0”开始，此命令执行完后会返回一个新的cursor值。如果cursor!="0"，则表示还有key未返回，需要再调用scan，并使用此新的cursor值，来获取下一批key；如果cursor=="0"，则表示遍历结束。
pattern：模糊匹配的样式。
count：每次返回的键的数量，默认为10‌条。并不完全保证数量一致。

> 中文文档：http://doc.redisfans.com/key/scan.html
> 
> 英文文档：https://www.redis.net.cn/order/3535.html
> 
> 参考示例：https://segmentfault.com/a/1190000016717860?decode__1660=n4%2BxBD0DciYmqhDAx05%2BbDyDIO6okD9iBAe%2BoD

# scan 命令示例

```
127.0.0.1:6379> keys *
 1) "key14"
 2) "key11"
 3) "key15"
 4) "key8"
 5) "key5"
 6) "key7"
 7) "key1"
 8) "key13"
 9) "key3"
10) "key10"
11) "key12"
12) "key6"
13) "key2"
14) "key9"
15) "key4"
127.0.0.1:6379> scan 0
1) "13"
2)  1) "key11"
    2) "key12"
    3) "key14"
    4) "key7"
    5) "key1"
    6) "key2"
    7) "key10"
    8) "key15"
    9) "key8"
   10) "key5"
127.0.0.1:6379> scan 0 count 0
(error) ERR syntax error
127.0.0.1:6379> scan 0 count 1
1) "12"
2) 1) "key11"
127.0.0.1:6379> scan 0 count 2
1) "2"
2) 1) "key11"
   2) "key12"
127.0.0.1:6379> scan 0 count 3
1) "10"
2) 1) "key11"
   2) "key12"
   3) "key14"
127.0.0.1:6379> scan 0 count 100
1) "0"
2)  1) "key11"
    2) "key12"
    3) "key14"
    4) "key7"
    5) "key1"
    6) "key2"
    7) "key10"
    8) "key15"
    9) "key8"
   10) "key5"
   11) "key6"
   12) "key13"
   13) "key3"
   14) "key9"
   15) "key4"
127.0.0.1:6379> scan 0 count 3
1) "10"
2) 1) "key11"
   2) "key12"
   3) "key14"
127.0.0.1:6379> scan 10 count 3
1) "1"
2) 1) "key7"
   2) "key1"
   3) "key2"
127.0.0.1:6379> scan 1 count 3
1) "13"
2) 1) "key10"
   2) "key15"
   3) "key8"
   4) "key5"
127.0.0.1:6379> scan 13 count 3
1) "15"
2) 1) "key6"
   2) "key13"
   3) "key3"
127.0.0.1:6379> scan 15 count 3
1) "0"
2) 1) "key9"
   2) "key4"
127.0.0.1:6379> scan 0 match *1*
1) "13"
2) 1) "key11"
   2) "key12"
   3) "key14"
   4) "key1"
   5) "key10"
   6) "key15"
127.0.0.1:6379> scan 0 match *1?
1) "13"
2) 1) "key11"
   2) "key12"
   3) "key14"
   4) "key10"
   5) "key15"
127.0.0.1:6379> scan 0 match *1? count 2
1) "2"
2) 1) "key11"
   2) "key12"
127.0.0.1:6379> scan 2 match *1? count 2
1) "14"
2) 1) "key14"
127.0.0.1:6379> scan 14 match *1? count 2
1) "5"
2) 1) "key10"
127.0.0.1:6379> scan 5 match *1? count 2
1) "13"
2) 1) "key15"
127.0.0.1:6379> scan 13 match *1? count 2
1) "15"
2) 1) "key13"
127.0.0.1:6379> scan 15 match *1? count 2
1) "0"
2) (empty list or set)
127.0.0.1:6379>
```
