
-- 查询'是否工资统发'为空的数据条数
SELECT count(1) FROM bas_person_info t where t.is_uni_sala is null;

-- 将为空的 '是否工资统发' 全部修改为 2-否
UPDATE bas_person_info t set t.is_uni_sala = 2 where t.is_uni_sala is null;







-- 查询'工资关系所在单位'为空的数据条数
SELECT count(1) FROM bas_person_ext t where sala_agency_code is null;

-- 将为空的 '工资关系所在单位' 全部修改为 info 表的单位ID、编码、名称
UPDATE bas_person_ext t
   set (t.sala_agency_id, t.sala_agency_code, t.sala_agency_name) =
       (SELECT t2.agency_id, t2.agency_code, t2.agency_name
          FROM bas_person_info t2
         where t.per_id = t2.per_id)
 where t.sala_agency_code is null;
