
# 使用游标复制数据 - 将视图 id 为 13003 的字段数据复制并插入到 视图id 为 13005 的视图，主键使用 变量 i 赋值 #

	declare
	cursor vrows is select t.* from GAP_UI_COLUMN t where t.view_id = '13003' order by t.disp_order;
	p number := 660000;
	begin
	  for vrow in vrows loop-- 2、for 循环遍历游标，无需手动开关游标
		  insert into GAP_UI_COLUMN 
		(FIELD_CODE,NAME,DISP_ORDER,TITLE,DATA_TYPE,IS_VISIBLE,IS_ENABLED,DEFAULT_VALUE,COLUMN_WIDTH,COLUMN_HEIGHT,ALIGNMENT,DEC_LEN,ELE_CODE,ELE_SHOW_TYPE,ELE_LEVEL_NUM,ELE_CODE_PRIMARY,ELE_CODE_RELATED,IS_CALCULATED,IS_REQUIRED,EXPRESSION,PARENT_ID,ROW_SPAN,COL_SPAN,COL_LEVEL,MAX_LENGTH,MIN_LENGTH,IS_FROZEN,REMARK,IS_COLUMN_SORT,TIPS,IS_LOADELEMENT,JUMP_GROUP_ID,SWHERE,ELE_BOUND_VALUE,ELE_DISPLAY_VALUE,ROW_WRAP,ID,VIEW_ID)
		  select 
		  FIELD_CODE,NAME,DISP_ORDER,TITLE,DATA_TYPE,IS_VISIBLE,IS_ENABLED,DEFAULT_VALUE,COLUMN_WIDTH,COLUMN_HEIGHT,ALIGNMENT,DEC_LEN,ELE_CODE,ELE_SHOW_TYPE,ELE_LEVEL_NUM,ELE_CODE_PRIMARY,ELE_CODE_RELATED,IS_CALCULATED,IS_REQUIRED,EXPRESSION,PARENT_ID,ROW_SPAN,COL_SPAN,COL_LEVEL,MAX_LENGTH,MIN_LENGTH,IS_FROZEN,REMARK,IS_COLUMN_SORT,TIPS,IS_LOADELEMENT,JUMP_GROUP_ID,SWHERE,ELE_BOUND_VALUE,ELE_DISPLAY_VALUE,ROW_WRAP,p,'13005'       
		  from GAP_UI_COLUMN t2 where t2.id = vrow.id;
		  p := p + 1;
	  end loop; 
	end;


# 使用游标复制数据 - 将视图 id 为 13003 的字段数据复制并插入到 视图id 为 13005 的视图，主键在对方的基础上拼接上滚动的数字字符串 #

	declare
	cursor vrows is select t.* from GAP_UI_COLUMN t where t.view_id in('f8f3a8843cb34932a292179f39820771') order by t.disp_order;
	p number := 1;
	begin
	  for vrow in vrows loop-- 2、for 循环遍历游标，无需手动开关游标
		  insert into GAP_UI_COLUMN 
		(FIELD_CODE,NAME,DISP_ORDER,TITLE,DATA_TYPE,IS_VISIBLE,IS_ENABLED,DEFAULT_VALUE,COLUMN_WIDTH,COLUMN_HEIGHT,ALIGNMENT,DEC_LEN,ELE_CODE,ELE_SHOW_TYPE,ELE_LEVEL_NUM,ELE_CODE_PRIMARY,ELE_CODE_RELATED,IS_CALCULATED,IS_REQUIRED,EXPRESSION,PARENT_ID,ROW_SPAN,COL_SPAN,COL_LEVEL,MAX_LENGTH,MIN_LENGTH,IS_FROZEN,REMARK,JUMP_GROUP_ID,IS_LOADELEMENT,TIPS,IS_COLUMN_SORT,SWHERE,ELE_BOUND_VALUE,ELE_DISPLAY_VALUE,ROW_WRAP,ID,VIEW_ID)
		  select 
		  FIELD_CODE,NAME,DISP_ORDER,TITLE,DATA_TYPE,IS_VISIBLE,IS_ENABLED,DEFAULT_VALUE,COLUMN_WIDTH,COLUMN_HEIGHT,ALIGNMENT,DEC_LEN,ELE_CODE,ELE_SHOW_TYPE,ELE_LEVEL_NUM,ELE_CODE_PRIMARY,ELE_CODE_RELATED,IS_CALCULATED,IS_REQUIRED,EXPRESSION,PARENT_ID,ROW_SPAN,COL_SPAN,COL_LEVEL,MAX_LENGTH,MIN_LENGTH,IS_FROZEN,REMARK,IS_COLUMN_SORT,TIPS,IS_LOADELEMENT,JUMP_GROUP_ID,SWHERE,ELE_BOUND_VALUE,ELE_DISPLAY_VALUE,ROW_WRAP,
		  vrow.id || p,'4aa82fe616cb4b4099c0bf437cef5451'       
		  from GAP_UI_COLUMN t2 where t2.id = vrow.id;
		  p := p + 1;
	  end loop; 
	end;


# 使用游标复制数据 - 将视图 id 为 13004 的字段数据的顺序更新给 13408 视图的字段 #

	declare
	cursor vrows is select t.* from GAP_UI_COLUMN t where t.view_id = '13004' order by t.disp_order;
	begin
	  for vrow in vrows loop-- 2、for 循环遍历游标，无需手动开关游标
		  update GAP_UI_COLUMN t set t.disp_order=vrow.disp_order where t.view_id='13408' and t.name=vrow.name and t.field_code=vrow.field_code;
	  end loop; 
	end;


# 使用游标更新 - 将视图 id 为 13003 的字段的 disp_order 字段值使用 p 变量动态赋值 #

	declare
	cursor vrows is select t.* from GAP_UI_COLUMN t where t.view_id in('13003');
	p number := 1;
	begin
	  for vrow in vrows loop-- 2、for 循环遍历游标，无需手动开关游标
		 update GAP_UI_COLUMN t set t.disp_order = p where t.id = vrow.id;
		  p := p + 3;
	  end loop; 
	end;


# 使用游标更新 - 将视图 id 为 13007 的字段的 disp_order 字段值在原来得基础上放大 3 倍 #

	declare
	cursor vrows is select t.* from GAP_UI_COLUMN t where t.view_id in('13007');
	begin
	  for vrow in vrows loop-- 2、for 循环遍历游标，无需手动开关游标
		 update GAP_UI_COLUMN t set t.disp_order = t.disp_order * 3 where t.id = vrow.id;
	  end loop; 
	end;



# 预算 3.0 ：使用游标复制对象表 #

  declare
  cursor vrows is SELECT * FROM GAP_BORM_OBJATTR T WHERE T.OBJ_ID='14219';
  begin
    for vrow in vrows loop-- 2、for 循环遍历游标，无需手动开关游标
      insert into GAP_BORM_OBJATTR 
    (OBJ_ID, ATTR_CODE, ATTR_NAME, FIELD_NAME, ATTR_TYPE, ATTR_LENGTH, REF_OBJ_ID, REF_ATTR_CODE, IS_PK, ATTR_ID, SERVER_NAME)
      select 
      142191, ATTR_CODE, ATTR_NAME, FIELD_NAME, ATTR_TYPE, ATTR_LENGTH, REF_OBJ_ID, REF_ATTR_CODE, IS_PK, ATTR_ID || 110, SERVER_NAME       
      from GAP_BORM_OBJATTR t2 where t2.attr_id = vrow.attr_id;
    end loop; 
  end;

# 预算3.0：使用 PL/sql 复制字段，从经办岗赋值给审核岗 #

	  declare
	  cursor vrows is select t.* from GAP_UI_COLUMN t where t.view_id = '10980' order by t.disp_order;
	  p number := 10;
	  begin
	    for vrow in vrows loop-- 2、for 循环遍历游标，无需手动开关游标
	      insert into GAP_UI_COLUMN 	    (FIELD_CODE,NAME,DISP_ORDER,TITLE,DATA_TYPE,IS_VISIBLE,IS_ENABLED,DEFAULT_VALUE,COLUMN_WIDTH,COLUMN_HEIGHT,ALIGNMENT,DEC_LEN,ELE_CODE,ELE_SHOW_TYPE,ELE_LEVEL_NUM,ELE_CODE_PRIMARY,ELE_CODE_RELATED,IS_CALCULATED,IS_REQUIRED,EXPRESSION,PARENT_ID,ROW_SPAN,COL_SPAN,COL_LEVEL,MAX_LENGTH,MIN_LENGTH,IS_FROZEN,REMARK,JUMP_GROUP_ID,IS_LOADELEMENT,TIPS,IS_COLUMN_SORT,SWHERE,ELE_BOUND_VALUE,ELE_DISPLAY_VALUE,ROW_WRAP,ID,VIEW_ID)
	      select 	   FIELD_CODE,NAME,DISP_ORDER,TITLE,DATA_TYPE,IS_VISIBLE,IS_ENABLED,DEFAULT_VALUE,COLUMN_WIDTH,COLUMN_HEIGHT,ALIGNMENT,DEC_LEN,ELE_CODE,ELE_SHOW_TYPE,ELE_LEVEL_NUM,ELE_CODE_PRIMARY,ELE_CODE_RELATED,IS_CALCULATED,IS_REQUIRED,EXPRESSION,PARENT_ID,ROW_SPAN,COL_SPAN,COL_LEVEL,MAX_LENGTH,MIN_LENGTH,IS_FROZEN,REMARK,JUMP_GROUP_ID,IS_LOADELEMENT,TIPS,IS_COLUMN_SORT,SWHERE,ELE_BOUND_VALUE,ELE_DISPLAY_VALUE,ROW_WRAP,
	      'A3' || substr(ID,0,length(ID)-4) || p,'201214'       
	      from GAP_UI_COLUMN t2 where t2.id = vrow.id;
	      p := p + 1;
	    end loop; 
  		end;

# 预算3.0：批量更新指定视图组编码下的全部按钮为失效，不启用状态 #

update gap_ui_button t3
   set t3.is_enabled = 0
 where t3.view_id in
       (select t1.id
          from gap_ui_view t1
         where t1.group_id in
               (select t2.id
                  from GAP_UI_VIEW_GROUP t2
                 start with t2.code in ('208002')
                connect by prior t2.id = t2.parent_id));


# 预算3.0：发包脚本 # 

select t.*,rowid from gap_ui_module t where t.id in('28213208');-- 查询功能
select t.*,rowid from gap_module_parameter t where t.module_id in('28213208');-- 查询参数
select * from gap_role_module t where t.module_id in('28213208');-- 角色对功能
select * from gap_menu t where t.module_id in('28213208');-- 菜单对功能

-- 功能对视图组
select t.*,rowid from GAP_UI_VIEW_GROUP t start with t.code in(select t2.uiview_group_code from gap_ui_module t2 where t2.id in('28213208')) connect by prior t.id = t.parent_id;

-- 视图组下的视图
select t3.* from gap_ui_view t3 where t3.group_id in(
select t.id from GAP_UI_VIEW_GROUP t start with t.code in(select t2.uiview_group_code from gap_ui_module t2 where t2.id in('28213208')) connect by prior t.id = t.parent_id
);

-- 视图组关联视图
select * from Gap_Ui_View_Group_View t4 where t4.view_group_id in(select t.id from GAP_UI_VIEW_GROUP t start with t.code in(select t2.uiview_group_code from gap_ui_module t2 where t2.id in('28213208')) connect by prior t.id = t.parent_id);

-- 视图下的列
select * from gap_ui_column t4 where t4.view_id in(
select t3.id from gap_ui_view t3 where t3.group_id in(
select t.id from GAP_UI_VIEW_GROUP t start with t.code in(select t2.uiview_group_code from gap_ui_module t2 where t2.id in('28213208')) connect by prior t.id = t.parent_id
));

-- 视图下的按钮
select * from gap_ui_button t4 where t4.view_id in(
  select t3.id from gap_ui_view t3 where t3.group_id in(
    select t.id from GAP_UI_VIEW_GROUP t start with t.code in(select t2.uiview_group_code from gap_ui_module t2 where t2.id in('28213208')) connect by prior t.id = t.parent_id
));

-- 查询基础库中 parent_id=‘0’ 的数据，然后查询平台单位要素进行更新
declare
i number :=0;
cursor vrows is SELECT T.Parent_Id,t.ele_code,t.mof_div_code FROM ele_agency t where t.ele_id in(SELECT t2.biz_key FROM bas_agency_info t2 where nvl(t2.parent_id,'0') ='0');
begin
  for vrow in vrows loop
     update bas_agency_info t set t.parent_id = vrow.Parent_Id where t.agency_code = vrow.ele_code and t.mof_div_code = vrow.mof_div_code;
     dbms_output.put_line('更新【'|| vrow.mof_div_code || '-' || vrow.ele_code  ||' 】单位 parent_id= ' || vrow.Parent_Id); 
     i := i+1;
  end loop; 
   dbms_output.put_line('一共更新【'|| i ||' 】条数据'); 
end;
