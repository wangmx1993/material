Import started on 2023/4/13 19:55:54
-- Drop table
drop table EMP cascade constraints

Table dropped


-- Drop table
drop table EMP_LOG cascade constraints

Table dropped


create table EMP
(
  empno        NUMBER(4) not null,
  ename        VARCHAR2(10),
  job          VARCHAR2(9),
  mgr          NUMBER(4),
  hiredate     DATE,
  sal          NUMBER(7,2),
  comm         NUMBER(7,2),
  deptno       NUMBER(2),
  mof_div_code VARCHAR2(9),
  fiscal_year  VARCHAR2(4)
)

Table created

comment on column EMP.mof_div_code
  is '区划'

Comment added

comment on column EMP.fiscal_year
  is '年度'

Comment added

alter table EMP
  add constraint EMP_PK primary key (EMPNO)

Table altered

alter table EMP
  add constraint EMP_FK_DEPT foreign key (DEPTNO)
  references DEPT (DEPTNO)

Table altered

alter table EMP
  add constraint 部门编号不能大于50
  check (deptno between 0 and 50)

Table altered


create table EMP_LOG
(
  empno    NUMBER(4) not null,
  ename    VARCHAR2(10),
  job      VARCHAR2(9),
  mgr      NUMBER(4),
  hiredate DATE,
  sal      NUMBER(7,2),
  comm     NUMBER(7,2),
  deptno   NUMBER(2),
  f1       NUMBER(2),
  f12      NUMBER(2),
  f123     NUMBER(2),
  f1234    NUMBER(2),
  af12     NUMBER(2)
)

Table created


-- Disable triggers
alter table EMP disable all triggers

Table altered


-- Disable triggers
alter table EMP_LOG disable all triggers

Table altered


-- Disable foreign key constraints
alter table EMP disable constraint EMP_FK_DEPT

Table altered


Import table EMP
19 Records loaded, 0 errors

Import table EMP_LOG
14 Records loaded, 0 errors

-- Enable foreign key constraints
alter table EMP enable constraint EMP_FK_DEPT

Table altered


-- Enable triggers
alter table EMP enable all triggers

Table altered


-- Enable triggers
alter table EMP_LOG enable all triggers

Table altered


Import finished on 2023/4/13 19:56:01