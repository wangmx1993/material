
-- CREATE OR REPLACE PROCEDURE PROC_SYNC_NEW_YEAR_INIT_TAB is

declare
  v_sql             varchar2(1024); -- 自动拼接 DDL SQL 并执行
  p_from_table_name varchar2(64); -- 来源表，表名大写
  p_to_table_name   varchar2(64); -- 目标表，表名大写
  p_tab_is_exist    number(1); --来源表、目标表是否存在

begin
  --20230918: 自动同步新年度初始化操作备份表、临时表字段，防止初始化失败

  v_sql             := '';
  p_from_table_name := 'BAS_AGENCY_INFO'; --大写
  p_to_table_name   := 'BAS_AGENCY_INFO_FORMER'; --大写
  SELECT count(1) into p_tab_is_exist
    FROM user_tables T
   where t.TABLE_NAME = p_from_table_name or t.TABLE_NAME = p_to_table_name;
  if p_tab_is_exist > 1 then
    for vrow in (SELECT t1.COLUMN_NAME,
                        t1.DATA_TYPE,
                        t1.DATA_LENGTH,
                        t1.DATA_PRECISION,
                        t1.DATA_SCALE
                   FROM user_tab_columns t1
                  where TABLE_NAME = p_from_table_name
                    and not exists
                  (SELECT 1
                           FROM user_tab_columns t2
                          where TABLE_NAME = p_to_table_name
                            and t2.COLUMN_NAME = t1.column_name)) loop

      v_sql := 'alter table ' || p_to_table_name || ' add ' ||
               vrow.COLUMN_NAME || ' ' || vrow.data_type;
      if vrow.data_type = 'NUMBER' then
        if vrow.data_precision is not null then
          if vrow.data_scale > 0 then
            v_sql := v_sql || '(' || vrow.data_precision || ',' ||
                     vrow.data_scale || ')';
          else
            v_sql := v_sql || '(' || to_char(vrow.data_precision) || ')';
          end if;
        else
          v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
        end if;
      elsif vrow.data_type in ('CHAR', 'VARCHAR2', 'NVARCHAR2', 'CLOB') and
            vrow.DATA_LENGTH is not null then
        v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
      end if;
      -- execute immediate (v_sql);
      dbms_output.put_line(v_sql);
    end loop;
  end if;


  v_sql := '';
  p_from_table_name := 'BAS_AGENCY_EXT';
  p_to_table_name := 'BAS_AGENCY_EXT_FORMER';
  SELECT count(1) into p_tab_is_exist
    FROM user_tables T
   where t.TABLE_NAME in (p_from_table_name, p_to_table_name);
  if p_tab_is_exist > 1 then
    for vrow in (SELECT t1.COLUMN_NAME,
                        t1.DATA_TYPE,
                        t1.DATA_LENGTH,
                        t1.DATA_PRECISION,
                        t1.DATA_SCALE
                   FROM user_tab_columns t1
                  where TABLE_NAME = p_from_table_name
                    and not exists
                  (SELECT 1
                           FROM user_tab_columns t2
                          where TABLE_NAME = p_to_table_name
                            and t2.COLUMN_NAME = t1.column_name)) loop

      v_sql := 'alter table ' || p_to_table_name || ' add ' ||
               vrow.COLUMN_NAME || ' ' || vrow.data_type;
      if vrow.data_type = 'NUMBER' then
        if vrow.data_precision is not null then
          if vrow.data_scale > 0 then
            v_sql := v_sql || '(' || vrow.data_precision || ',' ||
                     vrow.data_scale || ')';
          else
            v_sql := v_sql || '(' || to_char(vrow.data_precision) || ')';
          end if;
        else
          v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
        end if;
      elsif vrow.data_type in ('CHAR', 'VARCHAR2', 'NVARCHAR2', 'CLOB') and
            vrow.DATA_LENGTH is not null then
        v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
      end if;
      -- execute immediate (v_sql);
      dbms_output.put_line(v_sql);
    end loop;
  end if;

  v_sql             := '';
  p_from_table_name := 'BAS_PERSON_INFO';
  p_to_table_name   := 'BAS_PERSON_INFO_FORMER';
  SELECT count(1) into p_tab_is_exist
    FROM user_tables T
   where t.TABLE_NAME in (p_from_table_name, p_to_table_name);
  if p_tab_is_exist > 1 then
    for vrow in (SELECT t1.COLUMN_NAME,
                        t1.DATA_TYPE,
                        t1.DATA_LENGTH,
                        t1.DATA_PRECISION,
                        t1.DATA_SCALE
                   FROM user_tab_columns t1
                  where TABLE_NAME = p_from_table_name
                    and not exists
                  (SELECT 1
                           FROM user_tab_columns t2
                          where TABLE_NAME = p_to_table_name
                            and t2.COLUMN_NAME = t1.column_name)) loop

      v_sql := 'alter table ' || p_to_table_name || ' add ' ||
               vrow.COLUMN_NAME || ' ' || vrow.data_type;
      if vrow.data_type = 'NUMBER' then
        if vrow.data_precision is not null then
          if vrow.data_scale > 0 then
            v_sql := v_sql || '(' || vrow.data_precision || ',' ||
                     vrow.data_scale || ')';
          else
            v_sql := v_sql || '(' || to_char(vrow.data_precision) || ')';
          end if;
        else
          v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
        end if;
      elsif vrow.data_type in ('CHAR', 'VARCHAR2', 'NVARCHAR2', 'CLOB') and
            vrow.DATA_LENGTH is not null then
        v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
      end if;
      -- execute immediate (v_sql);
      dbms_output.put_line(v_sql);
    end loop;
  end if;

  v_sql             := '';
  p_from_table_name := 'BAS_PERSON_EXT';
  p_to_table_name   := 'BAS_PERSON_EXT_FORMER';
  SELECT count(1) into p_tab_is_exist
    FROM user_tables T
   where t.TABLE_NAME in (p_from_table_name, p_to_table_name);
  if p_tab_is_exist > 1 then
    for vrow in (SELECT t1.COLUMN_NAME,
                        t1.DATA_TYPE,
                        t1.DATA_LENGTH,
                        t1.DATA_PRECISION,
                        t1.DATA_SCALE
                   FROM user_tab_columns t1
                  where TABLE_NAME = p_from_table_name
                    and not exists
                  (SELECT 1
                           FROM user_tab_columns t2
                          where TABLE_NAME = p_to_table_name
                            and t2.COLUMN_NAME = t1.column_name)) loop

      v_sql := 'alter table ' || p_to_table_name || ' add ' ||
               vrow.COLUMN_NAME || ' ' || vrow.data_type;
      if vrow.data_type = 'NUMBER' then
        if vrow.data_precision is not null then
          if vrow.data_scale > 0 then
            v_sql := v_sql || '(' || vrow.data_precision || ',' ||
                     vrow.data_scale || ')';
          else
            v_sql := v_sql || '(' || to_char(vrow.data_precision) || ')';
          end if;
        else
          v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
        end if;
      elsif vrow.data_type in ('CHAR', 'VARCHAR2', 'NVARCHAR2', 'CLOB') and
            vrow.DATA_LENGTH is not null then
        v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
      end if;
      -- execute immediate (v_sql);
      dbms_output.put_line(v_sql);
    end loop;
  end if;

  v_sql             := '';
  p_from_table_name := 'BAS_PERSON_ASSIST_INFO';
  p_to_table_name   := 'BAS_PERSON_ASSIST_INFO_FORMER';
  SELECT count(1) into p_tab_is_exist
    FROM user_tables T
   where t.TABLE_NAME in (p_from_table_name, p_to_table_name);
  if p_tab_is_exist > 1 then
    for vrow in (SELECT t1.COLUMN_NAME,
                        t1.DATA_TYPE,
                        t1.DATA_LENGTH,
                        t1.DATA_PRECISION,
                        t1.DATA_SCALE
                   FROM user_tab_columns t1
                  where TABLE_NAME = p_from_table_name
                    and not exists
                  (SELECT 1
                           FROM user_tab_columns t2
                          where TABLE_NAME = p_to_table_name
                            and t2.COLUMN_NAME = t1.column_name)) loop

      v_sql := 'alter table ' || p_to_table_name || ' add ' ||
               vrow.COLUMN_NAME || ' ' || vrow.data_type;
      if vrow.data_type = 'NUMBER' then
        if vrow.data_precision is not null then
          if vrow.data_scale > 0 then
            v_sql := v_sql || '(' || vrow.data_precision || ',' ||
                     vrow.data_scale || ')';
          else
            v_sql := v_sql || '(' || to_char(vrow.data_precision) || ')';
          end if;
        else
          v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
        end if;
      elsif vrow.data_type in ('CHAR', 'VARCHAR2', 'NVARCHAR2', 'CLOB') and
            vrow.DATA_LENGTH is not null then
        v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
      end if;
      -- execute immediate (v_sql);
      dbms_output.put_line(v_sql);
    end loop;
  end if;

  v_sql             := '';
  p_from_table_name := 'BAS_AGENCY_INFO';
  p_to_table_name   := 'BAS_AGENCY_INFO_INIT_TMP';
  SELECT count(1) into p_tab_is_exist
    FROM user_tables T
   where t.TABLE_NAME in (p_from_table_name, p_to_table_name);
  if p_tab_is_exist > 1 then
    for vrow in (SELECT t1.COLUMN_NAME,
                        t1.DATA_TYPE,
                        t1.DATA_LENGTH,
                        t1.DATA_PRECISION,
                        t1.DATA_SCALE
                   FROM user_tab_columns t1
                  where TABLE_NAME = p_from_table_name
                    and not exists
                  (SELECT 1
                           FROM user_tab_columns t2
                          where TABLE_NAME = p_to_table_name
                            and t2.COLUMN_NAME = t1.column_name)) loop

      v_sql := 'alter table ' || p_to_table_name || ' add ' ||
               vrow.COLUMN_NAME || ' ' || vrow.data_type;
      if vrow.data_type = 'NUMBER' then
        if vrow.data_precision is not null then
          if vrow.data_scale > 0 then
            v_sql := v_sql || '(' || vrow.data_precision || ',' ||
                     vrow.data_scale || ')';
          else
            v_sql := v_sql || '(' || to_char(vrow.data_precision) || ')';
          end if;
        else
          v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
        end if;
      elsif vrow.data_type in ('CHAR', 'VARCHAR2', 'NVARCHAR2', 'CLOB') and
            vrow.DATA_LENGTH is not null then
        v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
      end if;
      -- execute immediate (v_sql);
      dbms_output.put_line(v_sql);
    end loop;
  end if;

  v_sql             := '';
  p_from_table_name := 'BAS_AGENCY_EXT';
  p_to_table_name   := 'BAS_AGENCY_EXT_INIT_TMP';
  SELECT count(1) into p_tab_is_exist
    FROM user_tables T
   where t.TABLE_NAME in (p_from_table_name, p_to_table_name);
  if p_tab_is_exist > 1 then
    for vrow in (SELECT t1.COLUMN_NAME,
                        t1.DATA_TYPE,
                        t1.DATA_LENGTH,
                        t1.DATA_PRECISION,
                        t1.DATA_SCALE
                   FROM user_tab_columns t1
                  where TABLE_NAME = p_from_table_name
                    and not exists
                  (SELECT 1
                           FROM user_tab_columns t2
                          where TABLE_NAME = p_to_table_name
                            and t2.COLUMN_NAME = t1.column_name)) loop

      v_sql := 'alter table ' || p_to_table_name || ' add ' ||
               vrow.COLUMN_NAME || ' ' || vrow.data_type;
      if vrow.data_type = 'NUMBER' then
        if vrow.data_precision is not null then
          if vrow.data_scale > 0 then
            v_sql := v_sql || '(' || vrow.data_precision || ',' ||
                     vrow.data_scale || ')';
          else
            v_sql := v_sql || '(' || to_char(vrow.data_precision) || ')';
          end if;
        else
          v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
        end if;
      elsif vrow.data_type in ('CHAR', 'VARCHAR2', 'NVARCHAR2', 'CLOB') and
            vrow.DATA_LENGTH is not null then
        v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
      end if;
      -- execute immediate (v_sql);
      dbms_output.put_line(v_sql);
    end loop;
  end if;

  v_sql             := '';
  p_from_table_name := 'BAS_PERSON_INFO';
  p_to_table_name   := 'BAS_PERSON_INFO_INIT_TMP';
  SELECT count(1) into p_tab_is_exist
    FROM user_tables T
   where t.TABLE_NAME in (p_from_table_name, p_to_table_name);
  if p_tab_is_exist > 1 then
    for vrow in (SELECT t1.COLUMN_NAME,
                        t1.DATA_TYPE,
                        t1.DATA_LENGTH,
                        t1.DATA_PRECISION,
                        t1.DATA_SCALE
                   FROM user_tab_columns t1
                  where TABLE_NAME = p_from_table_name
                    and not exists
                  (SELECT 1
                           FROM user_tab_columns t2
                          where TABLE_NAME = p_to_table_name
                            and t2.COLUMN_NAME = t1.column_name)) loop

      v_sql := 'alter table ' || p_to_table_name || ' add ' ||
               vrow.COLUMN_NAME || ' ' || vrow.data_type;
      if vrow.data_type = 'NUMBER' then
        if vrow.data_precision is not null then
          if vrow.data_scale > 0 then
            v_sql := v_sql || '(' || vrow.data_precision || ',' ||
                     vrow.data_scale || ')';
          else
            v_sql := v_sql || '(' || to_char(vrow.data_precision) || ')';
          end if;
        else
          v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
        end if;
      elsif vrow.data_type in ('CHAR', 'VARCHAR2', 'NVARCHAR2', 'CLOB') and
            vrow.DATA_LENGTH is not null then
        v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
      end if;
      -- execute immediate (v_sql);
      dbms_output.put_line(v_sql);
    end loop;
  end if;

  v_sql             := '';
  p_from_table_name := 'BAS_PERSON_EXT';
  p_to_table_name   := 'BAS_PERSON_EXT_INIT_TMP';
  SELECT count(1) into p_tab_is_exist
    FROM user_tables T
   where t.TABLE_NAME in (p_from_table_name, p_to_table_name);
  if p_tab_is_exist > 1 then
    for vrow in (SELECT t1.COLUMN_NAME,
                        t1.DATA_TYPE,
                        t1.DATA_LENGTH,
                        t1.DATA_PRECISION,
                        t1.DATA_SCALE
                   FROM user_tab_columns t1
                  where TABLE_NAME = p_from_table_name
                    and not exists
                  (SELECT 1
                           FROM user_tab_columns t2
                          where TABLE_NAME = p_to_table_name
                            and t2.COLUMN_NAME = t1.column_name)) loop

      v_sql := 'alter table ' || p_to_table_name || ' add ' ||
               vrow.COLUMN_NAME || ' ' || vrow.data_type;
      if vrow.data_type = 'NUMBER' then
        if vrow.data_precision is not null then
          if vrow.data_scale > 0 then
            v_sql := v_sql || '(' || vrow.data_precision || ',' ||
                     vrow.data_scale || ')';
          else
            v_sql := v_sql || '(' || to_char(vrow.data_precision) || ')';
          end if;
        else
          v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
        end if;
      elsif vrow.data_type in ('CHAR', 'VARCHAR2', 'NVARCHAR2', 'CLOB') and
            vrow.DATA_LENGTH is not null then
        v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
      end if;
      -- execute immediate (v_sql);
      dbms_output.put_line(v_sql);
    end loop;
  end if;

  v_sql             := '';
  p_from_table_name := 'BAS_PERSON_ASSIST_INFO';
  p_to_table_name   := 'BAS_P_ASSIST_INFO_INIT_TMP';
  SELECT count(1) into p_tab_is_exist
    FROM user_tables T
   where t.TABLE_NAME in (p_from_table_name, p_to_table_name);
  if p_tab_is_exist > 1 then
    for vrow in (SELECT t1.COLUMN_NAME,
                        t1.DATA_TYPE,
                        t1.DATA_LENGTH,
                        t1.DATA_PRECISION,
                        t1.DATA_SCALE
                   FROM user_tab_columns t1
                  where TABLE_NAME = p_from_table_name
                    and not exists
                  (SELECT 1
                           FROM user_tab_columns t2
                          where TABLE_NAME = p_to_table_name
                            and t2.COLUMN_NAME = t1.column_name)) loop

      v_sql := 'alter table ' || p_to_table_name || ' add ' ||
               vrow.COLUMN_NAME || ' ' || vrow.data_type;
      if vrow.data_type = 'NUMBER' then
        if vrow.data_precision is not null then
          if vrow.data_scale > 0 then
            v_sql := v_sql || '(' || vrow.data_precision || ',' ||
                     vrow.data_scale || ')';
          else
            v_sql := v_sql || '(' || to_char(vrow.data_precision) || ')';
          end if;
        else
          v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
        end if;
      elsif vrow.data_type in ('CHAR', 'VARCHAR2', 'NVARCHAR2', 'CLOB') and
            vrow.DATA_LENGTH is not null then
        v_sql := v_sql || '(' || vrow.DATA_LENGTH || ')';
      end if;
      -- execute immediate (v_sql);
      dbms_output.put_line(v_sql);
    end loop;
  end if;

end;
