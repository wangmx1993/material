
-- 1、查询基础中父节点ID为空或者为0的单位信息
select b.mof_div_code, b.agency_code, b.agency_name, b.parent_id
  from bas_agency_info b
 where b.is_leaf = 1 -- 末级是单位
   and b.is_enabled = 1 -- 有效的
   and b.is_deleted = 2 -- 未删除的
   and nvl(b.parent_id, '0') = '0'
   and exists (select 1
          from ele_agency e
         where b.agency_code = e.ele_code
           and b.mof_div_code = e.mof_div_code
           and sysdate between e.start_date and e.end_date --未失效的
           and e.is_deleted = 2 -- 未删除的
           and e.is_leaf = 1 --叶子节点是单位
           and nvl(e.parent_id, '0') != '0' --父节点不为空的
        );


-- 2、查询基础库中存在但是平台不存在的单位信息
select b.mof_div_code, b.agency_code, b.agency_name, b.parent_id, b.biz_key
  from bas_agency_info b
 where b.is_leaf = 1 -- 末级是单位
   and b.is_enabled = 1 -- 有效的
   and b.is_deleted = 2 -- 未删除的
   and not exists (select 1
          from ele_agency e
         where b.agency_code = e.ele_code
           and b.mof_div_code = e.mof_div_code);


-- 3、查询基础库和平台也存在，但是平台已经失效的单位信息
select b.mof_div_code, b.agency_code, b.agency_name, b.parent_id, b.biz_key
  from bas_agency_info b
 where b.is_leaf = 1 -- 末级是单位
   and b.is_enabled = 1 -- 有效的
   and b.is_deleted = 2 -- 未删除的
   and exists (select 1
          from ele_agency e
         where b.agency_code = e.ele_code
           and b.mof_div_code = e.mof_div_code
           and (e.is_deleted = 1 or sysdate < e.start_date or sysdate > e.end_date));


-- 4、查询基础库中已维护的父节点ID与平台维护的父节点ID不一致的单位信息
select b.mof_div_code, b.agency_code, b.agency_name, b.parent_id
  from bas_agency_info b
 where b.is_leaf = 1 -- 末级是单位
   and b.is_enabled = 1 -- 有效的
   and b.is_deleted = 2 -- 未删除的
   and nvl(b.parent_id,'0') != '0' --父节点不为空的
   and exists (select 1
          from ele_agency e
         where b.agency_code = e.ele_code
           and b.mof_div_code = e.mof_div_code
           and sysdate between e.start_date and e.end_date --未失效的
           and e.is_deleted = 2 -- 未删除的
           and e.is_leaf = 1 --叶子节点是单位
           and nvl(e.parent_id, '0') != '0'
           and e.parent_id != b.parent_id
        );


-- 5、查询基础库中已维护的 biz_key(平台单位ID) 与平台维护的单位ID不一致的单位信息
select b.biz_key, b.mof_div_code, b.agency_code, b.agency_name
  from bas_agency_info b
 where b.is_leaf = 1 -- 末级是单位
   and b.is_enabled = 1 -- 有效的
   and b.is_deleted = 2 -- 未删除的
   and exists (select 1
          from ele_agency e
         where b.agency_code = e.ele_code
           and b.mof_div_code = e.mof_div_code
           and sysdate between e.start_date and e.end_date --未失效的
           and e.is_deleted = 2 -- 未删除的
           and e.is_leaf = 1
           and b.biz_key != e.ele_id);


-- 6、查询基础库中单位名称与平台不一致的单位信息
select b.mof_div_code, b.agency_code, b.agency_name, b.biz_key
  from bas_agency_info b
 where b.is_leaf = 1 -- 末级是单位
   and b.is_enabled = 1 -- 有效的
   and b.is_deleted = 2 -- 未删除的
   and exists (select 1
          from ele_agency e
         where b.agency_code = e.ele_code
           and b.mof_div_code = e.mof_div_code
           and sysdate between e.start_date and e.end_date --未失效的
           and e.is_deleted = 2 -- 未删除的
           and e.is_leaf = 1 --叶子节点是单位
           and b.agency_name != e.ele_name);


-- 7、查询基础库人员信息表中单位名称与平台不一致的人员信息
select distinct b.mof_div_code, b.agency_code, b.agency_name
  from bas_person_info b
 where exists (select 1
          from ele_agency e
         where b.agency_code = e.ele_code
           and b.mof_div_code = e.mof_div_code
           and sysdate between e.start_date and e.end_date --未失效的
           and e.is_deleted = 2 -- 未删除的
           and e.is_leaf = 1 --叶子节点是单位
           and b.agency_name != e.ele_name);


-- 8、查询基础库单位信息中 biz_key 重复的单位.
select biz_key
  from bas_agency_info t
 where t.is_deleted = 2
 group by t.biz_key
having count(1) > 1






