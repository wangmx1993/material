--  Table structure for region
DROP TABLE IF EXISTS region;
CREATE TABLE region (
  id int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  name varchar(56) COLLATE utf8_bin NOT NULL COMMENT '显示名称',
  pId int(11) NOT NULL COMMENT '父节点id',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
 
-- 添加1级菜单
insert into region(id, name, pId) values(1, '湖南省',0);-- 顶级菜单没有父菜单，所以 pId 为 0
insert into region(id, name, pId) values(2, '广东省',0);
 
-- 添加2级菜单
insert into region(id, name, pId) values(3, '长沙市',1);
insert into region(id, name, pId) values(4, '娄底市',1);
insert into region(id, name, pId) values(5, '深圳市',2);
 
-- 添加3级菜单
insert into region(id, name, pId) values(6, '芙蓉区',3);
insert into region(id, name, pId) values(7, '天心区',3);
insert into region(id, name, pId) values(8, '岳麓区',3);
insert into region(id, name, pId) values(9, '开福区',3);
insert into region(id, name, pId) values(10, '雨花区',3);
insert into region(id, name, pId) values(11, '望城区',3);
 
insert into region(id, name, pId) values(12, '娄星区',4);   
insert into region(id, name, pId) values(13, '冷水江市',4); 
insert into region(id, name, pId) values(14, '涟源市',4); 
insert into region(id, name, pId) values(15, '双峰县',4);
insert into region(id, name, pId) values(16, '新化县',4);
 
insert into region(id, name, pId) values(17, '罗湖区',5);      
insert into region(id, name, pId) values(18, '福田区',5);
insert into region(id, name, pId) values(19, '南山区',5);
insert into region(id, name, pId) values(20, '宝安区',5);
insert into region(id, name, pId) values(21, '坪山区',5);
insert into region(id, name, pId) values(22, '龙岗区',5);  
 
-- 添加4级菜单 
insert into region(id, name, pId) values(23, '白溪镇',16);  
insert into region(id, name, pId) values(24, '洋溪镇',16);  
insert into region(id, name, pId) values(25, '吉庆镇',16);  
insert into region(id, name, pId) values(26, '曹家镇',16);  