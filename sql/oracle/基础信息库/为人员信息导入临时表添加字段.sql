
 -- 为人员信息导入临时表添加字段============只需要修改变量：add_column_name、add_column_type 的值即可
 declare
   cursor vrows is
   -- 查询人员信息所有的导入临时表，格式如：BAS_PERSON_IMP_430000000_202005
     SELECT T.TABLE_NAME
       FROM user_tables t
      where t.TABLE_NAME like 'BAS_PERSON_IMP_%'
        and length(t.table_name) > 25
        and instr(t.TABLE_NAME, '_', 1, 4) > 0;
   add_column_name  varchar2(64) := 'IS_GUARANTEE2'; -- 待添加字段的列名
   add_column_type  varchar2(64) := 'varchar2(55)'; -- 待添加字段的类型
   add_column_exist number := 0; -- 待添加的列目标表中是否已经存在
   execute_sql      varchar2(1024) := ''; -- 自动拼接执行 SQL，如：Alter Table xxx Add xxx NUMBER(1);
   table_total      number := 0; --临时表总个数
   execute_total    number := 0; --添加成功的个数
 begin
   for vrow in vrows loop
     -- 如果表中已经存在待添加的列，则不再重复添加
     SELECT count(1) into add_column_exist FROM user_tab_columns t where t.TABLE_NAME = vrow.TABLE_NAME and t.COLUMN_NAME = add_column_name;
     if add_column_exist <= 0 then
       -- 拼接 SQL
       execute_sql := 'ALTER TABLE ' || vrow.TABLE_NAME || ' ADD ' ||
                      add_column_name || ' ' || add_column_type;
       -- 执行 SQL
       execute immediate execute_sql;
       execute_total := execute_total + 1;
     end if;
   table_total := table_total +1;
   end loop;
   dbms_output.put_line('临时表总个数为 ' || table_total || ' 张，其中：');
   dbms_output.put_line('  ' || execute_total || ' 张添加成功：' || add_column_name || ' ' || add_column_type);
   dbms_output.put_line('  ' || (table_total - execute_total) ||  ' 张已经存在：' || add_column_name || ' ' || add_column_type);
 end;