
--查询日常动态维护版本在目标年度有单位基本信息，却没有扩展信息的单位
--年度和版本可以注释掉，此时全表查询
SELECT t1.mof_div_code,
       t1.agency_code,
       t1.agency_id,
       t1.biz_key,
       t1.version,
       t1.fiscal_year,
       t1.create_time,
       t1.create_user_name,
       t1.update_time,
       t1.update_user_name
  FROM bas_agency_info t1
 where t1.fiscal_year = '2024'
   and version = '2024rcdtwh'
   and t1.is_deleted = 2
   and not exists
 (SELECT 1 FROM bas_agency_ext t2 where t2.agency_id = t1.agency_id);

--处理日常动态维护版本在目标年度有单位基本信息，却没有扩展信息的单位，为它们生成单位扩展信息
--年度和版本可以注释掉，此时全表修正
MERGE INTO bas_agency_ext ext
USING (SELECT t1.AGENCY_ID,
              t1.BIZ_KEY,
              t1.CREATE_TIME,
              t1.CREATE_USER_CODE,
              t1.CREATE_USER_ID,
              t1.CREATE_USER_NAME,
              t1.END_DATE,
              t1.FISCAL_YEAR,
              t1.IS_DELETED,
              t1.IS_ENABLED,
              t1.IS_LAST_INST,
              t1.MOF_DIV_CODE,
              t1.MOF_DIV_NAME,
              t1.START_DATE,
              t1.UPDATE_TIME,
              t1.UPDATE_USER_CODE,
              t1.UPDATE_USER_ID,
              t1.UPDATE_USER_NAME,
              t1.VERSION,
              t1.VERSION_NAME
         FROM bas_agency_info t1
        where t1.fiscal_year = '2024'
          and version = '2024rcdtwh'
          and t1.is_deleted = 2
          and not exists (SELECT 1
                 FROM bas_agency_ext t2
                where t2.agency_id = t1.agency_id)) info
ON (ext.agency_id = info.agency_id)
WHEN NOT MATCHED THEN
--特别提醒1：扩展表的字段很多，下面是部标要求的不允许为空的字段，所以暂时只生成它们。
-----如果还有遗落的必录项这里没有写，可以自行加上，INSERT 与 VALUES 后面同步加上就行。
--特别提醒2：insert 后面是需要给 ext 表赋值的字段名称
  INSERT
    (ext.ACTUNOW_STAF_NUM,--实有在编人数
     ext.ACTU_STAF_NUM,--实有人数
     ext.AGENCY_EXT_ID,--与AGENCY_ID相同
     ext.AGENCY_ID,
     ext.BIZ_KEY,
     ext.CREATE_TIME,
     ext.CREATE_USER_CODE,
     ext.CREATE_USER_ID,
     ext.CREATE_USER_NAME,
     ext.END_DATE,
     ext.FISCAL_YEAR,
     ext.IS_DELETED,
     ext.IS_ENABLED,
     ext.IS_LAST_INST,
     ext.MOF_DIV_CODE,
     ext.MOF_DIV_NAME,
     ext.START_DATE,
     ext.UPDATE_TIME,
     ext.UPDATE_USER_CODE,
     ext.UPDATE_USER_ID,
     ext.UPDATE_USER_NAME,
     ext.VERSION,
     ext.VERSION_NAME)
  VALUES
--特别提醒3：给上面的字段进行赋值，与上面 insert 后面的字段一样对应，info表有的，直接从info表取，没有的，手动给一个默认值
--特别提醒4：假如单位扩展表有上一年的数据，如果有需要，可以从上一年复制业务数据过来，这里暂时不操作了。
    (0,
     0,
     info.AGENCY_ID,
     info.AGENCY_ID,
     info.BIZ_KEY,
     info.CREATE_TIME,
     info.CREATE_USER_CODE,
     info.CREATE_USER_ID,
     info.CREATE_USER_NAME,
     info.END_DATE,
     info.FISCAL_YEAR,
     info.IS_DELETED,
     info.IS_ENABLED,
     info.IS_LAST_INST,
     info.MOF_DIV_CODE,
     info.MOF_DIV_NAME,
     info.START_DATE,
     info.UPDATE_TIME,
     info.UPDATE_USER_CODE,
     info.UPDATE_USER_ID,
     info.UPDATE_USER_NAME,
     info.VERSION,
     info.VERSION_NAME);



--====================【河南】=个性化单位扩展表===============./start============
MERGE INTO BAS_AGENCY_EXT_ASSET T1
    USING (SELECT t.agency_id,
                  t.biz_key,
                  t.create_time,
                  t.create_user_code,
                  t.create_user_id,
                  t.create_user_name,
                  t.end_date,
                  t.fiscal_year,
                  t.is_deleted,
                  t.is_enabled,
                  t.is_last_inst,
                  t.mof_div_code,
                  t.start_date,
                  t.update_time,
                  t.update_user_code,
                  t.update_user_id,
                  t.update_user_name,
                  t.version,
                  t.version_name
           FROM bas_agency_info t
           WHERE t.version IN
                 ('2022rcdtwh', '2023rcdtwh', '2024rcdtwh', '2025rcdtwh')
             AND t.is_deleted = 2
             AND NOT EXISTS (SELECT 1
                             FROM BAS_AGENCY_EXT_ASSET t2
                             WHERE t.agency_id = t2.agency_id)) INFO
    ON (T1.agency_id = INFO.agency_id)
    WHEN NOT MATCHED THEN
        INSERT
            (T1.agency_id,
             T1.biz_key,
             T1.create_time,
             T1.create_user_code,
             T1.create_user_id,
             T1.create_user_name,
             T1.end_date,
             T1.fiscal_year,
             T1.is_deleted,
             T1.is_enabled,
             T1.is_last_inst,
             T1.mof_div_code,
             T1.start_date,
             T1.update_time,
             T1.update_user_code,
             T1.update_user_id,
             T1.update_user_name,
             T1.version,
             T1.version_name)
            VALUES
                (INFO.agency_id,
                 INFO.biz_key,
                 INFO.create_time,
                 INFO.create_user_code,
                 INFO.create_user_id,
                 INFO.Create_User_Name,
                 INFO.end_date,
                 INFO.fiscal_year,
                 INFO.is_deleted,
                 INFO.is_enabled,
                 INFO.is_last_inst,
                 INFO.mof_div_code,
                 INFO.start_date,
                 SYSDATE,
                 INFO.update_user_code,
                 INFO.update_user_id,
                 '手动补充扩展表数据',
                 INFO.version,
                 INFO.version_name);


MERGE INTO BAS_AGENCY_EXT_STU T1
    USING (SELECT t.agency_id,
                  t.agency_code,
                  t.agency_name,
                  t.biz_key,
                  t.create_time,
                  t.create_user_code,
                  t.create_user_id,
                  t.create_user_name,
                  t.end_date,
                  t.is_deleted,
                  t.is_enabled,
                  t.is_last_inst,
                  t.mof_div_code,
                  t.start_date,
                  t.update_time,
                  t.update_user_code,
                  t.update_user_id,
                  t.update_user_name,
                  t.version,
                  t.version_name
           FROM bas_agency_info t
           WHERE t.version IN
                 ('2022rcdtwh', '2023rcdtwh', '2024rcdtwh', '2025rcdtwh')
             AND t.is_deleted = 2
             AND NOT EXISTS (SELECT 1
                             FROM BAS_AGENCY_EXT_STU t2
                             WHERE t.agency_id = t2.agency_id)) INFO
    ON (T1.agency_id = INFO.agency_id)
    WHEN NOT MATCHED THEN
        INSERT
            (T1.agency_id,
             t1.agency_code,
             t1.agency_name,
             T1.biz_key,
             T1.create_time,
             T1.create_user_code,
             T1.create_user_id,
             T1.create_user_name,
             T1.end_date,
             T1.is_deleted,
             T1.is_enabled,
             T1.is_last_inst,
             T1.mof_div_code,
             T1.start_date,
             T1.update_time,
             T1.update_user_code,
             T1.update_user_id,
             T1.update_user_name,
             T1.version,
             T1.version_name)
            VALUES
                (INFO.agency_id,
                 INFO.AGENCY_CODE,
                 INFO.AGENCY_NAME,
                 INFO.biz_key,
                 INFO.create_time,
                 INFO.create_user_code,
                 INFO.create_user_id,
                 INFO.Create_User_Name,
                 INFO.end_date,
                 INFO.is_deleted,
                 INFO.is_enabled,
                 INFO.is_last_inst,
                 INFO.mof_div_code,
                 INFO.start_date,
                 SYSDATE,
                 INFO.update_user_code,
                 INFO.update_user_id,
                 '手动补充扩展表数据',
                 INFO.version,
                 INFO.version_name);
--====================【河南】=个性化单位扩展表===============./end============
