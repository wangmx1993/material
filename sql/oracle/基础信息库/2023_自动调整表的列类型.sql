CREATE OR REPLACE PROCEDURE PROC_CHANGE_COLUMN_TYPE_WMX(p_table_name     in varchar2, --需要调整的表名,如 emp
                                                        p_column_name    in varchar2, --需要调整的列名，如 job
                                                        p_from_data_type in varchar2, --调整前的数据类型,如 number(18,4)
                                                        p_to_data_type   in varchar2, --调整后的数据类型，如 varchar2(38)
                                                        --是否已自动执行脚本，当且仅当为1时已自动执行
                                                        --不已自动执行时，手动从输出的日志中复制脚本即可.
                                                        p_is_auto_run in number)
--自定义函数：调整表[字段类型]/[列类型]，暂时不支持日期类，用于 number 与 varchar2 的转换
 is
  v_sql              varchar2(2048); --动态执行的sql
  v_temp_suffix      varchar2(16) := '_back'; --临时字段的后缀
  v_temp_column_name varchar2(64); --临时字段名称
  v_nullable         char(1) := 'Y'; -- 是否允许为空
begin

  if p_is_auto_run = 1 then
    dbms_output.put_line('-- ==============自动执行==============');
  else
    dbms_output.put_line('-- ==============手动执行==============');
  end if;

  --得到临时字段名称
  SELECT trim(p_column_name) || trim(v_temp_suffix) into v_temp_column_name FROM dual T;

  -- 查询字段是否允许为空
  SELECT t.NULLABLE into v_nullable FROM user_tab_columns t where t.TABLE_NAME = upper(p_table_name) and t.COLUMN_NAME = upper(p_column_name);

  if v_nullable = 'N' then
    v_sql := 'alter table ' || p_table_name || ' modify ' || p_column_name || ' null';
    dbms_output.put_line('--让目标字段暂时可以为空');
    dbms_output.put_line(v_sql || ';');
    if p_is_auto_run = 1 then
      execute immediate (v_sql);
    end if;
  end if;

  v_sql := 'alter table ' || p_table_name || ' add ' || v_temp_column_name || ' ' || trim(p_from_data_type);
  dbms_output.put_line('--先新增一个临时字段用于备份数据');
  dbms_output.put_line(v_sql || ';');
  if p_is_auto_run = 1 then
    execute immediate (v_sql);
  end if;

  v_sql := 'UPDATE ' || p_table_name || ' SET ' || v_temp_column_name || ' = ' || p_column_name || ' WHERE ' || p_column_name || ' is not null ';
  dbms_output.put_line('--将数据赋值给临时字段');
  dbms_output.put_line(v_sql || ';');
  if p_is_auto_run = 1 then
    execute immediate (v_sql);
  end if;

  v_sql := 'UPDATE ' || p_table_name || ' SET ' || p_column_name || ' = null WHERE ' || p_column_name || ' is not null ';
  dbms_output.put_line('--将目标字段数据置空');
  dbms_output.put_line(v_sql || ';');
  if p_is_auto_run = 1 then
    execute immediate (v_sql);
  end if;

  v_sql := 'alter table ' || p_table_name || ' modify ' || p_column_name || ' ' || p_to_data_type;
  dbms_output.put_line('--修目标字段类型');
  dbms_output.put_line(v_sql || ';');
  if p_is_auto_run = 1 then
    execute immediate (v_sql);
  end if;

  v_sql := 'UPDATE ' || p_table_name || ' SET ' || p_column_name || ' = ' || v_temp_column_name || ' WHERE ' || v_temp_column_name || ' is not null';
  dbms_output.put_line('--将临时字段的值重新赋值给目标字段');
  dbms_output.put_line(v_sql || ';');
  if p_is_auto_run = 1 then
    execute immediate (v_sql);
  end if;

  v_sql := 'alter table ' || p_table_name || ' drop column ' || v_temp_column_name;
  dbms_output.put_line('--删除临时字段');
  dbms_output.put_line(v_sql || ';');
  if p_is_auto_run = 1 then
    execute immediate (v_sql);
  end if;

  if v_nullable = 'N' then
    v_sql := 'alter table ' || p_table_name || ' modify ' || p_column_name || ' not null';
    dbms_output.put_line('--让目标字段重新设置不允许为空');
    dbms_output.put_line(v_sql || ';');
    if p_is_auto_run = 1 then
      execute immediate (v_sql);
    end if;
  end if;

end;

/


-- 调用示例1：手动执行脚本  mgr number -> mgr varchar2(4)
-- CALL PROC_CHANGE_COLUMN_TYPE_WMX('emp', 'empno', 'number(4)', 'varchar2(38)', 2);

-- 调用示例2：自动执行脚本  mgr varchar2(4) mgr number
-- CALL PROC_CHANGE_COLUMN_TYPE_WMX('emp', 'mgr', 'varchar2(38)', 'number(4)', 1);