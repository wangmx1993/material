
-- 查询重复的数据
select mof_div_code, fiscal_year,version, agency_code, is_deleted, count(1)
  from bas_agency_info t
 where is_deleted = 2
 and t.version='2022rcdtwh'
 group by mof_div_code, fiscal_year,version, agency_code, is_deleted
having count(1) > 1;--179



--优先保留更新时间最新的数据,
--更新时间相同时,优先保留创建时间最新的数据
--创建时间也相等时,随机保留一条

declare
  cursor vrows is
  -- 查询重复的数据
    select mof_div_code,
           fiscal_year,
           version,
           agency_code,
           is_deleted,
           count(1)
      from bas_agency_info t
     where is_deleted = 2
       and t.version = '2022rcdtwh'
     group by mof_div_code, fiscal_year, version, agency_code, is_deleted
    having count(1) > 1;

  v_count           number := 0;
  v_pre_create_time bas_agency_info.create_time%type; -- 对比时记录上一条数据的 create_time 值
  v_pre_update_time bas_agency_info.update_time%type; -- 对比时记录上一条数据的 update_time 值
  v_pre_agency_id   bas_agency_info.agency_id%type; -- 对比时记录上一条数据的 agency_id 值

begin
  for vrow in vrows loop
    --每次处理重复数据前，先置为 null.
    v_pre_create_time := null;
    v_pre_update_time := null;
    v_pre_agency_id   := null;

    v_count := v_count + 1;
    dbms_output.put_line(v_count || ' ' || vrow.MOF_DIV_CODE || ' ' ||
                         vrow.AGENCY_CODE || ' ' || vrow.fiscal_year || ' ' ||
                         vrow.version || ' 重复.');

    -- 逐个查询重复的数据，然后对重复的数据进行处理。
    -- 思路是：逐个对比前后两条数据，每次都删除其中一条，比如重复了 3 条，先比较1，2条，假如结果是保留第2条，然后比较 2，3条，再删除其中优先级低的，依此类推。
    for agency in (select MOF_DIV_CODE,
                          AGENCY_CODE,
                          t.agency_id,
                          t.update_time,
                          t.create_time,
                          rowid
                     from bas_agency_info t
                    where t.mof_div_code = vrow.MOF_DIV_CODE
                      and t.agency_code = vrow.AGENCY_CODE
                      and t.is_deleted = vrow.is_deleted
                      and t.fiscal_year = vrow.fiscal_year
                      and t.version = vrow.version) loop

      --不为 null 表示前一条已经记录了
      if v_pre_agency_id is not null then
        -- 如果前一条更新时间小于后一条,则逻辑删除前一条
        if v_pre_update_time < agency.update_time then
          UPDATE bas_agency_EXT T
             SET t.is_deleted = 1
           WHERE agency_id = v_pre_agency_id;
          UPDATE bas_agency_info T
             SET t.is_deleted = 1
           WHERE agency_id = v_pre_agency_id;
          dbms_output.put_line('  agency_id ' || v_pre_agency_id || ' 对比 ' ||
                               agency.agency_id || ' 保留 ' ||
                               agency.agency_id || ' 删除 ' ||
                               v_pre_agency_id ||
                               '  前一条更新时间小于后一条,则逻辑删除前一条');
          --重新赋值当前保留下来的数据，与后一条继续进行对比
          v_pre_update_time := agency.update_time;
          v_pre_create_time := agency.create_time;
          v_pre_agency_id   := agency.agency_id;

          -- 如果前一条更新时间大于后一条,则逻辑删除后一条
        elsif v_pre_update_time > agency.update_time then
          UPDATE bas_agency_EXT T
             SET t.is_deleted = 1
           WHERE agency_id = agency.agency_id;
          UPDATE bas_agency_info T
             SET t.is_deleted = 1
           WHERE agency_id = agency.agency_id;
          dbms_output.put_line('  agency_id ' || v_pre_agency_id || ' 对比 ' ||
                               agency.agency_id || ' 保留 ' ||
                               v_pre_agency_id || ' 删除 ' ||
                               agency.agency_id ||
                               '  前一条更新时间大于后一条,则逻辑删除后一条        ');

          -- 如果前一条创建时间小于后一条,则逻辑删除前一条
        elsif v_pre_create_time < agency.create_time then
          UPDATE bas_agency_EXT T
             SET t.is_deleted = 1
           WHERE agency_id = v_pre_agency_id;
          UPDATE bas_agency_info T
             SET t.is_deleted = 1
           WHERE agency_id = v_pre_agency_id;
          dbms_output.put_line('  agency_id ' || v_pre_agency_id || ' 对比 ' ||
                               agency.agency_id || ' 保留 ' ||
                               agency.agency_id || ' 删除 ' ||
                               v_pre_agency_id ||
                               '  前一条创建时间小于后一条,则逻辑删除前一条');
          --重新赋值当前保留下来的数据，与后一条继续进行对比
          v_pre_update_time := agency.update_time;
          v_pre_create_time := agency.create_time;
          v_pre_agency_id   := agency.agency_id;

          -- 如果前一条创建时间大于后一条,则逻辑删除后一条
        elsif v_pre_create_time > agency.create_time then
          UPDATE bas_agency_EXT T
             SET t.is_deleted = 1
           WHERE agency_id = agency.agency_id;
          UPDATE bas_agency_info T
             SET t.is_deleted = 1
           WHERE agency_id = agency.agency_id;
          dbms_output.put_line('  agency_id ' || v_pre_agency_id || ' 对比 ' ||
                               agency.agency_id || ' 保留 ' ||
                               v_pre_agency_id || ' 删除 ' ||
                               agency.agency_id ||
                               ' 前一条创建时间大于后一条,则逻辑删除后一条');

          --如果前后两条数据 update_time、create_time 都相等，则默认删除上一条
        else
          UPDATE bas_agency_EXT T
             SET t.is_deleted = 1
           WHERE agency_id = v_pre_agency_id;
          UPDATE bas_agency_info T
             SET t.is_deleted = 1
           WHERE agency_id = v_pre_agency_id;
          dbms_output.put_line('  agency_id ' || v_pre_agency_id || ' 对比 ' ||
                               agency.agency_id || ' 保留 ' ||
                               agency.agency_id || ' 删除 ' ||
                               v_pre_agency_id || ' 时间相同,默认删除上一条.');
          --重新赋值当前保留下来的数据，与后一条继续进行对比
          v_pre_update_time := agency.update_time;
          v_pre_create_time := agency.create_time;
          v_pre_agency_id   := agency.agency_id;
        end if;

      else
        --第一次循环赋值
        v_pre_update_time := agency.update_time;
        v_pre_create_time := agency.create_time;
        v_pre_agency_id   := agency.agency_id;
      end if;

    end loop;
  end loop;
  --  commit; --4、提交事务
end;
