
-- 书籍类型表
CREATE TABLE IF NOT EXISTS book_type (
    id INT PRIMARY KEY auto_increment COMMENT '主键',
    name VARCHAR(64) NOT NULL COMMENT '类型名称'
);

-- 书籍表
CREATE TABLE IF NOT EXISTS book (
  id int(11) PRIMARY KEY AUTO_INCREMENT COMMENT '主键',
  title varchar(128) NOT NULL COMMENT '标题',
  price float(8,2) DEFAULT NULL COMMENT '价格',
  publish date DEFAULT NULL COMMENT '发布时间',
    info varchar(128) DEFAULT NULL COMMENT '描述',
    type_id INT COMMENT '外键',
    FOREIGN KEY(type_id) REFERENCES book_type(id)
);

-- 插入测试数据
INSERT into book_type VALUES(null,'武侠');
INSERT into book_type VALUES(null,'科幻');
INSERT into book_type VALUES(null,'言情');
INSERT into book_type VALUES(null,'修仙');
INSERT into book_type VALUES(null,'历史');
INSERT into book_type VALUES(null,'动漫');

INSERT INTO book VALUES (NULL,'三国演义',88.68,'2019-09-08','',5);
INSERT INTO book VALUES (NULL,'红楼梦',48.35,'2019-03-08','一个家族的兴衰',3);
INSERT INTO book VALUES (NULL,'青云志',58.48,'2019-05-08','少年修仙传奇',4);
INSERT INTO book VALUES (NULL,'三体',83.68,'2019-04-08','',2);
INSERT INTO book VALUES (NULL,'笑傲江湖',81.18,'2019-06-08','金庸武侠',1);
INSERT INTO book VALUES (NULL,'倚天屠龙记',45.78,'2019-07-08','金庸武侠',1);
INSERT INTO book(id,title,price,publish) VALUES (NULL,'中华上下五千年',65.35,'2019-01-08');
INSERT INTO book (title, price, publish, info, type_id) VALUES ('水浒传', '58.00', '2012-05-08', '四大名著', '5');
INSERT INTO book (title, price, publish, info, type_id) VALUES ('西游记', '68.50', '2012-03-28', '四大名著', '5');
INSERT INTO book (title, price, publish, info, type_id) VALUES ('雪山飞狐', '38.50', '2019-05-18', '金庸武侠', '1');
INSERT INTO book (title, price, publish, info, type_id) VALUES ('鹿鼎记', '35.80', '2020-01-18', '金庸武侠', '1');
INSERT INTO book (title, price, publish, info, type_id) VALUES ('花千骨', '45.80', '2010-02-18', NULL, '4');