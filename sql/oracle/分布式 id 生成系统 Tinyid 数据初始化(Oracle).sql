CREATE TABLE tiny_id_info (
  id NUMBER NOT NULL PRIMARY KEY,
  biz_type varchar2(63) DEFAULT '' NOT NULL  unique,
  begin_id NUMBER DEFAULT 0 NOT NULL ,
  max_id NUMBER DEFAULT 0 NOT NULL ,
  step NUMBER(11) DEFAULT 0,
  delta NUMBER(11)  DEFAULT 1 NOT NULL,
  remainder NUMBER(11) DEFAULT 0 NOT NULL,
  create_time timestamp DEFAULT sysdate NOT NULL,
  update_time timestamp DEFAULT sysdate NOT NULL,
  version NUMBER DEFAULT 0 NOT NULL
);

comment on table tiny_id_info  is 'id信息表'; 
comment on column tiny_id_info.id is '主键';  
comment on column tiny_id_info.biz_type is '业务类型，唯一';  
comment on column tiny_id_info.begin_id is '开始id，仅记录初始值，无其他含义。初始化时begin_id和max_id应相同';  
comment on column tiny_id_info.max_id is '当前最大id';  
comment on column tiny_id_info.step is '步长,号段长度';  
comment on column tiny_id_info.delta is '每次id增量'; 
comment on column tiny_id_info.remainder is '余数'; 
comment on column tiny_id_info.create_time is '创建时间'; 
comment on column tiny_id_info.update_time is '更新时间'; 
comment on column tiny_id_info.version is '版本号'; 



CREATE TABLE tiny_id_token (
  id NUMBER NOT NULL PRIMARY KEY,
  token varchar2(255) DEFAULT '' NOT NULL ,
  biz_type varchar2(63) DEFAULT '' NOT NULL ,
  remark varchar2(255) DEFAULT '' NOT NULL ,
  create_time timestamp DEFAULT sysdate NOT NULL ,
  update_time timestamp DEFAULT sysdate NOT NULL 
);

comment on table tiny_id_token  is 'token信息表'; 
comment on column tiny_id_token.token is 'token口令';  
comment on column tiny_id_token.biz_type is '此token可访问的业务类型标识';  
comment on column tiny_id_token.remark is '备注';  
comment on column tiny_id_token.create_time is '创建时间';  
comment on column tiny_id_token.update_time is '更新时间';  



INSERT INTO tiny_id_info (id, biz_type, begin_id, max_id, step, delta, remainder, create_time, update_time, version)
VALUES (1, 'test', 1, 1, 100000, 1, 0, sysdate, sysdate, 1);

INSERT INTO tiny_id_info (id, biz_type, begin_id, max_id, step, delta, remainder, create_time, update_time, version)
VALUES (2, 'test_odd', 1, 1, 100000, 2, 1, sysdate, sysdate, 3);


INSERT INTO tiny_id_token (id, token, biz_type, remark, create_time, update_time)
VALUES (1, '0f673adf80504e2eaa552f5d791b644c', 'test', '1', sysdate, sysdate);

INSERT INTO tiny_id_token (id, token, biz_type, remark, create_time, update_time)
VALUES (2, '0f673adf80504e2eaa552f5d791b644c', 'test_odd', '1', sysdate, sysdate);

