# Driver.js 概述

1、**Driver.js 是一个功能强大、高度可定制的原版 JavaScript 引擎，可在整个页面上引导用户的注意力。无外部依赖，轻量级，支持所有主流浏览器，高度可定制。**

- **简单**：易于使用，完全没有外部依赖性

- **轻量级**：与其他 +12kb gzip 压缩的库相比，gzip 压缩后仅为 5kb

- **高度可定制**：具有强大的 API，可以随心所欲地使用

- **Highlight anything**：突出显示页面上的任何（字面上的任何）元素

- **功能介绍**：为您的 Web 应用程序创建强大的功能介绍

- **焦点移位器**：为用户添加焦点移位器

- **用户友好**：一切都可以通过键盘控制

- **TypeScript**：用 TypeScript 编写

- **一致的行为**：可在所有浏览器中使用

- **MIT 许可**：免费用于个人和商业用途

```
Github：https://github.com/kamranahmedse/driver.js
官网：https://driverjs.com/
官网文档：https://driverjs.com/docs/installation
```

# Driver.js 使用示例

1、官网文档有详细的使用说明，下面是亲测的效果。

[src/main/resources/static/html/页面导览Driver.html · 汪少棠/web_app - Gitee.com](https://gitee.com/wangmx1993/web_app/blob/master/src/main/resources/static/html/%E9%A1%B5%E9%9D%A2%E5%AF%BC%E8%A7%88Driver.html)

![](../images/34234234214343545.gif)

2、如果觉得默认的弹框样式不喜欢，还可以参考官网文档自定义样式：https://driverjs.com/docs/styling-popover
