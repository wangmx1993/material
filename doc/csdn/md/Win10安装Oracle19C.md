# Win10安装Oracle19C图解

1、提前准备好一个大目录(如D:\software\Oracle19C)，将后续相关的内容全部放在它下面。如下所示，app作为Oracle基目录，后期安装时会自动新建；db_home是官网下载好的压缩包解压后的目录，手动修改目录名称就行，名称随意；tableSpace作为表空间目录用于存储后期的表空间，手动新建，名称随意。

![](../images/2024-12-23_154104.png)

2、将下载好的Oracle19C压缩包解压到指定目录，如下所示 db_home，setup.exe 是安装文件，deinstall/deinstall.bat 是卸载文件。路径切记不要有中文和空格。这些内容安装完也不能删除，相反它本身也是作为数据库内容的一部分。

![](../images/oraclle19C1.png)

3、默认

![](../images/oraclle19C2.png)

4、默认

![](../images/oraclle19C3.png)

5、默认

![](../images/oraclle19C4.png)

6、基目录指向提前准备好的目录；因为公司都是国内的项目，所以字符集都是使用的[ZHS16GBK]，而不是默认的[AL32UTF8]；

![](../images/oraclle19C5.png)

7、点击安装

![](../images/oraclle19C6.png)

![](../images/oraclle19C7.png)

8、安装完成后 https://localhost:5500/em 可访问 Oralce 数据库，这通常是数据库管理员(DBA)的事情。sys 账号是数据库管理员(DBA)账号，system 是系统管理员账号，它们默认都已经解锁了，密码就是之前设置的口令。

![](../images/oraclle19C8.png)

![](../images/oraclle19C9.png)

9、通常安装好的数据库需要提供给其他同事进行访问，所以需要修改下面的监听文件。

![](../images/oraclle19C10.png)

10、通常系统环境变量默认已经有了。

![](../images/oraclle19C11.png)
