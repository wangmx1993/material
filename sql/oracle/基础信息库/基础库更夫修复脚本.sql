-- 1、修复基础中父节点ID为空或者为0的单位信息(生产已执行)
declare
  i number := 0;
  cursor vrows is
    SELECT e.ele_id,
           e.ele_code,
           e.mof_div_code,
           e.parent_id,
           e.level_no,
           e.level_num,
           e.is_leaf
      FROM ele_agency e
     where (e.mof_div_code, e.ele_code) in
           (select b.mof_div_code, b.agency_code
              from bas_agency_info b
             where b.is_leaf = 1 -- 末级是单位
               and b.is_enabled = 1 -- 有效的
               and b.is_deleted = 2 --未删除的
               and nvl(b.parent_id, '0') = '0')
       and sysdate between e.start_date and e.end_date --未失效的
       and e.is_deleted = 2 -- 未删除的
       and e.is_leaf = 1 --叶子节点是单位
       and nvl(e.parent_id, '0') != '0';
begin
  for vrow in vrows loop
    update bas_agency_info t
       set t.parent_id = vrow.parent_id
     where t.agency_code = vrow.ele_code
       and t.mof_div_code = vrow.mof_div_code;
    i := i + 1;
  end loop;
  dbms_output.put_line('一共更新【' || i || ' 】条数据');
end;


-- 4、修复基础库中已维护的父节点ID与平台维护的父节点ID不一致的单位信息(生产已执行)
declare
  i number := 0;
  cursor vrows is
    SELECT e.ele_id,
           e.ele_code,
           e.mof_div_code,
           e.parent_id,
           e.level_no,
           e.level_num,
           e.is_leaf
      FROM ele_agency e
     where sysdate between e.start_date and e.end_date --未失效的
       and e.is_deleted = 2 -- 未删除的
       and e.is_leaf = 1 --叶子节点是单位
       and nvl(e.parent_id, '0') != '0'
       and (e.mof_div_code, e.ele_code) in
           (select b.mof_div_code, b.agency_code
              from bas_agency_info b
             where b.is_leaf = 1 -- 末级是单位
               and b.is_enabled = 1 -- 有效的
               and b.is_deleted = 2 --未删除的
               and nvl(b.parent_id, '0') != '0'
               and b.parent_id != e.parent_id);
begin
  for vrow in vrows loop
    update bas_agency_info t
       set t.parent_id = vrow.parent_id
     where t.agency_code = vrow.ele_code
       and t.mof_div_code = vrow.mof_div_code;
    i := i + 1;
  end loop;
  dbms_output.put_line('一共更新【' || i || ' 】条数据');
end;


-- 5、修复基础库中已维护的 biz_key(平台单位ID) 与平台维护的单位ID不一致的单位信息(生产已执行)
declare
  i number := 0;
  cursor vrows is
    SELECT e.ele_id,
           e.ele_code,
           e.mof_div_code,
           e.parent_id,
           e.level_no,
           e.level_num,
           e.is_leaf
      FROM ele_agency e
     where sysdate between e.start_date and e.end_date --未失效的
       and e.is_deleted = 2 -- 未删除的
       and e.is_leaf = 1 --叶子节点是单位
       and exists (select 1
              from bas_agency_info b
             where b.agency_code = e.ele_code
               and b.mof_div_code = e.mof_div_code
               and b.is_leaf = 1 -- 末级是单位
               and b.is_enabled = 1 -- 有效的
               and b.is_deleted = 2 --未删除的
               and b.biz_key != e.ele_id);
begin
  for vrow in vrows loop
    update bas_agency_info t
       set t.biz_key = vrow.ele_id
     where t.agency_code = vrow.ele_code
       and t.mof_div_code = vrow.mof_div_code;
    i := i + 1;
  end loop;
  dbms_output.put_line('一共更新【' || i || ' 】条数据');
end;


-- 6、修复基础库中单位名称与平台不一致的单位信息
declare
  i number := 0;
  cursor vrows is
    SELECT e.ele_id,
           e.ele_code,
           e.ele_name,
           e.mof_div_code,
           e.parent_id
      FROM ele_agency e
     where sysdate between e.start_date and e.end_date --未失效的
       and e.is_deleted = 2 -- 未删除的
       and e.is_leaf = 1 --叶子节点是单位
       and exists (select 1
              from bas_agency_info b
             where b.agency_code = e.ele_code
               and b.mof_div_code = e.mof_div_code
               and b.is_leaf = 1 -- 末级是单位
               and b.is_enabled = 1 -- 有效的
               and b.is_deleted = 2 --未删除的
               and b.agency_name != e.ele_name);
begin
  for vrow in vrows loop
    update bas_agency_info t
       set t.agency_name = vrow.ele_name
     where t.agency_code = vrow.ele_code
       and t.mof_div_code = vrow.mof_div_code;
    i := i + 1;
  end loop;
  dbms_output.put_line('一共更新【' || i || ' 】条数据');
end;


-- 7、修复基础库人员信息表中单位名称与平台不一致的人员信息
declare
  i number := 0;
  cursor vrows is
    SELECT e.ele_id, e.mof_div_code, e.ele_code, e.ele_name, e.parent_id
      FROM ele_agency e
     where sysdate between e.start_date and e.end_date --未失效的
       and e.is_deleted = 2 -- 未删除的
       and e.is_leaf = 1 --叶子节点是单位
       and exists (select 1
              from bas_person_info b
             where b.agency_code = e.ele_code
               and b.mof_div_code = e.mof_div_code
               and b.agency_name != e.ele_name);
begin
  for vrow in vrows loop
    update bas_person_info t
       set t.agency_name = vrow.ele_name
     where t.agency_code = vrow.ele_code
       and t.mof_div_code = vrow.mof_div_code;
    i := i + 1;
  end loop;
  dbms_output.put_line('一共操作【' || i || ' 】个单位!');
end;





