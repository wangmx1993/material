
-- 抽取 BAS_DEBT_LIMIT_NEW 表中有 而 BAS_DEBT_LIMIT 表中没有的字段
-- 复制查询结果就是执行脚本
SELECT 'ALTER TABLE BAS_DEBT_LIMIT ADD ' || t.COLUMN_NAME || ' ' ||
       t.DATA_TYPE || case
         when data_type = 'NUMBER' then
          case
          --指定了长度时
            when data_precision is not null then
             case
             --当是小数时
               when data_scale > 0 then
                '(' || data_precision || ',' || data_scale || ')'
               else
                '(' || to_char(data_precision) || ')'
             end
            else
             '(' || t.DATA_LENGTH || ')'
          end
         when data_type in ('CHAR', 'VARCHAR2', 'NVARCHAR2', 'CLOB') and
              t.DATA_LENGTH is not null then
          '(' || t.DATA_LENGTH || ')'
         else
          ''
       end || ';' as sql2_
  FROM user_tab_columns T
 where t.TABLE_NAME = 'BAS_DEBT_LIMIT_NEW'
   and not exists (SELECT 1
          FROM user_tab_columns T2
         where TABLE_NAME = 'BAS_DEBT_LIMIT'
           and t.COLUMN_NAME = t2.COLUMN_NAME);




--复制指定表的列生成建表语句
SELECT 'create table bas_person_info_former(' 复制内容执行即可建表 FROM dual
union all
SELECT '  ' || COLUMN_NAME || ' ' ||
       DATA_TYPE || case
         when data_type = 'NUMBER' then
          case
          --指定了长度时
            when data_precision is not null then
             case
             --当是小数时
               when data_scale > 0 then
                '(' || data_precision || ',' || data_scale || ')'
               else
                '(' || to_char(data_precision) || ')'
             end
            else
             '(' || DATA_LENGTH || ')'
          end
         when data_type in ('CHAR', 'VARCHAR2', 'NVARCHAR2', 'CLOB') and
              DATA_LENGTH is not null then
          '(' || DATA_LENGTH || ')'
         else
          ''
       end || ','
  FROM (SELECT distinct T.COLUMN_NAME, t.DATA_TYPE, t.DATA_LENGTH,t.DATA_PRECISION,t.COLUMN_ID,data_scale FROM user_tab_columns t
         where t.TABLE_NAME in (upper('bas_person_info')) order by t.COLUMN_ID) t2
union all

SELECT ') partition by list (MOF_DIV_CODE) subpartition by range (fiscal_YEAR)' FROM dual
union all

SELECT ' subpartition template (subpartition year_2020 values less than (2021),subpartition year_2021 values less than (2022),subpartition year_2022 values less than (2023),subpartition year_2023 values less than (2024),subpartition year_2024 values less than (2025),subpartition year_2025 values less than (2026),subpartition year_2026 values less than (2027),subpartition year_2027 values less than (2028),subpartition year_2028 values less than (2029),subpartition year_2029 values less than (2030),subpartition year_2030 values less than (2031),subpartition year_max values less than (maxvalue))(' FROM dual
union all

SELECT 'partition m' || T5.ele_code_pre || ' values (''' ||
       listagg(t5.ele_code, ''',''') within group(order by t5.ele_code_pre desc) || '''),' as p_values
  FROM (select distinct substr(ele_code, 0, 4) ele_code_pre, ele_code
          from ele_mofdiv
         where ele_code like '43%'
         order by ele_code) t5
 group by t5.ele_code_pre
union all

SELECT 'partition m0 values (default) );' FROM dual;






-- 生成的建表语句格式如下：
--
-- create table bas_person_info_former(
--   PER_ID VARCHAR2(38),
--   BIZ_KEY VARCHAR2(38),
--   IDEN_TYPE_ID VARCHAR2(38),
--   IDEN_TYPE_CODE VARCHAR2(2),
--   IDEN_TYPE_NAME VARCHAR2(60),
--   IDEN_NO VARCHAR2(20),
--   MOF_DIV_CODE VARCHAR2(9),
--   PER_NAME VARCHAR2(60),
--   MANAGE_MOF_DEP_ID VARCHAR2(38),
--   MANAGE_MOF_DEP_CODE VARCHAR2(6),
--   MANAGE_MOF_DEP_NAME VARCHAR2(40),
--   BIRTH_MD VARCHAR2(8),
--   IS_FIN_SUPPLY NUMBER(1),
--   VERSION VARCHAR2(12),
--   VERSION_NAME VARCHAR2(400),
-- )
-- partition by list (MOF_DIV_CODE) subpartition by range (fiscal_YEAR)
-- subpartition template (
--              subpartition year_2020 values less than (2021),
--              subpartition year_2021 values less than (2022),
--              subpartition year_2022 values less than (2023),
--              subpartition year_2023 values less than (2024),
--              subpartition year_2024 values less than (2025),
--              subpartition year_2025 values less than (2026),
--              subpartition year_2026 values less than (2027),
--              subpartition year_2027 values less than (2028),
--              subpartition year_2028 values less than (2029),
--              subpartition year_2029 values less than (2030),
--              subpartition year_2030 values less than (2031),
--              subpartition year_max values less than (maxvalue)
--              )
-- (
-- partition m4300 values ('430000000'),
-- partition m4301 values ('430100000','430102000','430103000','430104000','430105000','430111000','430112000','430121000','430124000','430131000','430132000','430181000'),
-- partition m4302 values ('430200000','430202000','430203000','430204000','430211000','430221000','430223000','430224000','430225000','430231000','430281000'),
-- partition m4303 values ('430300000','430302000','430304000','430311000','430312000','430313000','430321000','430381000','430382000'),
-- partition m4304 values ('430400000','430405000','430406000','430407000','430408000','430412000','430415000','430416000','430417000','430421000','430422000','430423000','430424000','430426000','430481000','430482000'),
-- partition m4305 values ('430500000','430502000','430503000','430511000','430521000','430522000','430523000','430524000','430525000','430527000','430528000','430529000','430541000','430581000'),
-- partition m4306 values ('430600000','430602000','430603000','430611000','430621000','430623000','430624000','430626000','430631000','430632000','430633000','430634000','430681000','430682000'),
-- partition m4307 values ('430700000','430702000','430703000','430711000','430712000','430713000','430714000','430721000','430722000','430723000','430724000','430725000','430726000','430781000'),
-- partition m4308 values ('430800000','430802000','430811000','430821000','430822000'),
-- partition m4309 values ('430900000','430902000','430903000','430911000','430912000','430921000','430922000','430923000','430981000'),
-- partition m4310 values ('431000000','431002000','431003000','431011000','431012000','431021000','431022000','431023000','431024000','431025000','431026000','431027000','431028000','431081000'),
-- partition m4311 values ('431100000','431102000','431103000','431115000','431116000','431117000','431121000','431122000','431123000','431124000','431125000','431126000','431127000','431128000','431129000'),
-- partition m4312 values ('431200000','431202000','431211000','431212000','431221000','431222000','431223000','431224000','431225000','431226000','431227000','431228000','431229000','431230000','431281000','431282000'),
-- partition m4313 values ('431300000','431302000','431321000','431322000','431331000','431381000','431382000'),
-- partition m4331 values ('433100000','433101000','433111000','433122000','433123000','433124000','433125000','433126000','433127000','433130000'),
-- partition m0 values (default) );




