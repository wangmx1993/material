
-- Create table
create table DEPT (
  deptno NUMBER(2) not null,
  dname  VARCHAR2(14),
  loc    VARCHAR2(13)
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table DEPT  add constraint DEPT_PK primary key (DEPTNO);

create table DEPT_HIS (
  deptno NUMBER(2),
  dname  VARCHAR2(14),
  loc    VARCHAR2(13)
);

create table DEPT_LOG (
  deptno NUMBER(2),
  dname  VARCHAR2(14),
  loc    VARCHAR2(13)
);


insert into dept (DEPTNO, DNAME, LOC) values (10, 'ACCOUNTING', 'NEW YORK');
insert into dept (DEPTNO, DNAME, LOC) values (20, 'RESEARCH', 'DALLAS');
insert into dept (DEPTNO, DNAME, LOC) values (30, 'SALES14', 'CHICAGO');
insert into dept (DEPTNO, DNAME, LOC) values (40, 'OPERATIONS', 'BOSTON 11');

