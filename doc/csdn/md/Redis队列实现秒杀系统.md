
# 使用spring操作redis的list队列实现秒杀

```
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import java.time.Duration;
import java.util.Collection;

@Service
public class RedisFlashSaleService<T> {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    //添加字符串并设置过期时间
    public void addString(String key, String value, Duration duration) {
        stringRedisTemplate.opsForValue().set(key, value, duration);
    }

    //查找字符串
    public String findString(String key) {
        return stringRedisTemplate.opsForValue().get(key);
    }

    //根据Key删除
    public Boolean deleteByKey(String key) {
        return stringRedisTemplate.delete(key);
    }

 //在队列尾部减少一个对象
    public String removeOneEntryOnListRight(String listName) {
        return stringRedisTemplate.opsForList().rightPop(listName);
    }

 //在队列头部新增对象
    public Long addEntriesOnListLeft(String listName, Collection<String> args) {
        return stringRedisTemplate.opsForList().leftPushAll(listName, args);
    }

}
```

主要使用的是最后两个方法，最后一个方法，在队列头部新增对象，如果没有这个队列，他会创建出来这个队列，然后将一个集合统统塞到这个redis队列中。倒数第二个方法每调用一次，会删除队列中最后一个元素，然后返回这个元素的值，如果队列中已经没有元素了(队列已经没了)那么他会返回null，他们都是原子操作。

如此，每个请求都无需经过加锁操作，直接利用redis的单线程特性，即可实现高并发下的秒杀：请求到达redis，redis会逐个执行，每一次执行要么返回一个值，要么返回null。很显然，返回值的就是抢到了，返回null的就是没抢到。而且可以灵活的为这个队列新加入一些元素(老板发话再加100台)或者直接把这个队列删了(老板说不行，不卖了)都不会对代码产生任何影响。

其中对应的redis操作指令分别是：

- 在队列左侧新增：lpush

- 在队列右侧消费：rpop

老板不卖了：del

# 简单的使用方法

先贴在任务开始时向redis中插入一个大队列

```
List<String> entriesList = new LinkedList<>();
   for (int i = 0; i < 100; i++){
       entriesList.add("某个商品");
   }
   redisFlashSaleService.addEntriesOnListLeft("队列名",entriesList);
```

突然想到这个实现即使秒杀100台不同型号的手机（并且在秒到时就通知用户秒到的是啥），也不用改代码。

每次秒杀执行：

```
String redisResult = redisFlashSaleService.removeOneEntryOnListRight("队列名");
    if (null == redisResult) {
        //说明没抢到
    }else{
 //说明抢到了 执行抢到逻辑
}
```

突然发现这个实现看起来甚至比那些所谓的秒杀demo还简单，但他既没有并发问题，也没有为了解决并发问题而衍生的性能问题。